﻿using System;

namespace Mu
{
	internal static class Interlace
	{
		/// <summary>
		/// Simple linear pattern that starts from (0, 0) and goes to (w, h).
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
		public static Vector2us[] Linear(int width, int height)
		{
			Vector2us[] positions = new Vector2us[width * height];
			
			for (ushort y = 0; y < height; y++)
			{
				for (ushort x = 0; x < width; x++)
				{
					positions[y * width + x] = new Vector2us(x, y);
				}
			}

			return positions;
		}

		/// <summary>
		/// Progressive pattern with multiple passes.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
		public static Vector2us[] Progressive(int width, int height)
		{
			Vector2us[] positions = new Vector2us[width * height];

			const int numPasses = 8;
			int i = 0;
			for (int p = numPasses - 1; p >= 0; p--)
			{
				ushort step = (ushort)(1 << p);
				ushort mask = (ushort)((step << 1) - 1);
				bool skip = p != numPasses - 1;

				for (ushort y = 0; y < height; y += step)
				{
					for (ushort x = 0; x < width; x += step)
					{
						if (skip && (x & mask) == 0 && (y & mask) == 0) continue;
						positions[i].X = x;
						positions[i].Y = y;
						i++;
					}
				}
			}

			return positions;
		}

		/// <summary>
		/// Pattern that starts from the center of the image and expands.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
		public static Vector2us[] Centered(int width, int height)
		{
			Vector2us[] positions = new Vector2us[width * height];

			int x0 = width / 2;
			int x1 = x0 + 1;
			int y0 = height / 2;
			int y1 = y0 + 1;

			int i = 0;
			int r = Math.Max(width, height) / 2;

			while (i < positions.Length)
			{
				if (y0 >= 0)
				{
					int start = Math.Max(0, x0);
					int end = Math.Min(width - 1, x1);
					for (int x = start; x <= end; x++)
						positions[i++] = new Vector2us((ushort)x, (ushort)y0);
				}
				if (x0 >= 0)
				{
					int start = Math.Max(0, y0 + 1);
					int end = Math.Min(height - 1, y1 - 1);
					for (int y = start; y <= end; y++)
						positions[i++] = new Vector2us((ushort)x0, (ushort)y);
				}
				if (y1 < height)
				{
					int start = Math.Max(0, x0);
					int end = Math.Min(width - 1, x1);
					for (int x = end; x >= start; x--)
						positions[i++] = new Vector2us((ushort)x, (ushort)y1);
				}
				if (x1 < width)
				{
					int start = Math.Max(0, y0 + 1);
					int end = Math.Min(height - 1, y1 - 1);
					for (int y = end; y >= start; y--)
						positions[i++] = new Vector2us((ushort)x1, (ushort)y);
				}

				x0--;
				y0--;
				x1++;
				y1++;
			}

			return positions;
		}
	}
}
