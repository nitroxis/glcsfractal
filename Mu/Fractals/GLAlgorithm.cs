﻿using System;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// An algorithm implemented using OpenGL.
	/// </summary>
	public abstract class GLAlgorithm : Algorithm
	{
		#region Fields

		protected readonly GLContext context;

		private GLTexture texture; // the output buffer containing the iteration count; uint[totalSize].
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the texture.
		/// </summary>
		public GLTexture Texture
		{
			get { return this.texture; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GLAlgorithm.
		/// </summary>
		protected GLAlgorithm(GLContext context)
		{
			this.context = context;
			this.createTexture();
		}

		#endregion

		#region Methods

		protected override void OnSizeChanged()
		{
			base.OnSizeChanged();

			if (this.texture != null) this.texture.Dispose();

			if (this.context != null)
				this.createTexture();
		}

		private void createTexture()
		{
			this.texture = new GLTexture(this.context, TextureTarget.Texture2D);

			this.context.Invoke(() =>
			{
				GL.BindTexture(TextureTarget.Texture2D, this.texture.ID);
				GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.R32ui, this.Width, this.Height, 0, PixelFormat.RedInteger, PixelType.UnsignedInt, IntPtr.Zero);
			});
		}

		/// <summary>
		/// Clears the output texture.
		/// </summary>
		protected void Clear()
		{
			this.Markers.Clear();
			
			if (this.context.HasExtension("GL_ARB_clear_texture"))
			{
				this.context.Invoke(() =>
				{
					uint zero = 0;
					GL.ClearTexImage(this.texture.ID, 0, PixelFormat.RedInteger, PixelType.UnsignedInt, ref zero);
				});
			}
			else
			{
				this.context.Invoke(() =>
				{
					uint[] zero = new uint[this.Width * this.Height];
					GL.BindTexture(TextureTarget.Texture2D, this.texture.ID);
					GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.R32ui, this.Width, this.Height, 0, PixelFormat.RedInteger, PixelType.UnsignedInt, zero);
				});
			}
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			if (this.texture != null) this.texture.Dispose();
		}

		#endregion
	}
}
