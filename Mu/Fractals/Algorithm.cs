﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Mu
{
	/// <summary>
	/// Base class for fractal calculations.
	/// </summary>
	public abstract class Algorithm : DisposableObject
	{
		#region Events

		public event EventHandler ComputeProgress;

		#endregion
		
		#region Fields

		private int width;
		private int height;
		private uint maxIterations; // maximum number of iterations.

		protected FixedFormat fixedFormat;
		protected FixedComplex center; // location of our view in the complex plane.
		protected Fixed radius; // the zoom factor, i.e. the radius of a circle that is guaranteed to be visible.
		protected FixedComplex offset; // the location of the lower-left pixel, i.e. center - size/2
		protected FixedComplex size; // the size of the visible region.
		protected Fixed pixelSize; // the size of a pixel.

		private volatile bool isCanceled;

		private readonly List<Marker> markers;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the width.
		/// </summary>
		public int Width
		{
			get { return this.width; }
		}

		/// <summary>
		/// Gets the height.
		/// </summary>
		public int Height
		{
			get { return this.height; }
		}

		/// <summary>
		/// Gets or sets the number of maximum iterations per pixel.
		/// </summary>
		public uint MaxIterations
		{
			get { return this.maxIterations; }
			set
			{
				lock (this)
				{
					this.maxIterations = value;
					this.OnMaxIterationsChanged();	
				}
			}
		}

		/// <summary>
		/// Gets the fixed-point number format that is currently used for calculations.
		/// </summary>
		public FixedFormat FixedFormat
		{
			get { return this.fixedFormat; }
			set
			{
				lock (this)
				{
					if (value.Equals(this.fixedFormat))
						return;

					this.fixedFormat = value;
					this.initializeParameters();
					this.OnPrecisionChanged();
				}
			}
		}

		/// <summary>
		/// Gets a value that indicates whether the computation has been canceled.
		/// </summary>
		protected bool IsCanceled
		{
			get { return this.isCanceled; }
		}

		/// <summary>
		/// Gets a list of markers for debugging.
		/// </summary>
		public List<Marker> Markers
		{
			get { return this.markers; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FractalCalculator.
		/// </summary>
		protected Algorithm()
		{
			// some defaults.
			this.FixedFormat = new FixedFormat(2, 1);
			this.SetSize(256, 256);
			this.Reset();
			this.MaxIterations = 512;

			this.markers = new List<Marker>();
		}

		#endregion

		#region Methods

		#region Instance

		/// <summary>
		/// Updates the bounds of the visible region (i.e. the "offset", "size" and "delta" values) based on the center and zoom.
		/// </summary>
		private void updateBounds()
		{
			Fixed half = Fixed.Create(this.fixedFormat);
			Fixed temp = Fixed.Create(this.fixedFormat);

			half.Set(0.5d);
			temp.Set(2);

			if (this.height < this.width)
			{
				Fixed.Multiply(this.radius, temp, this.size.Imaginary);
				temp.Set(2.0d * this.width / this.height);
				Fixed.Multiply(this.radius, temp, this.size.Real);
			}
			else
			{
				Fixed.Multiply(this.radius, temp, this.size.Real);
				temp.Set(2.0d * this.height / this.width);
				Fixed.Multiply(this.radius, temp, this.size.Imaginary);
			}

			temp.Set(1.0f / this.width);
			Fixed.Multiply(this.size.Real, temp, this.pixelSize);

			// offset = center - size * 0.5
			this.offset.Real.Set(this.center.Real);
			Fixed.Multiply(this.size.Real, half, temp);
			Fixed.Subtract(this.offset.Real, temp, this.offset.Real);

			this.offset.Imaginary.Set(this.center.Imaginary);
			Fixed.Multiply(this.size.Imaginary, half, temp);
			Fixed.Subtract(this.offset.Imaginary, temp, this.offset.Imaginary);
		}

		private void checkPrecision()
		{
			// check for precision.
			const int precThreshold = 18;
			int msb = this.radius.MostSignificantBit - precThreshold;
			int prec = (msb - 31) / 32;

			int numDigits = this.FixedFormat.Digits;

			if (prec < 0)
			{
				numDigits = this.fixedFormat.Digits - prec;
			}
			else if (this.fixedFormat.Digits > 2)
			{
				prec = msb / 32;
				if (prec > 0)
					numDigits = this.fixedFormat.Digits - prec;
			}

			this.FixedFormat = new FixedFormat(numDigits, numDigits - 1);
		}

		/// <summary>
		/// Creates the calculation variables.
		/// </summary>
		private void initializeParameters()
		{
			Trace.WriteLine(string.Format("Initializing parameters with {0}-bit precision.", this.fixedFormat.Digits * 32));
			
			FixedComplex center2 = this.center;
			Fixed radius2 = this.radius;

			this.center = FixedComplex.Create(this.fixedFormat);
			this.radius = Fixed.Create(this.fixedFormat);

			if (center2 != null)
			{
				this.center.Real.Set(center2.Real);
				this.center.Imaginary.Set(center2.Imaginary);
			}

			if (radius2 != null)
				this.radius.Set(radius2);

			this.offset = FixedComplex.Create(this.fixedFormat);
			this.size = FixedComplex.Create(this.fixedFormat);
			this.pixelSize = Fixed.Create(this.fixedFormat);

			this.updateBounds();
		}

		/// <summary>
		/// Sets the the view to the specified coordinates.
		/// </summary>
		/// <param name="center"></param>
		/// <param name="radius"></param>
		public void SetCoordinates(FixedComplex center, Fixed radius)
		{
			lock (this)
			{
				this.center.Real.Set(center.Real);
				this.center.Imaginary.Set(center.Imaginary);
				this.radius.Set(radius);

				this.updateBounds();
			}
		}

		/// <summary>
		/// Resets zoom and center to the initial state.
		/// </summary>
		public void Reset()
		{
			this.SetCoordinates(FixedComplex.Create(this.fixedFormat, 0, 0), Fixed.Create(this.fixedFormat, 2));
		}

		/// <summary>
		/// Gets the current view coordinates of the fractal.
		/// </summary>
		/// <param name="center"></param>
		/// <param name="radius"></param>
		public void GetCoordinates(FixedComplex center, Fixed radius)
		{
			lock (this)
			{
				center.Real.Set(this.center.Real);
				center.Imaginary.Set(this.center.Imaginary);
				radius.Set(this.radius);
			}
		}
		
		/// <summary>
		/// Moves the center by the specified amount relative to the view radius.
		/// </summary>
		/// <param name="x">The X move offset.</param>
		/// <param name="y">The Y move offset.</param>
		public virtual void MoveCenter(double x, double y)
		{
			lock (this)
			{
				Fixed dx = Fixed.Create(this.fixedFormat, 2.0d * x);
				Fixed dy = Fixed.Create(this.fixedFormat, 2.0d * y);

				Fixed.Multiply(dx, this.radius, dx);
				Fixed.Multiply(dy, this.radius, dy);

				Fixed.Add(this.center.Real, dx, this.center.Real);
				Fixed.Add(this.center.Imaginary, dy, this.center.Imaginary);

				Fixed.Add(this.offset.Real, dx, this.offset.Real);
				Fixed.Add(this.offset.Imaginary, dy, this.offset.Imaginary);
			}
		}

		/// <summary>
		/// Performs a centered zoom.
		/// </summary>
		/// <param name="factor"></param>
		public virtual void ZoomToCenter(double factor)
		{
			lock (this)
			{
				Fixed z = Fixed.Create(this.fixedFormat, factor);

				// zoom.
				Fixed.Multiply(this.radius, z, this.radius);
				this.checkPrecision();
				this.updateBounds();
			}
		}

		/// <summary>
		/// Performs a zoom that zooms to the specified pixel.
		/// </summary>
		/// <param name="factor"></param>
		/// <param name="x">The relative X coordinate of the pixel to center the zoom to.</param>
		/// <param name="y">The relative Y coordinate of the pixel to center the zoom to.</param>
		public void ZoomToPoint(double factor, double x, double y)
		{
			lock (this)
			{
				this.MoveCenter(x * (1.0d - factor), y * (1.0d - factor));
				this.ZoomToCenter(factor);
			}
		}

		/// <summary>
		/// Copies the parameters from another fractal to this one.
		/// </summary>
		/// <param name="fractal"></param>
		public void CopyFrom(Algorithm fractal)
		{
			lock (this)
			{
				this.FixedFormat = fractal.fixedFormat;
				this.radius.Set(fractal.radius);
				this.center.Real.Set(fractal.center.Real);
				this.center.Imaginary.Set(fractal.center.Imaginary);
				this.MaxIterations = fractal.maxIterations;

				this.updateBounds();
			}
		}

		/// <summary>
		/// Sets the output size.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public void SetSize(int width, int height)
		{
			lock (this)
			{
				this.width = width;
				this.height = height;
				this.updateBounds();

				this.OnSizeChanged();
			}
		}

		protected abstract void Run();

		/// <summary>
		/// Starts the computation.
		/// </summary>
		/// <returns></returns>
		public void Compute()
		{
			lock (this)
			{
				if (this.isCanceled)
					return;

				this.Run();
			}
		}

		/// <summary>
		/// Cancels the computation.
		/// </summary>
		public void Cancel()
		{
			this.isCanceled = true;

			// wait until Run() exits.
			lock (this)
				this.isCanceled = false;
		}

		#region Events

		protected virtual void OnMaxIterationsChanged()
		{

		}

		protected virtual void OnPrecisionChanged()
		{

		}

		protected virtual void OnSizeChanged()
		{
			
		}

		protected virtual void OnComputeProgress(EventArgs e)
		{
			if (this.ComputeProgress != null)
				this.ComputeProgress(this, e);
		}

		#endregion

		/// <summary>
		/// Disposes the algorithm.
		/// </summary>
		protected override void Dispose(bool managed)
		{
			this.isCanceled = true;

			lock (this)
			{
				
			}
		}

		#endregion

		#endregion
	}
}
