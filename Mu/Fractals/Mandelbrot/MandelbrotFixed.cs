﻿using System;
using System.IO;

namespace Mu
{
	/// <summary>
	/// Mandelbrot algorithm using fixed-point arbitrary precision math.
	/// </summary>
	public sealed class MandelbrotFixed : Mandelbrot
	{
		#region Constructors

		/// <summary>
		/// Creates a new MandelbrotFixed.
		/// </summary>
		public MandelbrotFixed(GLContext context)
			: base(context, 32)
		{

		}

		#endregion

		#region Methods

		protected override string GetMathLibrary()
		{
			GlslGenerator generator = new GlslGenerator(this.fixedFormat);
			StringWriter writer = new StringWriter();
			generator.Generate(writer);
			writer.Close();
			return writer.GetStringBuilder().ToString();
		}

		protected override uint InitializeBuffers()
		{
			int fpSize = ((this.fixedFormat.Digits + 3) / 4) * 4;

			if (this.parameterBuffer == null)
				this.parameterBuffer = new GLBuffer(this.context, (IntPtr)(3 * fpSize * 4));

			uint[] data = new uint[3 * fpSize];

			Array.Copy(this.offset.Real.GetDigits(), 0, data, 0 * fpSize, this.fixedFormat.Digits);
			Array.Copy(this.offset.Imaginary.GetDigits(), 0, data, 1 * fpSize, this.fixedFormat.Digits);
			Array.Copy(this.pixelSize.GetDigits(), 0, data, 2 * fpSize, this.fixedFormat.Digits);

			this.parameterBuffer.Set(data);

			// create C and Z buffers.
			const uint totalSize = localSize * 32;
			IntPtr complexBufferSize = (IntPtr)(totalSize * 2u * (((uint)(this.fixedFormat.Digits + 3) / 4u) * 16u));
			
			if (this.cBuffer == null) this.cBuffer = new GLBuffer(this.context, complexBufferSize);
			if (this.zBuffer == null) this.zBuffer = new GLBuffer(this.context, complexBufferSize);

			return totalSize;
		}
 
		#endregion
	}
}
