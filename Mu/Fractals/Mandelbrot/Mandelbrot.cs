﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Fractal computation class.
	/// </summary>
	public abstract class Mandelbrot : GLAlgorithm
	{
		#region Constants

		protected const int localSize = 512;

		#endregion

		#region Fields

		private readonly uint globalSize;

		private GLProgram initProgram;
		private GLProgram iterateProgram;
		private GLBuffer positionsBuffer; // contains the X, Y positions of pixels.
		protected GLBuffer parameterBuffer; // contains the view parameters.
		protected GLBuffer cBuffer; // contains the positions in the complex plane for each pixel (i.e. C).
		protected GLBuffer zBuffer; // contains the current iteration value for each pixel (i.e. Z).

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Mandelbrot fractal.
		/// </summary>
		protected Mandelbrot(GLContext context, uint globalSize)
			: base(context)
		{
			this.globalSize = globalSize;
			this.createPrograms();
			this.generatePositions();
		}

		#endregion

		#region Methods

		protected abstract string GetMathLibrary();

		protected abstract uint InitializeBuffers();

		private void generatePositions()
		{
			if (this.positionsBuffer != null) this.positionsBuffer.Dispose();
			
			// generate interlace pattern.
			this.positionsBuffer = new GLBuffer(this.context, (IntPtr)(this.Width * this.Height * 4));
			this.positionsBuffer.Set(Interlace.Centered(this.Width, this.Height));
		}

		private void disposePrograms()
		{
			if (this.initProgram != null) this.initProgram.Dispose();
			if (this.iterateProgram != null) this.iterateProgram.Dispose();
		}
		
		private void createPrograms()
		{
			this.disposePrograms();

			Dictionary<string, string> includes = new Dictionary<string, string>();
			includes["Math"] = this.GetMathLibrary();

			// init shader.
			string initSource = Helper.GetCode("MandelbrotInit", s => s.Replace("GROUPSIZE", localSize.ToString(CultureInfo.InvariantCulture)), includes);
			this.initProgram = new GLProgram(this.context, "MandelbrotInit");
			this.initProgram.Attach(ShaderType.ComputeShader, initSource);
			this.initProgram.Link();

			// compute shader.
			string iterateSource = Helper.GetCode("Mandelbrot", s => s.Replace("GROUPSIZE", localSize.ToString(CultureInfo.InvariantCulture)), includes);
			this.iterateProgram = new GLProgram(this.context, "Mandelbrot");
			this.iterateProgram.Attach(ShaderType.ComputeShader, iterateSource);
			this.iterateProgram.Link();

#if DEBUG
			File.WriteAllText("MandelbrotInit.gen.glsl", initSource);
			File.WriteAllText("Mandelbrot.gen.glsl", iterateSource);
			if (this.context.HasExtension("GL_ARB_get_program_binary"))
			{
				// dump binary for inspection.
				this.context.Invoke(() =>
				{
					int length;
					BinaryFormat fmt;
					GL.GetProgram(this.iterateProgram.ID, (GetProgramParameterName)All.ProgramBinaryLength, out length);
					byte[] binary = new byte[length];
					GL.GetProgramBinary(this.iterateProgram.ID, length, out length, out fmt, binary);

					// detect nvidia's binary format.
					int start = 0;
					for (int i = 0; i < length - 4; i++)
					{
						if (binary[i + 0] == '!' &&
							binary[i + 1] == '!' &&
							binary[i + 2] == 'N' &&
							binary[i + 3] == 'V')
						{
							start = i;
						}
						else if (start > 0 && binary[i + 0] == 0)
						{
							byte[] text = new byte[i - start];
							Array.Copy(binary, start, text, 0, i - start);
							binary = text;
							break;
						}
					}

					File.WriteAllBytes("Mandelbrot.glsl.bin", binary);
				});
			}
#endif
		}

		protected override void OnSizeChanged()
		{
			base.OnSizeChanged();

			if (this.context != null)
				this.generatePositions();
		}

		protected override void OnPrecisionChanged()
		{
			base.OnPrecisionChanged();

			if (this.parameterBuffer != null) this.parameterBuffer.Dispose();
			if (this.cBuffer != null) this.cBuffer.Dispose();
			if (this.zBuffer != null) this.zBuffer.Dispose();

			this.cBuffer = null;
			this.zBuffer = null;
			this.parameterBuffer = null;

			if (this.context != null)
				this.createPrograms();
		}

		protected override void Run()
		{
			this.Clear();
			if (this.IsCanceled) return;

			uint totalUnits = this.InitializeBuffers();

			this.context.Invoke(() =>
			{
				uint maxUnits = (uint)this.Width * (uint)this.Height;

				this.Texture.BindImage(0, TextureAccess.ReadWrite, SizedInternalFormat.R32ui);
				this.cBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 1);
				this.zBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 2);
				this.parameterBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 3);
				
				uint globalOffset = 0;
				while (globalOffset < maxUnits)
				{
					if (this.IsCanceled) return;

					uint numInvokations = Math.Min(totalUnits, maxUnits - globalOffset);
					uint numIterations = 512u / (uint)this.fixedFormat.Digits;
					uint globalX = (numInvokations + localSize - 1) / localSize;

					// bind sub-region of the position buffer.
					this.positionsBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 0, globalOffset * 4u, numInvokations * 4u);

					// dispatch compute shader that calculates C for the current region.
					this.initProgram.Use();
					GL.DispatchCompute(globalX, 1, 1);

					// copy C to Z.
					GL.BindBuffer(BufferTarget.CopyReadBuffer, this.cBuffer.ID);
					GL.BindBuffer(BufferTarget.CopyWriteBuffer, this.zBuffer.ID);
					GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, IntPtr.Zero, IntPtr.Zero, this.cBuffer.Size);
					
					// iterate.
					this.iterateProgram.Use();
					GL.Uniform1(this.iterateProgram.GetLocation("numIterations"), numIterations);

					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);
					uint iterationOffset = 0;
					while (iterationOffset < this.MaxIterations)
					{
						if (this.IsCanceled) return;

						// check if number of iterations exceeds max iterations.
						if (numIterations > this.MaxIterations - iterationOffset)
						{
							numIterations = this.MaxIterations - iterationOffset;
							GL.Uniform1(this.iterateProgram.GetLocation("numIterations"), numIterations);
						}

						// dispatch.
						GL.DispatchCompute(globalX, 1, 1);
						iterationOffset += numIterations;
					}

					// report progress.
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);
					this.OnComputeProgress(EventArgs.Empty);
					globalOffset += numInvokations;
				}
			});
		}

		/// <summary>
		/// Disposes the calculator.
		/// </summary>
		protected override void Dispose(bool managed)
		{
			base.Dispose();

			this.disposePrograms();
			
			if (this.positionsBuffer != null) this.positionsBuffer.Dispose();
			if (this.parameterBuffer != null) this.parameterBuffer.Dispose();
			if (this.cBuffer != null) this.cBuffer.Dispose();
			if (this.zBuffer != null) this.zBuffer.Dispose();
		}

		#endregion
	}
}
