#version 430 core
layout (local_size_x = GROUPSIZE) in;

#include <Math>

uniform uint numIterations; // maximum number of iterations.

layout (r32ui, binding = 0) uniform uimage2D outputImage; // output image.

layout (std430, binding = 0) readonly buffer buffer0 { uint positions[]; }; // pixel positions.
layout (shared, binding = 1) readonly buffer buffer1 { complex C[]; } cBuffer; // positions in the complex plane, aka C.
layout (shared, binding = 2) buffer buffer2 { complex Z[]; } zBuffer; // iterated values, aka Z.

uint iterate(complex C, inout complex Z)
{
	uint i = 0;

	complex ZZ; // square of Z.
	real a; // square of the absolute value of Z.

	// main loop.
	// Z = Z^2 + C
	// |Z| must stay smaller than 2 for a point to be part of the set.
	// as we are using |Z|^2 here we simple use 4 instead of calculating the square root of it.
	while(i < numIterations)
	{
		// a = |Z| ^ 2
		sqr(Z.r, ZZ.r);
		sqr(Z.i, ZZ.i);
		add(ZZ.r, ZZ.i, a);

		// check bailout.
		if(bailout(a))
			break;
		
		// Z.i = ((Z.r + Z.i) ^ 2 - Z.r ^ 2 - Z.i ^ 2) + C.i
		add(Z.r, Z.i, a);
		sqr(a, Z.i);
		sub(Z.i, ZZ.r, Z.i);
		sub(Z.i, ZZ.i, Z.i);
		add(Z.i, C.i, Z.i);

		// Z.r = (Z.r ^ 2 - Z.i ^ 2) + C.r
		sub(ZZ.r, ZZ.i, Z.r);
		add(Z.r, C.r, Z.r);

		// increment iteration counter.
		i++;
	}

	return i;
}

void main(void)
{
	uint id = gl_GlobalInvocationID.x;

	// read C and Z from input buffers and iterate.
	complex C = cBuffer.C[id];
	complex Z = zBuffer.Z[id];
	uint newIterations = iterate(C, Z);
	zBuffer.Z[id] = Z;

	// add the result of this pass to the output.
	ivec2 position = ivec2(positions[id] & 0xFFFF, positions[id] >> 16);
	imageAtomicAdd(outputImage, position, newIterations);
}

