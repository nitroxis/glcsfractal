#version 430 core
layout (local_size_x = GROUPSIZE) in;

#include <Math>

layout (std430, binding = 0) readonly buffer buffer0 { uint positions[]; }; // pixel positions.
layout (shared, binding = 1) writeonly buffer buffer1 { complex C[]; } cBuffer; // positions in the complex plane, aka C.

// view parameters:
layout (std430, binding = 3) readonly buffer viewParameters
{
	complex offset; // offset in the complex plane (i.e. the position of the bottom left corner).
	real pixelSize; // the size of a pixel.
};

void main(void)
{
	uint id = gl_GlobalInvocationID.x;

	// find out x and y coordinate of the pixel.
	uvec2 position = uvec2(positions[id] & 0xFFFF, positions[id] >> 16);

	// calculate the position in the complex plane.
	complex C;

	mul(pixelSize, position.x, C.r);
	mul(pixelSize, position.y, C.i);

	add(offset.r, C.r, C.r);
	add(offset.i, C.i, C.i);
	
	// store it in the state buffer.
	cBuffer.C[id] = C;
}