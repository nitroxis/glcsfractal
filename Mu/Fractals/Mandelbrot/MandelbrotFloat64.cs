﻿using System;

namespace Mu
{
	/// <summary>
	/// Mandelbrot algorithm using 64-bit floating-point math.
	/// </summary>
	public sealed class MandelbrotFloat64 : Mandelbrot
	{
		#region Constructors

		/// <summary>
		/// Creates a new MandelbrotFloat32.
		/// </summary>
		public MandelbrotFloat64(GLContext context)
			: base(context, 128)
		{

		}

		#endregion

		#region Methods

		protected override string GetMathLibrary()
		{
			return Helper.GetCode("Float64Math");
		}

		protected override uint InitializeBuffers()
		{

			if (this.parameterBuffer == null)
				this.parameterBuffer = new GLBuffer(this.context, (IntPtr)(32));

			double[] data = new double[3];

			data[0] = this.offset.Real.ToDouble();
			data[1] = this.offset.Imaginary.ToDouble();
			data[2] = this.pixelSize.ToDouble();

			this.parameterBuffer.Set(data);

			// create C and Z buffers.
			const uint totalSize = 128 * localSize;
			
			if (this.cBuffer == null) this.cBuffer = new GLBuffer(this.context, (IntPtr)(totalSize * 16));
			if (this.zBuffer == null) this.zBuffer = new GLBuffer(this.context, (IntPtr)(totalSize * 16));

			return totalSize;
		}
 
		#endregion
	}
}
