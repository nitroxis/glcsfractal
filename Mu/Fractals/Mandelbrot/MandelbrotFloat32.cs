﻿using System;

namespace Mu
{
	/// <summary>
	/// Mandelbrot algorithm using 32-bit floating-point math.
	/// </summary>
	public sealed class MandelbrotFloat32 : Mandelbrot
	{
		#region Constructors

		/// <summary>
		/// Creates a new MandelbrotFloat32.
		/// </summary>
		public MandelbrotFloat32(GLContext context)
			: base(context, 256)
		{

		}

		#endregion

		#region Methods

		protected override string GetMathLibrary()
		{
			return Helper.GetCode("Float32Math");
		}

		protected override uint InitializeBuffers()
		{
			if (this.parameterBuffer == null)
				this.parameterBuffer = new GLBuffer(this.context, (IntPtr)(16));

			float[] data = new float[3];

			data[0] = (float)this.offset.Real.ToDouble();
			data[1] = (float)this.offset.Imaginary.ToDouble();
			data[2] = (float)this.pixelSize.ToDouble();

			this.parameterBuffer.Set(data);

			// create C and Z buffers.
			const uint totalSize = 512 * localSize;

			if (this.cBuffer == null) this.cBuffer = new GLBuffer(this.context, (IntPtr)(totalSize * 16));
			if (this.zBuffer == null) this.zBuffer = new GLBuffer(this.context, (IntPtr)(totalSize * 16));

			return totalSize;
		}
 
		#endregion
	}
}
