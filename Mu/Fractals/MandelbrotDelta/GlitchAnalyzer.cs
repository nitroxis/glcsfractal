﻿using System;
using System.Globalization;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// A class to analyze the gltiches that occur during delta iteration.
	/// </summary>
	public sealed class GlitchAnalyzer : DisposableObject
	{
		#region Constants

		private const int groupSizeX = 32;
		private const int groupSizeY = 32;

		#endregion

		#region Fields

		private readonly GLContext context;
		private readonly GLProgram program;
		private readonly GLBuffer buffer;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GlitchAnalyzer.
		/// </summary>
		public GlitchAnalyzer(GLContext context)
		{
			this.context = context;
			this.program = new GLProgram(context, "GlitchAnalyzer");
			this.program.Attach(ShaderType.ComputeShader, Helper.GetCode("GlitchAnalyzer")
				.Replace("GROUPSIZEX", groupSizeX.ToString(CultureInfo.InvariantCulture))
				.Replace("GROUPSIZEY", groupSizeY.ToString(CultureInfo.InvariantCulture)));
			this.program.Link();

			this.buffer = new GLBuffer(context, (IntPtr)16);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Searches the specified texture for glitches and tries to guess a new reference point based on the glitch blobs.
		/// </summary>
		/// <param name="texture">The texture to analyze.</param>
		/// <param name="positionsBuffer">A buffer that receives the positions of glitched pixels.</param>
		/// <param name="width">The width of the texture.</param>
		/// <param name="height">The height of the texture.</param>
		/// <param name="x">The output X coordinate of the largest blob.</param>
		/// <param name="y">The output Y coordinate of the largest blob.</param>
		/// <returns>The number of glitches found in the image.</returns>
		public uint Analyze(GLTexture texture, GLBuffer positionsBuffer, int width, int height, out uint x, out uint y)
		{
			uint[] data = new uint[4];
			
			this.context.Invoke(() =>
			{
				// reset output buffer.
				this.buffer.Set(data);

				// set up the analyze program.
				this.program.Use();
				positionsBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 0);
				this.buffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 1);
				texture.BindImage(0, TextureAccess.ReadOnly, SizedInternalFormat.R32ui);
				
				// compute for whole image.
				GL.DispatchCompute((width + groupSizeX - 1) / groupSizeX, (height + groupSizeY - 1) / groupSizeY, 1);

				GL.MemoryBarrier(MemoryBarrierFlags.ShaderStorageBarrierBit);
				
				// get results.
				this.buffer.Get(data);
			});

			x = data[0];
			y = data[1];
			return data[3];
		}

		protected override void Dispose(bool managed)
		{
			this.program.Dispose();
			this.buffer.Dispose();
		}

		#endregion
	}
}
