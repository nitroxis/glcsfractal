#version 430 core
layout (local_size_x = GROUPSIZE) in;

uniform uint numInvokations; // remaining number of pixels.
uniform uint numIterations; // maximum number of iterations.

layout (r32ui, binding = 0) uniform uimage2D outputImage; // output image.

struct pixelInfo
{
	dvec2 D0; // distance to reference point.
	dvec2 D; // current delta iteration value.
};

layout (std430, binding = 0) readonly buffer buffer0 { uint positions[]; }; // positions in the complex plane, aka C.
layout (std430, binding = 1) buffer buffer1 { pixelInfo state[]; }; // distance to reference point.
layout (std430, binding = 2) readonly buffer buffer2 { dvec2 referenceOrbit[]; }; // the orbit of the reference point.

// complex multiplication a * b
dvec2 complexMul(dvec2 a, dvec2 b)
{
	return dvec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

// complex square a ^ 2
dvec2 complexSqr(dvec2 a)
{
	return dvec2((a.x + a.y) * (a.x - a.y), 2.0 * a.x * a.y);
}

void main(void)
{
	uint id = gl_GlobalInvocationID.x;

	// check bounds.
	if(id >= numInvokations)
		return;

	// load current iteration count.
	ivec2 position = ivec2(positions[id] & 0xFFFF, positions[id] >> 16);
	uint i = int(imageLoad(outputImage, position).x);
	if(i == uint(-1)) // already glitched, there's no point in continuing.
		return;

	// read D0 and D from input buffers and iterate.
	dvec2 D0 = state[id].D0;
	dvec2 D = state[id].D;
	
	// delta iteration.
	while(i < numIterations)
	{
		dvec2 X = referenceOrbit[i]; // the reference point, X[n].
		dvec2 Y = X + D; // calulate the actual point, i.e. Y[n] = X[n] + delta[n]
		double Ymag2 = dot(Y, Y); // |Y[n]|^2

		// check bailout.
		if(Ymag2 > 4.0)
			break;

		// glitch detection.
		if(dot(X, X) * GLITCHTHRESHOLD > Ymag2)
		{
			i = uint(-1);
			break;
		}

		// iterate delta.
		D = complexMul(dvec2(2.0, 0.0), complexMul(X, D)) + complexSqr(D) + D0;

		i++;
	}

	// store result.
	state[id].D = D;
	imageStore(outputImage, position, uvec4(i));
}

