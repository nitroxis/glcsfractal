﻿using System;
using OpenTK;

namespace Mu
{
	/// <summary>
	/// Represents a reference point.
	/// </summary>
	internal sealed class ReferencePoint
	{
		#region Fields

		private readonly FixedComplex position;
		private uint iterations;
		private uint maxIterations;
		private Vector2d[] orbit;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the position of the reference point.
		/// </summary>
		public FixedComplex Position
		{
			get { return this.position; }
		}

		/// <summary>
		/// Gets the reference orbit as two-dimensional double-precision vector array.
		/// </summary>
		public Vector2d[] Orbit
		{
			get { return this.orbit; }
		}

		/// <summary>
		/// Gets the number of iterations until the point bailed out.
		/// </summary>
		public uint Iterations
		{
			get { return this.iterations; }
		}

		/// <summary>
		/// Gets the maximum number of iterations.
		/// </summary>
		public uint MaxIterations
		{
			get { return this.maxIterations; }
			set { this.maxIterations = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ReferencePoint.
		/// </summary>
		public ReferencePoint(FixedComplex position, uint maxIterations)
		{
			this.position = FixedComplex.Create(position.Format);
			this.position.Real.Set(position.Real);
			this.position.Imaginary.Set(position.Imaginary);

			this.maxIterations = maxIterations;
		}

		/// <summary>
		/// Creates a new ReferencePoint.
		/// </summary>
		public ReferencePoint(ReferencePoint other)
			: this(other.position, other.maxIterations)
		{
			
		}

		#endregion

		#region Methods

		/// <summary>
		/// Iterates the reference point and stores all values in an array.
		/// </summary>
		/// <returns></returns>
		public void Calculate()
		{
			FixedFormat format = this.position.Format;

			FixedComplex Z = FixedComplex.Create(format);
			FixedComplex ZZ = FixedComplex.Create(format);
			Fixed a = Fixed.Create(format);
			Fixed bailOut = Fixed.Create(format);
			bailOut.Set(4);
		
			this.orbit = new Vector2d[this.maxIterations];

			uint i = 0;
			this.iterations = uint.MaxValue;
			
			// high precision mandelbrot calculation.
			while (i < this.maxIterations)
			{
				
				// a = |Z| ^ 2
				Fixed.Square(Z.Real, ZZ.Real);
				Fixed.Square(Z.Imaginary, ZZ.Imaginary);
				Fixed.Add(ZZ.Real, ZZ.Imaginary, a);

				// check bailout.
				if (this.iterations == uint.MaxValue && a >= bailOut)
					this.iterations = i;
				
				// Z.i = ((Z.r + Z.i) ^ 2 - Z.r ^ 2 - Z.i ^ 2) + C.i
				Fixed.Add(Z.Real, Z.Imaginary, a);
				Fixed.Square(a, Z.Imaginary);
				Fixed.Subtract(Z.Imaginary, ZZ.Real, Z.Imaginary);
				Fixed.Subtract(Z.Imaginary, ZZ.Imaginary, Z.Imaginary);
				Fixed.Add(Z.Imaginary, this.position.Imaginary, Z.Imaginary);

				// Z.r = (Z.r ^ 2 - Z.i ^ 2) + C.r
				Fixed.Subtract(ZZ.Real, ZZ.Imaginary, Z.Real);
				Fixed.Add(Z.Real, this.position.Real, Z.Real);

				this.orbit[i].X = Z.Real.ToDouble();
				this.orbit[i].Y = Z.Imaginary.ToDouble();

				// increment iteration counter.
				i++;
			}

			// point didn't escape:
			if (this.iterations == uint.MaxValue)
				this.iterations = this.maxIterations;
		}

		/// <summary>
		/// Calculates the series approximation coefficients at high precision and returns the values at double precision.
		/// </summary>
		/// <param name="approxIterations"></param>
		/// <param name="numCoefficients"></param>
		/// <param name="scale"></param>
		/// <returns></returns>
		public Vector2d[] GetApproximationCoefficients(uint approxIterations, int numCoefficients, Fixed scale)
		{
			FixedFormat format = this.position.Format;

			FixedComplex c0 = FixedComplex.Create(format, this.position);
			FixedComplex z = FixedComplex.Create(format, c0);

			FixedComplex[] x = new FixedComplex[numCoefficients];
			for (int i = 0; i < numCoefficients; i++)
				x[i] = FixedComplex.Create(format, i == 0 ? 1 : 0, 0);

			FixedComplex temp = FixedComplex.Create(format);
			FixedComplex temp2 = FixedComplex.Create(format);
			Fixed two = Fixed.Create(format);
			Fixed one = Fixed.Create(format);
			two.Set(2);
			one.Set(1);
			for (uint n = 0; n < approxIterations; n++)
			{
				for (int i = numCoefficients - 1; i >= 0; i--)
				{
					// X = z + x[n]
					FixedComplex.Multiply(z, x[i], temp); 
					for (int j = 0; j < i / 2; j++)
					{
						// X += x[j] * x[i-j-1]
						FixedComplex.Multiply(x[j], x[i - j - 1], temp2);
						FixedComplex.Add(temp, temp2, temp);
					}

					// X *= 2
					FixedComplex.Multiply(temp, two, temp);

					if ((i & 1) == 1)
					{
						// X += x[i/2] ^ 2
						FixedComplex.Square(x[i / 2], temp2);
						FixedComplex.Add(temp, temp2, temp);
					}

					// x[n] = X
					x[i].Set(temp);
				}

				// x[0] += 1
				FixedComplex.Add(x[0], one, x[0]);

				// iterate.
				FixedComplex.Square(z, z);
				FixedComplex.Add(z, c0, z);
			}

			// scale and convert.
			Fixed r = Fixed.Create(format);
			r.Set(scale);

			Vector2d[] coefficients = new Vector2d[numCoefficients];
			for (int i = 0; i < numCoefficients; i++)
			{
				FixedComplex.Multiply(x[i], r, temp);
				Fixed.Multiply(r, scale, r);

				coefficients[i].X = temp.Real.ToDouble();
				coefficients[i].Y = temp.Imaginary.ToDouble();

				//Console.WriteLine("{0} = {1}\t{2}", (char)('a' + i), coefficients[i], x[i]);
			}

			return coefficients;
		}

		/// <summary>
		/// Finds the period of a point in the complex plane.
		/// </summary>
		/// <param name="c0">The starting point.</param>
		/// <param name="radius">The radius of the region.</param>
		/// <param name="maxIterations">The maximum number of iterations.</param>
		/// <returns>The period of the poing.</returns>
		public static uint FindPeriod(FixedComplex c0, Fixed radius, uint maxIterations)
		{
			FixedFormat format = c0.Format;

			FixedComplex[] c, z;
			c = new FixedComplex[4];
			z = new FixedComplex[4];

			for (int i = 0; i < 4; i++)
			{
				c[i] = FixedComplex.Create(format);
				z[i] = FixedComplex.Create(format);
			}

			FixedComplex d = FixedComplex.Create(format);
			Fixed temp1 = Fixed.Create(format);
			Fixed temp2 = Fixed.Create(format);
			Fixed zero = Fixed.Create(format);
			zero.Set(0);
			Fixed.Negate(radius, temp1);

			// rectangle from (-r,-r) to (r,r)
			c[0].Set(temp1, temp1);
			c[1].Set(radius, temp1);
			c[2].Set(radius, radius);
			c[3].Set(temp1, radius);

			// add offset.
			for (int i = 0; i < 4; i++)
				FixedComplex.Add(c0, c[i], c[i]);

			for (uint p = 1; p < maxIterations; p++)
			{
				for (int i = 0; i < 4; i++)
				{
					FixedComplex.Multiply(z[i], z[i], z[i]);
					FixedComplex.Add(z[i], c[i], z[i]);
				}

				int preal = 0;
				for (int i = 0; i < 4; i++)
				{
					int j = (i + 1) % 4;
					if (!(z[i].Imaginary < zero && z[j].Imaginary < zero) &&
					    !(z[i].Imaginary > zero && z[j].Imaginary > zero))
					{
						FixedComplex.Subtract(z[j], z[i], d);

						Fixed.Multiply(d.Imaginary, z[i].Real, temp1);
						Fixed.Multiply(d.Real, z[i].Imaginary, temp2);
						Fixed.Subtract(temp1, temp2, temp1);
						Fixed.Multiply(temp1, d.Imaginary, temp1);

						if (temp1 > zero)
							preal++;
					}
				}

				if ((preal & 1) != 0)
					return p;
			}

			return 0;
		}


		#endregion
	}
}
