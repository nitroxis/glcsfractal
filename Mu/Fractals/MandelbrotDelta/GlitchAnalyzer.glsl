#version 430 core

layout (local_size_x = GROUPSIZEX, local_size_y = GROUPSIZEY) in;

// output of compute shader.
layout (r32ui, binding = 0) uniform readonly uimage2D map;

layout (std430, binding = 0) writeonly buffer buffer0 { uint positions[]; }; // positions in the complex plane, aka C.

layout (std430, binding = 1) buffer buffer1
{
	ivec2 currentPoint;
	int currentRadius;
	uint numGlitches;
};

bool isGlitch(ivec2 pos)
{
	return imageLoad(map, pos).r == uint(-1);
}

int getRadius(ivec2 p)
{
	// try to find radius using 8 samples in a rectangle.
	const int maxRadius = 512;
	for(int r = 1; r < maxRadius; r++)
	{
		if(!isGlitch(ivec2(p.x - r, p.y)) || 
		   !isGlitch(ivec2(p.x + r, p.y)) || 
		   !isGlitch(ivec2(p.x, p.y - r)) || 
		   !isGlitch(ivec2(p.x, p.y + r)))
			return r * r;

		if(!isGlitch(ivec2(p.x - r, p.y - r)) || 
		   !isGlitch(ivec2(p.x + r, p.y - r)) || 
		   !isGlitch(ivec2(p.x - r, p.y + r)) || 
		   !isGlitch(ivec2(p.x + r, p.y + r)))
			return 2 * r * r;
	}

	return maxRadius;
}

void main(void)
{
	ivec2 p = ivec2(gl_GlobalInvocationID);

	// check bounds and whether the pixel is a glitch.
	ivec2 size = imageSize(map);
	if(p.x >= size.x || p.y >= size.y || !isGlitch(p))
		return;

	// save position of pixel.
	positions[atomicAdd(numGlitches, 1)] = (uint(p.x) & 0xFFFF) | (uint(p.y) << 16);

	// get radius of blob.
	int r = getRadius(p);
	
	// if our blob radius is bigger than the current blob, replace it.
	if(r > atomicMax(currentRadius, r))
		currentPoint.xy = p;	
}