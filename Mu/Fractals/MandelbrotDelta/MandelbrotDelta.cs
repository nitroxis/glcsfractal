﻿using System;
using System.Collections.Generic;
using System.Globalization;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Computes the Mandelbrot set using the perturbation technique and series approximation.
	/// </summary>
	public sealed class MandelbrotDelta : GLAlgorithm
	{
		#region Constants

		private const int localSize = 448;
		private const int globalSize = 64;
		private const int totalUnits = localSize * globalSize;

		private const int numCoefficients = 12;

		#endregion

		#region Fields

		private GLProgram initProgram;
		private GLProgram iterateProgram;
		private GLBuffer positionsBuffer; // contains the X, Y positions of pixels.
		private GLBuffer stateBuffer;
		private GLBuffer referenceOrbitBuffer;
		private readonly GLBuffer coefficientsBuffer;

		private readonly double glitchThreshold;
		private readonly GlitchAnalyzer glitchAnalyzer;

		private uint approximatedIterations;

		private readonly List<ReferencePoint> cachedReferencePoints;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the number of iterations to approximate.
		/// </summary>
		public uint ApproximatedIterations
		{
			get { return this.approximatedIterations; }
			set { this.approximatedIterations = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MandelbrotDelta.
		/// </summary>
		public MandelbrotDelta(GLContext context)
			: base(context)
		{
			this.glitchThreshold = 1.0d / (1 << 16);
			
			this.createPrograms();
			this.generatePositions();
			this.createReferenceOrbitBuffer();
			this.coefficientsBuffer = new GLBuffer(context, (IntPtr)(numCoefficients * 16));
			this.glitchAnalyzer = new GlitchAnalyzer(context);
			
			this.cachedReferencePoints = new List<ReferencePoint>();
		}

		#endregion

		#region Methods

		private void generatePositions()
		{
			if (this.positionsBuffer != null)  this.positionsBuffer.Dispose();

			// generate interlace pattern.
			this.positionsBuffer = new GLBuffer(this.context, (IntPtr)(this.Width * this.Height * 4));
		}

		private void createReferenceOrbitBuffer()
		{
			if (this.referenceOrbitBuffer != null) this.referenceOrbitBuffer.Dispose();

			this.referenceOrbitBuffer = new GLBuffer(this.context, (IntPtr)(this.MaxIterations * 16));
		}

		private void disposePrograms()
		{
			if (this.initProgram != null) this.initProgram.Dispose();
			if (this.iterateProgram != null) this.iterateProgram.Dispose();
		}

		private void createPrograms()
		{
			this.disposePrograms();
			
			// init shader.
			string initSource = Helper.GetCode("MandelbrotDeltaInit", s => s.Replace("GROUPSIZE", localSize.ToString(CultureInfo.InvariantCulture)));
			this.initProgram = new GLProgram(this.context, "MandelbrotDeltaInit");
			this.initProgram.Attach(ShaderType.ComputeShader, initSource);
			this.initProgram.Link();

			// compute shader.
			string iterateSource = Helper.GetCode("MandelbrotDelta", s => s
				.Replace("GROUPSIZE", localSize.ToString(CultureInfo.InvariantCulture))
				.Replace("GLITCHTHRESHOLD", this.glitchThreshold.ToString(CultureInfo.InvariantCulture)));
			this.iterateProgram = new GLProgram(this.context, "MandelbrotDelta");
			this.iterateProgram.Attach(ShaderType.ComputeShader, iterateSource);
			this.iterateProgram.Link();
		}

		protected override void OnSizeChanged()
		{
			base.OnSizeChanged();

			if (this.context != null)
				this.generatePositions();
		}

		protected override void OnMaxIterationsChanged()
		{
			base.OnMaxIterationsChanged();

			if (this.context != null)
				this.createReferenceOrbitBuffer();

			if (this.cachedReferencePoints != null && this.cachedReferencePoints.Count > 0)
			{
				// only keep the best reference point.
				this.cachedReferencePoints.Sort((a, b) => b.Iterations.CompareTo(a.Iterations));
				this.cachedReferencePoints.RemoveRange(1, this.cachedReferencePoints.Count - 1);

				// update reference point.
				this.cachedReferencePoints[0].MaxIterations = this.MaxIterations;
				this.cachedReferencePoints[0].Calculate();
			}
		}

		private Vector2d getReferencePointOffset(ReferencePoint refPoint)
		{
			Vector2d refOffset;
			Fixed temp = Fixed.Create(this.fixedFormat);

			Fixed.Subtract(this.center.Real, refPoint.Position.Real, temp);
			refOffset.X = temp.ToDouble();

			Fixed.Subtract(this.center.Imaginary, refPoint.Position.Imaginary, temp);
			refOffset.Y = temp.ToDouble();

			return refOffset;
		}

		private void pass(ReferencePoint refPoint, uint maxUnits)
		{
			this.context.Invoke(() =>
			{
				this.Texture.BindImage(0, TextureAccess.ReadWrite, SizedInternalFormat.R32ui);
				this.stateBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 1);

				// upload reference orbit
				this.referenceOrbitBuffer.Set(refPoint.Orbit);
				this.referenceOrbitBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 2);
				this.coefficientsBuffer.Set(refPoint.GetApproximationCoefficients(this.approximatedIterations, numCoefficients, this.radius), 0, 0, numCoefficients);
				this.coefficientsBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 3);

				// setup init program.
				this.initProgram.Use();
				Vector2d offsetRel = this.getReferencePointOffset(refPoint);
				GL.Uniform2(this.initProgram.GetLocation("offset"), offsetRel.X, offsetRel.Y);
				GL.Uniform1(this.initProgram.GetLocation("pixelSize"), this.pixelSize.ToDouble());
				GL.Uniform1(this.initProgram.GetLocation("approximatedIterations"), this.approximatedIterations);
				GL.Uniform2(this.initProgram.GetLocation("size"), this.Width, this.Height);

				uint globalOffset = 0;
				while (globalOffset < maxUnits)
				{
					if (this.IsCanceled) return;

					uint numInvokations = Math.Min(totalUnits, maxUnits - globalOffset);
					uint numIterations = 2048u;
					uint globalX = (numInvokations + localSize - 1) / localSize;

					// bind sub-region of the position buffer.
					this.positionsBuffer.BindRange(BufferRangeTarget.ShaderStorageBuffer, 0, globalOffset * 4u, numInvokations * 4u);

					// dispatch compute shader that calculates D0 and D for the current region.
					this.initProgram.Use();
					GL.Uniform1(this.initProgram.GetLocation("numInvokations"), numInvokations);
					GL.DispatchCompute(globalX, 1, 1);

					// iterate.
					this.iterateProgram.Use();
					GL.Uniform1(this.iterateProgram.GetLocation("numInvokations"), numInvokations);
					GL.Uniform1(this.iterateProgram.GetLocation("numIterations"), numIterations);

					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);
					uint iterationOffset = this.approximatedIterations;
					while (iterationOffset < this.MaxIterations)
					{
						if (this.IsCanceled) return;

						// check if number of iterations exceeds max iterations.
						if (numIterations > this.MaxIterations - iterationOffset)
							numIterations = this.MaxIterations - iterationOffset;

						GL.Uniform1(this.iterateProgram.GetLocation("numIterations"), iterationOffset + numIterations);

						// dispatch.
						GL.DispatchCompute(globalX, 1, 1);
						iterationOffset += numIterations;
					}

					// report progress.
					GL.MemoryBarrier(MemoryBarrierFlags.ShaderImageAccessBarrierBit | MemoryBarrierFlags.ShaderStorageBarrierBit);
					this.OnComputeProgress(EventArgs.Empty);
					globalOffset += numInvokations;
				}
			});
		}

		private Vector2 toScreenSpace(FixedComplex c)
		{
			Vector2 position;

			Fixed temp = Fixed.Create(this.fixedFormat);

			temp.Set(c.Real);
			Fixed.Subtract(temp, this.offset.Real, temp);
			Fixed.Divide(temp, this.size.Real, temp);
			position.X = (float)temp.ToDouble() * 2.0f - 1.0f;

			temp.Set(c.Imaginary);
			Fixed.Subtract(temp, this.offset.Imaginary, temp);
			Fixed.Divide(temp, this.size.Imaginary, temp);
			position.Y = (float)temp.ToDouble() * 2.0f - 1.0f;

			return position;
		}

		private FixedComplex toComplexPlane(uint x, uint y)
		{
			FixedComplex newPosition = FixedComplex.Create(this.fixedFormat, x, y);

			// ref.r = offset.r + delta * x
			Fixed.Multiply(newPosition.Real, this.pixelSize, newPosition.Real);
			Fixed.Add(newPosition.Real, this.offset.Real, newPosition.Real);

			// ref.i = offset.i + delta * y
			Fixed.Multiply(newPosition.Imaginary, this.pixelSize, newPosition.Imaginary);
			Fixed.Add(newPosition.Imaginary, this.offset.Imaginary, newPosition.Imaginary);

			return newPosition;
		}

		private void removeOutOfBoundsReferences()
		{
			// max = offset + size.
			Fixed maxReal = Fixed.Create(this.fixedFormat, this.offset.Real);
			Fixed maxImag = Fixed.Create(this.fixedFormat, this.offset.Imaginary);
			Fixed.Add(maxReal, this.size.Real, maxReal);
			Fixed.Add(maxImag, this.size.Imaginary, maxImag);

			for (int i = this.cachedReferencePoints.Count - 1; i >= 0; i--)
			{
				ReferencePoint p = this.cachedReferencePoints[i];

				// check whether the reference point is out of bounds.
				if (p.Position.Real < this.offset.Real || p.Position.Imaginary < this.offset.Imaginary ||
				    p.Position.Real > maxReal || p.Position.Imaginary > maxImag)
				{
					this.cachedReferencePoints.RemoveAt(i);
				}
			}
		}

		protected override void Run()
		{
			this.Clear();

			if (this.IsCanceled) return;

			if (this.stateBuffer == null) this.stateBuffer = new GLBuffer(this.context, (IntPtr)(totalUnits * 32));
			this.positionsBuffer.Set(Interlace.Centered(this.Width, this.Height));

			this.removeOutOfBoundsReferences();

			ReferencePoint refPoint;

			// if there are no cached reference point, we'll just use the center.
			if (this.cachedReferencePoints.Count == 0)
			{
				refPoint = new ReferencePoint(this.center, this.MaxIterations);
				refPoint.Calculate();
			}
			else
			{
				this.cachedReferencePoints.Sort((a, b) => b.Iterations.CompareTo(a.Iterations));
				refPoint = this.cachedReferencePoints[0];
			}

			this.Markers.Add(new Marker(this.toScreenSpace(refPoint.Position), 10.0f, Color4.Lime));

			// clear all references.
			this.cachedReferencePoints.Clear();

			uint numInvokations = (uint)this.Width * (uint)this.Height;
			while (true)
			{
				if (this.IsCanceled) return;

				// add reference.
				this.cachedReferencePoints.Add(refPoint);

				// compute using that reference.
				this.pass(refPoint, numInvokations);

				// find glitches.
				uint x, y;
				uint numGlitches = this.glitchAnalyzer.Analyze(this.Texture, this.positionsBuffer, this.Width, this.Height, out x, out y);
				if (numGlitches == 0)
					break;

				// create and calculate new reference.
				refPoint = new ReferencePoint(this.toComplexPlane(x, y), this.MaxIterations);
				refPoint.Calculate();

				this.Markers.Add(new Marker(this.toScreenSpace(refPoint.Position), 7.0f, Color4.ForestGreen));

				// we only need as many invokations as there are glitches.
				numInvokations = numGlitches;
			}
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			if (this.positionsBuffer != null) this.positionsBuffer.Dispose();
			if (this.stateBuffer != null) this.stateBuffer.Dispose();
			if (this.referenceOrbitBuffer != null) this.referenceOrbitBuffer.Dispose();
			if (this.coefficientsBuffer != null) this.coefficientsBuffer.Dispose();

			this.disposePrograms();

			this.glitchAnalyzer.Dispose();
		}

		#endregion
	}
}
