#version 430 core
layout (local_size_x = GROUPSIZE) in;

uniform uint numInvokations; // number of pixels.
uniform dvec2 offset; // offset in the complex plane.
uniform double pixelSize; // distance between two pixels.
uniform ivec2 size;
uniform uint approximatedIterations; // the number of iterations approximated by the coefficients.

layout (r32ui, binding = 0) uniform uimage2D outputImage; // output image.

struct pixelInfo
{
	dvec2 D0; // distance to reference point.
	dvec2 D; // current delta iteration value.
};

layout (std430, binding = 0) readonly buffer buffer0 { uint positions[]; }; // pixel positions.
layout (std430, binding = 1) writeonly buffer buffer1 { pixelInfo state[]; }; 
layout (std430, binding = 3) readonly buffer buffer3 { dvec2 coefficients[]; }; // series approximation coefficients.

// complex multiplication a * b
dvec2 complexMul(dvec2 a, dvec2 b)
{
	return dvec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

// complex square a ^ 2
dvec2 complexSqr(dvec2 a)
{
	return dvec2((a.x + a.y) * (a.x - a.y), 2.0 * a.x * a.y);
}

void main(void)
{
	uint id = gl_GlobalInvocationID.x;

	// check bounds.
	if(id >= numInvokations)
		return;

	// find out x and y coordinate of the pixel.
	ivec2 position = ivec2(positions[id] & 0xFFFF, positions[id] >> 16);

	// approximation.
	dvec2 relativePosition = dvec2(position - size / 2);
	dvec2 Z0 = (offset / pixelSize + relativePosition) / double(size.y / 2);
	dvec2 Z = Z0;
	dvec2 D = dvec2(0.0);

	// Dn = C0 * D0 + C1 * D0^2 + C2 * D0^3 + ... + C(n-1) * D0^n
	for(int i = 0; i < 3; i++)
	{
		D += complexMul(coefficients[i], Z);
		Z = complexMul(Z0, Z);
	}
	
	// store D0 and D in the buffer.
	dvec2 D0 = offset + relativePosition * pixelSize;
	state[id].D0 = D0;
	state[id].D = D;
	
	// store initial approximated iterations.
	imageStore(outputImage, position, uvec4(approximatedIterations));
}