﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Mu
{
	/// <summary>
	/// Utility class.
	/// </summary>
	internal static class StreamExtensions
	{
		/// <summary>
		/// The default encoding for read and write operations with strings.
		/// </summary>
		public static readonly Encoding DefaultEncoding = Encoding.UTF8;

		#region Reading

		/// <summary>
		/// Reads a specific number of bytes from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="length">The number of bytes to read.</param>
		/// <returns></returns>
		public static byte[] Read(this Stream stream, int length)
		{
			byte[] buffer = new byte[length];
			if (stream.Read(buffer, 0, length) != length)
				throw new EndOfStreamException();

			return buffer;
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an EndOfStreamException if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public static void ReadBlocking(this Stream stream, byte[] buffer, int offset, int count)
		{
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
					throw new EndOfStreamException();

				count -= numBytes;
				offset += numBytes;
			}
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an EndOfStreamException if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static byte[] ReadBlocking(this Stream stream, int length)
		{
			byte[] buffer = new byte[length];
			stream.ReadBlocking(buffer, 0, length);
			return buffer;
		}

		/// <summary>
		/// Reads from the specified stream until the stream ends.
		/// </summary>
		/// <param name="stream"></param>
		public static void ReadToEnd(this Stream stream)
		{
			byte[] temp = new byte[4096];

			while (stream.Read(temp, 0, temp.Length) > 0) ;
		}

		/// <summary>
		/// Reads a structure or formatted class from a buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static T ReadStructure<T>(this byte[] buffer, int offset)
		{
			GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
			T value = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject() + offset, typeof(T));
			handle.Free();
			return value;
		}

		/// <summary>
		/// Reads a structure or formatted class from a stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static T ReadStructure<T>(this Stream stream)
		{
			int size = Marshal.SizeOf(typeof(T));
			byte[] data = stream.ReadBlocking(size);
			return data.ReadStructure<T>(0);
		}

		/// <summary>
		/// Reads a string of the specified length from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string ReadString(this Stream stream, int length)
		{
			return stream.ReadString(DefaultEncoding, length);
		}
		
		/// <summary>
		/// Reads a string of the specified length from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string ReadString(this Stream stream, Encoding encoding, int length)
		{
			return encoding.GetString(stream.ReadBlocking(length));
		}
		
		#region Little Endian

		/// <summary>
		/// Reads a 16 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16L(this byte[] buffer, int offset)
		{
			return (short)(buffer[offset] | (buffer[offset + 1] << 8));
		}

		/// <summary>
		/// Reads a 16 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(2);
			return (short)(buffer[0] | (buffer[1] << 8));
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16L(this byte[] buffer, int offset)
		{
			return (ushort)(buffer[offset] | (buffer[offset + 1] << 8));
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(2);
			return (ushort)(buffer[0] | (buffer[1] << 8));
		}

		/// <summary>
		/// Reads a 32 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32L(this byte[] buffer, int offset)
		{
			return buffer[offset] | (buffer[offset + 1] << 8) | (buffer[offset + 2] << 16) | (buffer[offset + 3] << 24);
		}

		/// <summary>
		/// Reads a 32 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);
			return buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
		}

		/// <summary>
		/// Reads a 32 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32L(this byte[] buffer, int offset)
		{
			return (uint)buffer[offset] | ((uint)buffer[offset + 1] << 8) | ((uint)buffer[offset + 2] << 16) | ((uint)buffer[offset + 3] << 24);
		}

		/// <summary>
		/// Reads a 32 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);
			return (uint)buffer[0] | ((uint)buffer[1] << 8) | ((uint)buffer[2] << 16) | ((uint)buffer[3] << 24);
		}


		/// <summary>
		/// Reads a 64 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64L(this byte[] buffer, int offset)
		{
			return (long)buffer[offset] | ((long)buffer[offset + 1] << 8) | ((long)buffer[offset + 2] << 16) | ((long)buffer[offset + 3] << 24) | ((long)buffer[offset + 4] << 32) | ((long)buffer[offset + 5] << 40) | ((long)buffer[offset + 6] << 48) | ((long)buffer[offset + 7] << 56);
		}

		/// <summary>
		/// Reads a 64 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);
			return (long)buffer[0] | ((long)buffer[1] << 8) | ((long)buffer[2] << 16) | ((long)buffer[3] << 24) | ((long)buffer[4] << 32) | ((long)buffer[5] << 40) | ((long)buffer[6] << 48) | ((long)buffer[7] << 56);
		}

		/// <summary>
		/// Reads a 64 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64L(this byte[] buffer, int offset)
		{
			return (ulong)buffer[offset] | ((ulong)buffer[offset + 1] << 8) | ((ulong)buffer[offset + 2] << 16) | ((ulong)buffer[offset + 3] << 24) | ((ulong)buffer[offset + 4] << 32) | ((ulong)buffer[offset + 5] << 40) | ((ulong)buffer[offset + 6] << 48) | ((ulong)buffer[offset + 7] << 56);
		}


		/// <summary>
		/// Reads a 64 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);
			return (ulong)buffer[0] | ((ulong)buffer[1] << 8) | ((ulong)buffer[2] << 16) | ((ulong)buffer[3] << 24) | ((ulong)buffer[4] << 32) | ((ulong)buffer[5] << 40) | ((ulong)buffer[6] << 48) | ((ulong)buffer[7] << 56);
		}

		/// <summary>
		/// Reads a 32 bit floating point value from a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32L(this byte[] buffer, int offset)
		{
			if (BitConverter.IsLittleEndian)
				return BitConverter.ToSingle(buffer, offset);

			// reverse.
			byte[] temp = new byte[4];
			temp[0] = buffer[offset + 3];
			temp[1] = buffer[offset + 2];
			temp[2] = buffer[offset + 1];
			temp[3] = buffer[offset];
			return BitConverter.ToSingle(temp, 0);
		}
		
		/// <summary>
		/// Reads a 32 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		/// <summary>
		/// Reads a 64 bit floating point value from a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64L(this byte[] buffer, int offset)
		{
			if (BitConverter.IsLittleEndian)
				return BitConverter.ToDouble(buffer, offset);

			// reverse.
			byte[] temp = new byte[8];
			temp[0] = buffer[offset + 7];
			temp[1] = buffer[offset + 6];
			temp[2] = buffer[offset + 5];
			temp[3] = buffer[offset + 4];
			temp[4] = buffer[offset + 3];
			temp[5] = buffer[offset + 2];
			temp[6] = buffer[offset + 1];
			temp[7] = buffer[offset];
			return BitConverter.ToDouble(temp, 0);
		}

		/// <summary>
		/// Reads a 64 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64L(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream)
		{
			return stream.ReadStringL(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32L();
			if (length < 0)
				return null;

			return stream.ReadString(encoding, length);
		}

		#endregion

		#region Big Endian

		/// <summary>
		/// Reads a 16 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16B(this byte[] buffer, int offset)
		{
			return (short)(buffer[offset + 1] | (buffer[offset] << 8));
		}

		/// <summary>
		/// Reads a 16 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(2);
			return (short)(buffer[1] | (buffer[0] << 8));
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16B(this byte[] buffer, int offset)
		{
			return (ushort)(buffer[offset + 1] | (buffer[offset + 0] << 8));
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(2);
			return (ushort)(buffer[1] | (buffer[0] << 8));
		}

		/// <summary>
		/// Reads a 32 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32B(this byte[] buffer, int offset)
		{
			return buffer[offset + 3] | (buffer[offset + 2] << 8) | (buffer[offset + 1] << 16) | (buffer[offset] << 24);
		}

		/// <summary>
		/// Reads a 32 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);
			return buffer[3] | (buffer[2] << 8) | (buffer[1] << 16) | (buffer[0] << 24);
		}

		/// <summary>
		/// Reads a 32 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32B(this byte[] buffer, int offset)
		{
			return (uint)buffer[offset + 3] | ((uint)buffer[offset + 2] << 8) | ((uint)buffer[offset + 1] << 16) | ((uint)buffer[offset] << 24);
		}

		/// <summary>
		/// Reads a 32 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);
			return (uint)buffer[3] | ((uint)buffer[2] << 8) | ((uint)buffer[1] << 16) | ((uint)buffer[0] << 24);
		}


		/// <summary>
		/// Reads a 64 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64B(this byte[] buffer, int offset)
		{
			return (long)buffer[offset + 7] | ((long)buffer[offset + 6] << 8) | ((long)buffer[offset + 5] << 16) | ((long)buffer[offset + 4] << 24) | ((long)buffer[offset + 3] << 32) | ((long)buffer[offset + 2] << 40) | ((long)buffer[offset + 1] << 48) | ((long)buffer[offset] << 56);
		}

		/// <summary>
		/// Reads a 64 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);
			return (long)buffer[7] | ((long)buffer[6] << 8) | ((long)buffer[5] << 16) | ((long)buffer[4] << 24) | ((long)buffer[3] << 32) | ((long)buffer[2] << 40) | ((long)buffer[1] << 48) | ((long)buffer[0] << 56);
		}

		/// <summary>
		/// Reads a 64 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64B(this byte[] buffer, int offset)
		{
			return (ulong)buffer[offset + 7] | ((ulong)buffer[offset + 6] << 8) | ((ulong)buffer[offset + 5] << 16) | ((ulong)buffer[offset + 4] << 24) | ((ulong)buffer[offset + 3] << 32) | ((ulong)buffer[offset + 2] << 40) | ((ulong)buffer[offset + 1] << 48) | ((ulong)buffer[offset] << 56);
		}

		/// <summary>
		/// Reads a 64 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);
			return (ulong)buffer[7] | ((ulong)buffer[6] << 8) | ((ulong)buffer[5] << 16) | ((ulong)buffer[4] << 24) | ((ulong)buffer[3] << 32) | ((ulong)buffer[2] << 40) | ((ulong)buffer[1] << 48) | ((ulong)buffer[0] << 56);
		}

		/// <summary>
		/// Reads a 32 bit floating point value from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32B(this byte[] buffer, int offset)
		{
			if (!BitConverter.IsLittleEndian)
				return BitConverter.ToSingle(buffer, offset);

			// reverse.
			byte[] temp = new byte[4];
			temp[0] = buffer[offset + 3];
			temp[1] = buffer[offset + 2];
			temp[2] = buffer[offset + 1];
			temp[3] = buffer[offset];
			return BitConverter.ToSingle(temp, 0);
		}


		/// <summary>
		/// Reads a 32 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(4);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		/// <summary>
		/// Reads a 64 bit floating point value from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64B(this byte[] buffer, int offset)
		{
			if (!BitConverter.IsLittleEndian)
				return BitConverter.ToDouble(buffer, offset);
			
			// reverse.
			byte[] temp = new byte[8];
			temp[0] = buffer[offset + 7];
			temp[1] = buffer[offset + 6];
			temp[2] = buffer[offset + 5];
			temp[3] = buffer[offset + 4];
			temp[4] = buffer[offset + 3];
			temp[5] = buffer[offset + 2];
			temp[6] = buffer[offset + 1];
			temp[7] = buffer[offset];
			return BitConverter.ToDouble(temp, 0);
		}

		/// <summary>
		/// Reads a 64 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64B(this Stream stream)
		{
			byte[] buffer = stream.ReadBlocking(8);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;
			
				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream)
		{
			return stream.ReadStringB(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32B();
			if (length < 0)
				return null;

			return stream.ReadString(encoding, length);
		}

		#endregion

		#region Variable Length

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16V(this Stream stream)
		{
			return unchecked((short)stream.ReadUInt16V());
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16V(this Stream stream)
		{
			int input = stream.ReadByte();
			if (input < 0) throw new EndOfStreamException();
			if (input <= 0x7F) return (ushort)input;

			int input2 = stream.ReadByte();
			if (input2 < 0) throw new EndOfStreamException();
			if (input2 <= 0x7F) return (ushort)((input & 0x7F) | (input2 << 7));

			int input3 = stream.ReadByte();
			if (input3 < 0) throw new EndOfStreamException();
			if (input3 > 3) throw new InvalidDataException();

			return (ushort)((input & 0x7F) | (input2 << 7) | (input3 << 14));
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32V(this Stream stream)
		{
			return unchecked((int)stream.ReadUInt32V());
		}
		
		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32V(this Stream stream)
		{
			uint value = 0;
			int shift = 0;

			while (true)
			{
				int input = stream.ReadByte();
				if (input < 0)
					throw new EndOfStreamException();

				value |= ((byte)input & 0x7Fu) << shift;
				if ((input & 0x80) == 0)
					break;

				if(shift >= 32)
					throw new InvalidDataException("Invalid variable-length coded integer.");

				shift += 7;
			}

			return value;
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64V(this Stream stream)
		{
			return unchecked((long)stream.ReadUInt64V());
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64V(this Stream stream)
		{
			ulong value = 0;
			int shift = 0;

			while (true)
			{
				int input = stream.ReadByte();
				if (input < 0)
					throw new EndOfStreamException();

				if (shift == 56)
				{
					value |= (ulong)input << 56;
				}
				else
				{
					value |= ((byte)input & 0x7Ful) << shift;

					if ((input & 0x80) == 0)
						break;

					shift += 7;
				}
			}

			return value;
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream)
		{
			return stream.ReadStringV(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32V();
			if (length < 0)
				return null;

			return stream.ReadString(encoding, length);
		}

		#endregion

		#endregion

		#region Writing

		/// <summary>
		/// Writes a structure or formatted class to a buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="value"></param>
		public static void WriteStructure<T>(this byte[] buffer, int offset, T value)
		{
			GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
			Marshal.StructureToPtr(value, handle.AddrOfPinnedObject() + offset, false);
			handle.Free();
		}

		/// <summary>
		/// Writes a structure or formatted class to a stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStructure<T>(this Stream stream, T value)
		{
			int size = Marshal.SizeOf(typeof(T));
			byte[] data = stream.ReadBlocking(size);
			data.WriteStructure<T>(0, value);
		}

		#region Little Endian

		/// <summary>
		/// Writes a 16 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt16L(this byte[] buffer, int offset, short value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
			}
		}

		/// <summary>
		/// Writes a 16 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16L(this Stream stream, short value)
		{
			byte[] buffer = new byte[2];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
			}
			stream.Write(buffer, 0, 2);
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt16L(this byte[] buffer, int offset, ushort value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
			}
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16L(this Stream stream, ushort value)
		{
			byte[] buffer = new byte[2];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
			}
			stream.Write(buffer, 0, 2);
		}
		
		/// <summary>
		/// Writes a 32 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt32L(this byte[] buffer, int offset, int value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
				buffer[offset + 2] = (byte)(value >> 16);
				buffer[offset + 3] = (byte)(value >> 24);
			}
		}

		/// <summary>
		/// Writes a 32 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32L(this Stream stream, int value)
		{
			byte[] buffer = new byte[4];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
				buffer[2] = (byte)(value >> 16);
				buffer[3] = (byte)(value >> 24);
			}
			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 32 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt32L(this byte[] buffer, int offset, uint value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
				buffer[offset + 2] = (byte)(value >> 16);
				buffer[offset + 3] = (byte)(value >> 24);
			}
		}

		/// <summary>
		/// Writes a 32 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32L(this Stream stream, uint value)
		{
			byte[] buffer = new byte[4];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
				buffer[2] = (byte)(value >> 16);
				buffer[3] = (byte)(value >> 24);
			}
			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 64 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt64L(this byte[] buffer, int offset, long value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
				buffer[offset + 2] = (byte)(value >> 16);
				buffer[offset + 3] = (byte)(value >> 24);
				buffer[offset + 4] = (byte)(value >> 32);
				buffer[offset + 5] = (byte)(value >> 40);
				buffer[offset + 6] = (byte)(value >> 48);
				buffer[offset + 7] = (byte)(value >> 56);
			}
		}

		/// <summary>
		/// Writes a 64 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64L(this Stream stream, long value)
		{
			byte[] buffer = new byte[8];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
				buffer[2] = (byte)(value >> 16);
				buffer[3] = (byte)(value >> 24);
				buffer[4] = (byte)(value >> 32);
				buffer[5] = (byte)(value >> 40);
				buffer[6] = (byte)(value >> 48);
				buffer[7] = (byte)(value >> 56);
			}
			stream.Write(buffer, 0, 8);
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt64L(this byte[] buffer, int offset, ulong value)
		{
			unchecked
			{
				buffer[offset] = (byte)(value);
				buffer[offset + 1] = (byte)(value >> 8);
				buffer[offset + 2] = (byte)(value >> 16);
				buffer[offset + 3] = (byte)(value >> 24);
				buffer[offset + 4] = (byte)(value >> 32);
				buffer[offset + 5] = (byte)(value >> 40);
				buffer[offset + 6] = (byte)(value >> 48);
				buffer[offset + 7] = (byte)(value >> 56);
			}
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64L(this Stream stream, ulong value)
		{
			byte[] buffer = new byte[8];
			unchecked
			{
				buffer[0] = (byte)(value);
				buffer[1] = (byte)(value >> 8);
				buffer[2] = (byte)(value >> 16);
				buffer[3] = (byte)(value >> 24);
				buffer[4] = (byte)(value >> 32);
				buffer[5] = (byte)(value >> 40);
				buffer[6] = (byte)(value >> 48);
				buffer[7] = (byte)(value >> 56);
			}
			stream.Write(buffer, 0, 8);
		}
	
		/// <summary>
		/// Writes a 32 bit floating point value into a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat32L(this byte[] buffer, int offset, float value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
			}
			else
			{
				buffer[offset] = temp[3];
				buffer[offset + 1] = temp[2];
				buffer[offset + 2] = temp[1];
				buffer[offset + 3] = temp[0];
			}
		}

		/// <summary>
		/// Writes a 32 bit floating point value to a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat32L(this Stream stream, float value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			
			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 64 bit floating point value into a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat64L(this byte[] buffer, int offset, double value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
				buffer[offset + 4] = temp[4];
				buffer[offset + 5] = temp[5];
				buffer[offset + 6] = temp[6];
				buffer[offset + 7] = temp[7];
			}
			else
			{
				buffer[offset] = temp[7];
				buffer[offset + 1] = temp[6];
				buffer[offset + 2] = temp[5];
				buffer[offset + 3] = temp[4];
				buffer[offset + 4] = temp[3];
				buffer[offset + 5] = temp[2];
				buffer[offset + 6] = temp[1];
				buffer[offset + 7] = temp[0];
			}
		}

		/// <summary>
		/// Writes a 64 bit floating point value to a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat64L(this Stream stream, double value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringL(this Stream stream, string value)
		{
			WriteStringL(stream, value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringL(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32L(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32L(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion

		#region Big Endian

		/// <summary>
		/// Writes a 16 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt16B(this byte[] buffer, int offset, short value)
		{
			unchecked
			{
				buffer[offset + 1] = (byte)(value);
				buffer[offset] = (byte)(value >> 8);
			}
		}

		/// <summary>
		/// Writes a 16 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16B(this Stream stream, short value)
		{
			byte[] buffer = new byte[2];
			unchecked
			{
				buffer[1] = (byte)(value);
				buffer[0] = (byte)(value >> 8);
			}
			stream.Write(buffer, 0, 2);
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt16B(this byte[] buffer, int offset, ushort value)
		{
			unchecked
			{
				buffer[offset + 1] = (byte)(value);
				buffer[offset] = (byte)(value >> 8);
			}
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16B(this Stream stream, ushort value)
		{
			byte[] buffer = new byte[2];
			unchecked
			{
				buffer[1] = (byte)(value);
				buffer[0] = (byte)(value >> 8);
			}
			stream.Write(buffer, 0, 2);
		}


		/// <summary>
		/// Writes a 32 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt32B(this byte[] buffer, int offset, int value)
		{
			unchecked
			{
				buffer[offset + 3] = (byte)(value);
				buffer[offset + 2] = (byte)(value >> 8);
				buffer[offset + 1] = (byte)(value >> 16);
				buffer[offset] = (byte)(value >> 24);
			}
		}

		/// <summary>
		/// Writes a 32 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32B(this Stream stream, int value)
		{
			byte[] buffer = new byte[4];
			unchecked
			{
				buffer[3] = (byte)(value);
				buffer[2] = (byte)(value >> 8);
				buffer[1] = (byte)(value >> 16);
				buffer[0] = (byte)(value >> 24);
			}
			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 32 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt32B(this byte[] buffer, int offset, uint value)
		{
			unchecked
			{
				buffer[offset + 3] = (byte)(value);
				buffer[offset + 2] = (byte)(value >> 8);
				buffer[offset + 1] = (byte)(value >> 16);
				buffer[offset] = (byte)(value >> 24);
			}
		}

		/// <summary>
		/// Writes a 32 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32B(this Stream stream, uint value)
		{
			byte[] buffer = new byte[4];
			unchecked
			{
				buffer[3] = (byte)(value);
				buffer[2] = (byte)(value >> 8);
				buffer[1] = (byte)(value >> 16);
				buffer[0] = (byte)(value >> 24);
			}
			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 64 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt64B(this byte[] buffer, int offset, long value)
		{
			unchecked
			{
				buffer[offset + 7] = (byte)(value);
				buffer[offset + 6] = (byte)(value >> 8);
				buffer[offset + 5] = (byte)(value >> 16);
				buffer[offset + 4] = (byte)(value >> 24);
				buffer[offset + 3] = (byte)(value >> 32);
				buffer[offset + 2] = (byte)(value >> 40);
				buffer[offset + 1] = (byte)(value >> 48);
				buffer[offset] = (byte)(value >> 56);
			}
		}

		/// <summary>
		/// Writes a 64 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64B(this Stream stream, long value)
		{
			byte[] buffer = new byte[8];
			unchecked
			{
				buffer[7] = (byte)(value);
				buffer[6] = (byte)(value >> 8);
				buffer[5] = (byte)(value >> 16);
				buffer[4] = (byte)(value >> 24);
				buffer[3] = (byte)(value >> 32);
				buffer[2] = (byte)(value >> 40);
				buffer[1] = (byte)(value >> 48);
				buffer[0] = (byte)(value >> 56);
			}
			stream.Write(buffer, 0, 8);
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt64B(this byte[] buffer, int offset, ulong value)
		{
			unchecked
			{
				buffer[offset + 7] = (byte)(value);
				buffer[offset + 6] = (byte)(value >> 8);
				buffer[offset + 5] = (byte)(value >> 16);
				buffer[offset + 4] = (byte)(value >> 24);
				buffer[offset + 3] = (byte)(value >> 32);
				buffer[offset + 2] = (byte)(value >> 40);
				buffer[offset + 1] = (byte)(value >> 48);
				buffer[offset] = (byte)(value >> 56);
			}
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64B(this Stream stream, ulong value)
		{
			byte[] buffer = new byte[8];
			unchecked
			{
				buffer[7] = (byte)(value);
				buffer[6] = (byte)(value >> 8);
				buffer[5] = (byte)(value >> 16);
				buffer[4] = (byte)(value >> 24);
				buffer[3] = (byte)(value >> 32);
				buffer[2] = (byte)(value >> 40);
				buffer[1] = (byte)(value >> 48);
				buffer[0] = (byte)(value >> 56);
			}
			stream.Write(buffer, 0, 8);
		}
		
		/// <summary>
		/// Writes a 32 bit floating point value into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat32B(this byte[] buffer, int offset, float value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
			}
			else
			{
				buffer[offset] = temp[3];
				buffer[offset + 1] = temp[2];
				buffer[offset + 2] = temp[1];
				buffer[offset + 3] = temp[0];
			}
		}

		/// <summary>
		/// Writes a 32 bit floating point value to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat32B(this Stream stream, float value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a 64 bit floating point value into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat64B(this byte[] buffer, int offset, double value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
				buffer[offset + 4] = temp[4];
				buffer[offset + 5] = temp[5];
				buffer[offset + 6] = temp[6];
				buffer[offset + 7] = temp[7];
			}
			else
			{
				buffer[offset] = temp[7];
				buffer[offset + 1] = temp[6];
				buffer[offset + 2] = temp[5];
				buffer[offset + 3] = temp[4];
				buffer[offset + 4] = temp[3];
				buffer[offset + 5] = temp[2];
				buffer[offset + 6] = temp[1];
				buffer[offset + 7] = temp[0];
			}
		}

		/// <summary>
		/// Writes a 64 bit floating point value to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat64B(this Stream stream, double value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringB(this Stream stream, string value)
		{
			WriteStringB(stream, value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringB(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32B(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32B(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion

		#region Variable Length

		/// <summary>
		/// Writes a 7-bit variable length encoded 16-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16V(this Stream stream, short value)
		{
			stream.WriteUInt16V(unchecked((ushort)value));
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 16-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16V(this Stream stream, ushort value)
		{
			if (value <= 0x7F)
			{
				stream.WriteByte((byte)value);
			}
			else if (value <= 0x3FFF)
			{
				stream.WriteByte((byte)((value & 0x7F) | 0x80));
				stream.WriteByte((byte)(value >> 7));
			}
			else
			{
				stream.WriteByte((byte)((value & 0x7F) | 0x80));
				stream.WriteByte((byte)(((value >> 7) & 0x7F) | 0x80));
				stream.WriteByte((byte)(value >> 14));
			}
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 32-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32V(this Stream stream, int value)
		{
			stream.WriteUInt32V(unchecked((uint)value));
		}
		
		/// <summary>
		/// Writes a 7-bit variable length encoded 32-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32V(this Stream stream, uint value)
		{
			do
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				stream.WriteByte(current);
			} while (value > 0);
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 64-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64V(this Stream stream, long value)
		{
			stream.WriteUInt64V(unchecked((ulong)value));
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 64-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64V(this Stream stream, ulong value)
		{
			for(int i = 0; i < 8; i++)
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				stream.WriteByte(current);

				if (value == 0)
					return;
			}

			stream.WriteByte((byte)value);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a variable-length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringV(this Stream stream, string value)
		{
			WriteStringV(stream, value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringV(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32V(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32V(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion

		#endregion
	}
}

