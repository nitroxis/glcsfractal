#version 330 core

layout(location = 0) in vec2 position;

out vec2 texCoord;

void main(void)
{
	texCoord = (vec2(1.0) + position) * 0.5;
	gl_Position = vec4(position.x, position.y, 0.0, 1.0);
}