#version 330 core

uniform usampler2D map;
uniform sampler1D gradient;

uniform vec2 pixelSize;

uniform float scale;
uniform float offset;
uniform uint maxIterations;

in vec2 texCoord;

out vec4 color;

void main(void)
{
	// read data.
	uvec4 data = texture(map, texCoord);

#if 0
	// edge-detection laplace-filter-ish effect
	float total = 0.0;
	const int radius = 1;
	const int size = 2 * radius + 1;
	const float[9] edgeWeight = float[]
	(
		0.1, 0.1, 0.1,
		0.1, 1.0, 0.1,
		0.1, 0.1, 0.1
	);

	for(int x = -radius; x <= radius; x++)
	{
		for(int y = -radius; y <= radius; y++)
		{
			vec2 tc = texCoord + pixelSize * vec2(x, y);
			float diff = abs(float(data.r) - float(texture(map, tc).r)) - 1.0;
			total += edgeWeight[(y + radius) * size + (x + radius)] * clamp(diff * 0.1, 0.0, 10.0);
		}
	}
	
	color = vec4(vec3(1.0 - clamp(total, 0.0, 1.0)), 1.0);
#else
	if(data.r == uint(-1))
	{
		color = vec4(1.0, 0.0, 0.0, 1.0);
	}
	else if(data.r >= maxIterations)
	{
		color = vec4(0.0, 0.0, 0.0, 1.0);
	}
	else
	{
		float x = float(data.r) * 0.01;
		color = texture(gradient, x * scale + offset); // get a color from the gradient by scaling and moving the iteration count.
	}
#endif
}