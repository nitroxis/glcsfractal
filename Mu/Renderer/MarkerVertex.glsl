#version 330 core

uniform vec2 offset;
uniform vec2 scale;

layout(location = 0) in vec2 position;

void main(void)
{
	gl_Position = vec4(offset + position * scale, 0.0, 1.0);
}