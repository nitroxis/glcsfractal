﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Renderer for iteration-maps produced by a fractal calculator.
	/// </summary>
	public sealed class Renderer : DisposableObject
	{
		#region Fields

		private readonly GLContext context;

		private readonly GLProgram renderProgram;
		private readonly GLProgram markerProgram;
		
		private readonly int fullscreenVBO;
		private readonly int fullscreenVAO;

		private readonly int markerVBO;
		private readonly int markerVAO;
		
		private readonly int gradientTexture; 
		private readonly float constantOffset;
		private readonly float constantScale;
		private float offset;
		private float scale;

		private bool showMarkers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the gradient offset.
		/// </summary>
		public float Offset
		{
			get { return this.offset; }
			set { this.offset = value; }
		}
		
		/// <summary>
		/// Gets or sets the gradient scale.
		/// </summary>
		public float Scale
		{
			get { return this.scale; }
			set { this.scale = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether markers are to be rendered on top of the image.
		/// </summary>
		public bool ShowMarkers
		{
			get { return this.showMarkers; }
			set { this.showMarkers = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Renderer.
		/// </summary>
		public Renderer(GLContext context)
		{
			this.context = context;

			#region Program

			this.renderProgram = new GLProgram(context, "RenderProgram");
			this.renderProgram.Attach(ShaderType.VertexShader, Helper.GetCode("RenderVertex"));
			this.renderProgram.Attach(ShaderType.FragmentShader, Helper.GetCode("RenderFragment"));
			this.renderProgram.Link();

			this.markerProgram = new GLProgram(context, "MarkerProgram");
			this.markerProgram.Attach(ShaderType.VertexShader, Helper.GetCode("MarkerVertex"));
			this.markerProgram.Attach(ShaderType.FragmentShader, Helper.GetCode("MarkerFragment"));
			this.markerProgram.Link();

			#endregion

			#region Fullscreen VBO/VAO

			this.fullscreenVBO = this.context.Invoke(() => GL.GenBuffer());
			this.fullscreenVAO = this.context.Invoke(() => GL.GenVertexArray());

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.ArrayBuffer, this.fullscreenVBO);
				GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(32), new[] { -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f }, BufferUsageHint.StaticDraw);

				GL.BindVertexArray(this.fullscreenVAO);
				GL.EnableVertexAttribArray(0);
				GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, 0);
			});

			#endregion

			#region Marker VBO/VAO

			const int segments = 32;
			const float innerRadius = 1.5f;
			const float outerRadius = 2.0f;

			float[] ring = new float[(segments + 1) * 4];
			int j = 0;
			ring[j++] = 0.0f;
			ring[j++] = innerRadius;
			ring[j++] = 0.0f;
			ring[j++] = outerRadius;
			for (int i = 0; i < segments; i++)
			{
				float angle = (float)((i + 1) * Math.PI * 2.0d / segments);
				ring[j++] = (float)(Math.Sin(angle) * innerRadius);
				ring[j++] = (float)(Math.Cos(angle) * innerRadius);
				ring[j++] = (float)(Math.Sin(angle) * outerRadius);
				ring[j++] = (float)(Math.Cos(angle) * outerRadius);
			}

			this.markerVBO = this.context.Invoke(() => GL.GenBuffer());
			this.markerVAO = this.context.Invoke(() => GL.GenVertexArray());

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.ArrayBuffer, this.markerVBO);
				GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(ring.Length * 4), ring, BufferUsageHint.StaticDraw);

				GL.BindVertexArray(this.markerVAO);
				GL.EnableVertexAttribArray(0);
				GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, 0, 0);
			});

			#endregion

			#region Gradient

			// blue-yellow-ish gradient.
			const int gradientWidth = 6;
			byte[] gradient =
			{
				0x00, 0x00, 0x30,
				0x08, 0x08, 0x64,
				0x20, 0x6B, 0xCB,
				0xED, 0xFF, 0xFF,
				0xFF, 0x99, 0x00,
				0x70, 0x10, 0x20,
			};

			this.gradientTexture = this.context.Invoke(() => GL.GenTexture());

			this.context.Invoke(() =>
			{
				GL.BindTexture(TextureTarget.Texture1D, this.gradientTexture);
				GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture1D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
				GL.TexImage1D(TextureTarget.Texture1D, 0, PixelInternalFormat.Rgb, gradientWidth, 0, PixelFormat.Rgb, PixelType.UnsignedByte, gradient);
			});

			this.constantOffset = (1.0f / gradientWidth) * 0.5f; // texel center
			this.constantScale = 1.0f / gradientWidth;
			this.offset = 0.0f;
			this.scale = 1.0f;

			#endregion
		}

		#endregion

		#region Methods

		/// <summary>
		/// Randomizes the palette.
		/// </summary>
		public void RandomizePalette()
		{
			Random rng = new Random();

			Vector3d c = new Vector3d(rng.NextDouble(), rng.NextDouble(), rng.NextDouble());
			c.Normalize();
			byte r = (byte)(c.X * 255.0d);
			byte g = (byte)(c.Y * 255.0d);
			byte b = (byte)(c.Z * 255.0d);

			byte[] gradient =
			{
				0, 0, 0,
				r, g, b,
				255, 255, 255,
				(byte)(255 - r), (byte)(255 - g), (byte)(255 - b),
			};

			this.context.Invoke(() =>
			{
				GL.BindTexture(TextureTarget.Texture1D, this.gradientTexture);
				GL.TexImage1D(TextureTarget.Texture1D, 0, PixelInternalFormat.Rgb, gradient.Length / 3, 0, PixelFormat.Rgb, PixelType.UnsignedByte, gradient);
			});
		}

		/// <summary>
		/// Renders a fractal.
		/// </summary>
		public void Render(GLAlgorithm fractal)
		{
			this.context.Invoke(() =>
			{
				GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.Zero);
				
				// bind gradient texture and calculation texture.
				GL.ActiveTexture(TextureUnit.Texture1);
				GL.BindTexture(TextureTarget.Texture1D, this.gradientTexture);
				GL.ActiveTexture(TextureUnit.Texture0);
				fractal.Texture.Bind();

				// set up program.
				this.renderProgram.Use();
				//GL.Uniform2(this.renderProgram.GetLocation("pixelSize"), 1.0f / fractal.Width, 1.0f / fractal.Height);
				GL.Uniform1(this.renderProgram.GetLocation("map"), 0);
				GL.Uniform1(this.renderProgram.GetLocation("gradient"), 1);
				GL.Uniform1(this.renderProgram.GetLocation("maxIterations"), fractal.MaxIterations);
				GL.Uniform1(this.renderProgram.GetLocation("scale"), this.constantScale * this.scale);
				GL.Uniform1(this.renderProgram.GetLocation("offset"), this.constantOffset + this.offset);

				// draw vertices.
				GL.BindVertexArray(this.fullscreenVAO);
				GL.DrawArrays(PrimitiveType.TriangleFan, 0, 4);

				// render markers.
				if (this.showMarkers && fractal.Markers.Count > 0)
				{
					GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

					// set up program.
					this.markerProgram.Use();
					GL.BindVertexArray(this.markerVAO);

					foreach (Marker marker in fractal.Markers)
					{
						GL.Uniform2(this.markerProgram.GetLocation("scale"), marker.Radius / fractal.Width, marker.Radius / fractal.Height);
						GL.Uniform2(this.markerProgram.GetLocation("offset"), marker.Position);
						GL.Uniform4(this.markerProgram.GetLocation("color"), marker.Color);
						GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 33 * 2);
					}
				}
			});
		}

		/// <summary>
		/// Disposes the renderer.
		/// </summary>
		protected override void Dispose(bool managed)
		{
			this.renderProgram.Dispose();
			this.markerProgram.Dispose();

			this.context.Invoke(() =>
			{
				GL.DeleteTexture(this.gradientTexture);

				GL.DeleteBuffer(this.fullscreenVBO);
				GL.DeleteVertexArray(this.fullscreenVAO);

				GL.DeleteBuffer(this.markerVBO);
				GL.DeleteVertexArray(this.markerVAO);
			});
		}
	
		#endregion
	}
}

