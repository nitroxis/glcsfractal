﻿using OpenTK;
using OpenTK.Graphics;

namespace Mu
{
	/// <summary>
	/// Represents a simple marker on a fractal that can be used to highlight certain points.
	/// </summary>
	public sealed class Marker
	{
		#region Fields

		public Vector2 Position;
		public float Radius;
		public Color4 Color;
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Creates a new Marker.
		/// </summary>
		public Marker(Vector2 position, float radius, Color4 color)
		{
			this.Position = position;
			this.Radius = radius;
			this.Color = color;
		}

		#endregion
	}
}
