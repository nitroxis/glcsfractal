﻿using System;
using System.Runtime.CompilerServices;

namespace Mu
{
	/// <summary>
	/// Represents an abstract disposable object.
	/// </summary>
	public abstract class DisposableObject : IDisposable
	{
		#region Fields

		private bool isDisposed;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating wether this object has been disposed or not.
		/// </summary>
		public bool IsDisposed
		{
			get { return this.isDisposed; }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Throws an exception if the object has been disposed.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		protected void CheckDisposed()
		{
			if (this.isDisposed)
				throw new ObjectDisposedException(this.GetType().Name);
		}

		/// <summary>
		/// Disposes the object and releases all resources.
		/// </summary>
		public void Dispose()
		{
			if (!this.isDisposed)
			{
				this.isDisposed = true;
				this.Dispose(true);
			}

			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Disposes the object and releases all resources.
		/// </summary>
		/// <param name="managed">Indicates wether managed resources should be released too.</param>
		protected virtual void Dispose(bool managed)
		{
			// add clean up code here...
		}

		~DisposableObject()
		{
			if (!this.isDisposed)
			{
				this.isDisposed = true;
				this.Dispose(false);
			}
		}

		#endregion
	}

}
