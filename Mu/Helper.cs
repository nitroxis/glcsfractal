﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Mu.Properties;

namespace Mu
{
	/// <summary>
	/// OpenGL helper methods.
	/// </summary>
	internal static class Helper
	{
		#region Methods

		/// <summary>
		/// Gets the code of a shader from the resources and preprocesses it.
		/// </summary>
		/// <param name="resourceName"></param>
		/// <param name="preprocessor"></param>
		/// <returns></returns>
		public static string GetCode(string resourceName, Func<string, string> preprocessor = null, Dictionary<string, string> includes = null)
		{
			Encoding enc = new UTF8Encoding(false);

			byte[] resource = Resources.ResourceManager.GetObject(resourceName) as byte[];
			if (resource == null)
				throw new Exception(string.Format("Could not find resource file \"{0}\".", resourceName));

			return preprocessCode(enc.GetString(resource), preprocessor, includes);
		}

		private static string preprocessCode(string source, Func<string, string> preprocessor, Dictionary<string, string> includes = null)
		{
			Match match;
			Encoding enc = new UTF8Encoding(false);

			if (preprocessor != null)
				source = preprocessor(source);

			while ((match = Regex.Match(source, @"#include\s+<([^>]+)>")).Success)
			{
				string before = source.Substring(0, match.Index);
				string after = source.Substring(match.Index + match.Length);
				string include;
	
				string includeName = match.Groups[1].Value;
				if (includes == null || !includes.TryGetValue(includeName, out include))
				{
					include = GetCode(includeName, preprocessor);
				}

				source = before + include + after;
			}
			
			return source;
		}
		
		#endregion
	}
}
