﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Represents an OpenGL buffer.
	/// </summary>
	public sealed class GLBuffer : DisposableObject
	{
		#region Fields

		private readonly GLContext context;
		private readonly int id;
		private IntPtr size;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the buffer ID.
		/// </summary>
		public int ID
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets or sets the size of the buffer.
		/// </summary>
		public IntPtr Size
		{
			get { return this.size; }
		}

		#endregion

		#region Constructors
		
		/// <summary>
		/// Creates a new ShaderStorage.
		/// </summary>
		public GLBuffer(GLContext context, IntPtr size)
		{
			this.context = context;
			this.id = context.Invoke(() => GL.GenBuffer());
			this.size = size;

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.CopyWriteBuffer, this.id);
				GL.BufferData(BufferTarget.CopyWriteBuffer, this.size, IntPtr.Zero, BufferUsageHint.StaticDraw);
			});

			Trace.WriteLine(string.Format("Created buffer {0} with {1} KiB.", this.id, (this.size.ToInt64() + 1023) / 1024));
		}

		#endregion

		/// <summary>
		/// Sets the data of the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		public void Set<T>(T[] data) where T : struct
		{
			this.Set(data, 0, 0, data.Length);
		}

		/// <summary>
		/// Sets the data of the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <param name="srcOffset"></param>
		/// <param name="destOffset"></param>
		/// <param name="count"></param>
		public void Set<T>(T[] data, int srcOffset, int destOffset, int count) where T : struct
		{
			int arraySize = Marshal.SizeOf(typeof(T)) * count;
			if (srcOffset + count > data.Length || destOffset + arraySize > this.size.ToInt64())
				throw new IndexOutOfRangeException();

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.CopyWriteBuffer, this.id);
				GL.BufferSubData(BufferTarget.CopyWriteBuffer, (IntPtr)destOffset, (IntPtr)arraySize, ref data[srcOffset]);
			});
		}

		/// <summary>
		/// Gets the data of the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		public void Get<T>(T[] data) where T : struct
		{
			this.Get(0, data, 0, data.Length);
		}

		/// <summary>
		/// Gets the data of the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="srcOffset"></param>
		/// <param name="data"></param>
		/// <param name="destOffset"></param>
		/// <param name="count"></param>
		public void Get<T>(int srcOffset, T[] data, int destOffset, int count) where T : struct
		{
			int arraySize = Marshal.SizeOf(typeof(T)) * count;
			if (destOffset + count > data.Length || srcOffset + arraySize > this.size.ToInt64())
				throw new IndexOutOfRangeException();

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.CopyReadBuffer, this.id);
				GL.GetBufferSubData(BufferTarget.CopyReadBuffer, (IntPtr)srcOffset, (IntPtr)arraySize, data);
			});
		}

		/// <summary>
		/// Clears the contents of the buffer.
		/// </summary>
		public void Clear()
		{
			uint zero = 0;

			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.CopyWriteBuffer, this.id);
				GL.ClearBufferData(BufferTarget.CopyWriteBuffer, PixelInternalFormat.R32ui, PixelFormat.RedInteger, All.UnsignedInt, ref zero);
			});
		}

		/// <summary>
		/// Binds the buffer.
		/// </summary>
		/// <param name="target">The target.</param>
		/// <param name="index">The slot to bind the buffer to.</param>
		public void BindRange(BufferRangeTarget target, int index)
		{
			this.BindRange(target, index, 0, (int)this.size);
		}

		/// <summary>
		/// Binds the buffer.
		/// </summary>
		/// <param name="target">The target.</param>
		/// <param name="index">The slot to bind the buffer to.</param>
		/// <param name="offset">The byte offset.</param>
		/// <param name="size">The number of bytes to use.</param>
		public void BindRange(BufferRangeTarget target, int index, int offset, int size)
		{
			this.BindRange(target, index, (uint)offset, (uint)size);
		}

		/// <summary>
		/// Binds the buffer.
		/// </summary>
		/// <param name="target">The target.</param>
		/// <param name="index">The slot to bind the buffer to.</param>
		/// <param name="offset">The byte offset.</param>
		/// <param name="size">The number of bytes to use.</param>
		public void BindRange(BufferRangeTarget target, int index, uint offset, uint size)
		{
			if (size > this.size.ToInt64() - offset)
				throw new IndexOutOfRangeException();

			this.context.Invoke(() => GL.BindBufferRange(target, index, this.id, (IntPtr)offset, (IntPtr)size));
		}
		
		#region Methods

		protected override void Dispose(bool managed)
		{
			Trace.WriteLine(string.Format("Deleted buffer {0}.", this.id));
			this.context.Invoke(() => GL.DeleteBuffer(this.id));
		}

		#endregion
	}
}
