﻿using System.Diagnostics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Represents an OpenGL texture.
	/// </summary>
	public sealed class GLTexture : DisposableObject
	{
		#region Fields

		private readonly GLContext context;
		private readonly int id;
		private readonly TextureTarget target;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL object ID.
		/// </summary>
		public int ID
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets the texture target of this texture.
		/// </summary>
		public TextureTarget Target
		{
			get { return this.target; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture.
		/// </summary>
		public GLTexture(GLContext context, TextureTarget target)
		{
			this.context = context;

			this.id = this.context.Invoke(() => GL.GenTexture());
			this.target = target;

			Trace.WriteLine(string.Format("Created texture {0} {1}.", this.target, this.id));
			
			this.context.Invoke(() =>
			{
				GL.BindTexture(this.target, this.id);
				GL.TexParameter(this.target, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
				GL.TexParameter(this.target, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
			});
		}

		#endregion

		#region Methods

		public void Bind()
		{
			this.context.Invoke(() => GL.BindTexture(this.target, this.id));
		}

		public void BindImage(int unit, TextureAccess access, SizedInternalFormat format)
		{
			this.context.Invoke(() => GL.BindImageTexture(unit, this.id, 0, false, 0, access, format));
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			Trace.WriteLine(string.Format("Deleted texture {0} {1}.", this.target, this.id));
			this.context.Invoke(() => GL.DeleteTexture(this.id));
		}

		#endregion
	}
}
