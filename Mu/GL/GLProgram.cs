﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Simple OpenGL program wrapper.
	/// </summary>
	public sealed class GLProgram : DisposableObject
	{
		#region Nested Types

		private class Uniform
		{
			public string Name;
			public int Location;
			public int Offset;
		}

		#endregion

		#region Fields

		private readonly GLContext context;
		private readonly string name;
		private readonly int id;
		private readonly List<int> shaders;
		private readonly List<Uniform> uniforms;
		private readonly Dictionary<int, int> blockSizes;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL object ID.
		/// </summary>
		public int ID
		{
			get { return this.id; }
		}

		/// <summary>
		/// Gets the name of the program.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GpuProgram.
		/// </summary>
		public GLProgram(GLContext context, string name)
		{
			this.context = context;
			this.name = name;
			this.id = this.context.Invoke(() => GL.CreateProgram());

			this.shaders = new List<int>();
			this.uniforms = new List<Uniform>();
			this.blockSizes = new Dictionary<int, int>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Attaches a shader.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="sources"></param>
		public void Attach(ShaderType type, params string[] sources)
		{
			int shader = this.context.Invoke(() => GL.CreateShader(type));

			int[] sourceLengths = new int[sources.Length];
			for (int i = 0; i < sources.Length; i++)
				sourceLengths[i] = sources[i].Length;

			this.context.Invoke(() =>
			{
				GL.ShaderSource(shader, sources.Length, sources, sourceLengths);
				GL.CompileShader(shader);
			});

			string log = this.context.Invoke(() => GL.GetShaderInfoLog(shader));

			if (!string.IsNullOrWhiteSpace(log))
				Trace.WriteLine(string.Format("{0}.{1}: {2}", this.name, type, log));

			int status = 0;
			this.context.Invoke(() => GL.GetShader(shader, ShaderParameter.CompileStatus, out status));
			if (status == 0) 
				throw new Exception(log);

			this.context.Invoke(() => GL.AttachShader(this.id, shader));
			
			this.shaders.Add(shader);
		}

		/// <summary>
		/// Links the program.
		/// </summary>
		public void Link()
		{
			this.context.Invoke(() => GL.LinkProgram(this.id));

			string log = this.context.Invoke(() => GL.GetProgramInfoLog(this.id));
			if (!string.IsNullOrWhiteSpace(log))
				Trace.WriteLine(string.Format("{0}: {1}", this.name, log));

			int status = 0;
			this.context.Invoke(() => GL.GetProgram(this.id, GetProgramParameterName.LinkStatus, out status));
			
			if (status == 0)
				throw new Exception(log);

			this.context.Invoke(() =>
			{
				int numUniforms;
				GL.GetProgram(this.id, GetProgramParameterName.ActiveUniforms, out numUniforms);
				if (numUniforms > 0)
				{
					int[] indices = new int[numUniforms];
					for (int i = 0; i < numUniforms; i++)
						indices[i] = i;

					int[] blockIndices = new int[numUniforms];
					int[] offsets = new int[numUniforms];

					GL.GetActiveUniforms(this.id, numUniforms, indices, ActiveUniformParameter.UniformBlockIndex, blockIndices);
					GL.GetActiveUniforms(this.id, numUniforms, indices, ActiveUniformParameter.UniformOffset, offsets);

					for (int i = 0; i < numUniforms; i++)
					{
						Uniform u = new Uniform();
						u.Name = GL.GetActiveUniformName(this.id, i);

						if (blockIndices[i] < 0)
						{
							u.Location = GL.GetUniformLocation(this.id, u.Name);
							u.Offset = -1;
						}
						else
						{
							u.Location = -1;
							u.Offset = offsets[i];
						}

						this.uniforms.Add(u);
					}
				}
				
				int numBlocks;
				GL.GetProgram(this.id, GetProgramParameterName.ActiveUniformBlocks, out numBlocks);

				for (int i = 0; i < numBlocks; i++)
				{
					int binding;
					int size;
					GL.GetActiveUniformBlock(this.id, i, ActiveUniformBlockParameter.UniformBlockBinding, out binding);
					GL.GetActiveUniformBlock(this.id, i, ActiveUniformBlockParameter.UniformBlockDataSize, out size);

					this.blockSizes[binding] = size;
				}
			});
		}

		/// <summary>
		/// Returns the location of the uniform with the specified name.
		/// </summary>
		/// <param name="uniformName"></param>
		/// <returns></returns>
		public int GetLocation(string uniformName)
		{
			foreach (Uniform u in this.uniforms)
			{
				if (u.Name == uniformName)
					return u.Location;
			}

			Trace.WriteLine(string.Format("Warning: Uniform location \"{0}\" not found.", uniformName));
			return -1;
		}

		/// <summary>
		/// Returns the offset of the uniform with the specified name.
		/// </summary>
		/// <param name="uniformName"></param>
		/// <returns></returns>
		public int GetOffset(string uniformName)
		{
			foreach (Uniform u in this.uniforms)
			{
				if (u.Name == uniformName)
					return u.Offset;
			}

			Trace.WriteLine(string.Format("Warning: Uniform offset \"{0}\" not found.", uniformName));
			return -1;
		}

		/// <summary>
		/// Gets the size of a uniform block.
		/// </summary>
		/// <param name="binding"></param>
		/// <returns></returns>
		public int GetBlockSize(int binding)
		{
			int size;
			if (this.blockSizes.TryGetValue(binding, out size))
				return size;

			return -1;
		}

		/// <summary>
		/// Uses the program.
		/// </summary>
		public void Use()
		{
			this.context.Invoke(() => GL.UseProgram(this.id));
		}

		/// <summary>
		/// Deletes the program.
		/// </summary>
		protected override void Dispose(bool managed)
		{
			this.context.Invoke(() =>
			{
				GL.DeleteProgram(this.id);
				foreach (int shader in this.shaders)
					GL.DeleteShader(shader);
			});
		}

		#endregion
	}
}
