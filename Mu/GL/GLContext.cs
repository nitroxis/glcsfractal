﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// An OpenGL context and task queue that ensures all GL calls are performed from one thread.
	/// </summary>
	public sealed class GLContext
	{
		#region Fields

		private readonly HashSet<string> extensions;
		private int glVersion;
		private int glslVersion;
		private string renderer;
		private string vendor;

		private bool running;
		private Thread workerThread;
		private Action task;
		private AutoResetEvent idle;
		private AutoResetEvent taskAvailable;
		private AutoResetEvent taskFinished;

#if DEBUG
		private DebugProc debugProc;
#endif

		#endregion

		#region Properties

		public int GLVersion
		{
			get { return this.glVersion; }
		}

		public int GLSLVersion
		{
			get { return this.glslVersion; }
		}

		public string Renderer
		{
			get { return this.renderer; }
		}

		public string Vendor
		{
			get { return this.vendor; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GLThread.
		/// </summary>
		public GLContext()
		{
			this.extensions = new HashSet<string>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Runs the thread.
		/// </summary>
		public void Run(ManualResetEvent ready)
		{
			this.running = true;
			this.workerThread = Thread.CurrentThread;

			Trace.WriteLine("Running GLContext.");
			this.initialize();

			this.idle = new AutoResetEvent(true);
			this.taskAvailable = new AutoResetEvent(false);
			this.taskFinished = new AutoResetEvent(false);

			ready.Set();

			while (this.running)
			{
				this.taskAvailable.WaitOne();
				this.task();
				this.taskFinished.Set();
			}
		}

		/// <summary>
		/// Stops the thread.
		/// </summary>
		public void Stop(params Action[] shutDownActions)
		{
			this.Invoke(() =>
			{
				Trace.WriteLine("Shutting down...");
				foreach (Action action in shutDownActions)
					action();

				this.running = false;

				Trace.WriteLine("GLContext stopped.");
			});

			this.workerThread.Join();
			this.workerThread = null;
			this.idle = null;
			this.taskAvailable = null;
			this.taskFinished = null;
		}

		/// <summary>
		/// Executes the specified task and returns the result.
		/// </summary>
		/// <typeparam name="T">The type of the return value.</typeparam>
		/// <param name="func">The task to execute.</param>
		/// <returns></returns>
		public T Invoke<T>(Func<T> func)
		{
			// this should be handled differently...
			if (!this.running)
			{
				Trace.WriteLine("Error: Tried to execute function on dead GLContext.");
				return default(T);
			}

			if (Thread.CurrentThread == this.workerThread)
				return func();

			T result = default(T);

#if DEBUG
			Exception ex = null;
#endif

			this.idle.WaitOne();

			this.task = () =>
			{
				result = func();

#if DEBUG
				ErrorCode code = GL.GetError();
				if (code != ErrorCode.NoError)
					ex = new Exception(string.Format("OpenGL error: {0}.", code));
				else
					ex = null;
#endif
			};

			this.taskAvailable.Set();
			this.taskFinished.WaitOne();
			this.idle.Set();

#if DEBUG
			if (ex != null)
				throw ex;
#endif

			return result;
		}

		/// <summary>
		/// Executes the specified task.
		/// </summary>
		/// <param name="action">The task to execute.</param>
		/// <returns></returns>
		public void Invoke(Action action)
		{
			// this should be handled differently...
			if (!this.running)
			{
				Trace.WriteLine("Error: Tried to execute function on dead GLContext.");
				return;
			}

			if (Thread.CurrentThread == this.workerThread)
			{
				action();
				return;
			}

#if DEBUG
			Exception ex = null;
#endif

			this.idle.WaitOne();

			this.task = () =>
			{
				action();

#if DEBUG
				ErrorCode code = GL.GetError();
				if (code != ErrorCode.NoError)
					ex = new Exception(string.Format("OpenGL error: {0}.", code));
				else
					ex = null;
#endif
			};

			this.taskAvailable.Set();
			this.taskFinished.WaitOne();
			this.idle.Set();

#if DEBUG
			if (ex != null)
				throw ex;
#endif
		}

		private void initialize()
		{
			// gather some info.
			int numExtensions = GL.GetInteger(GetPName.NumExtensions);
			for (int i = 0; i < numExtensions; i++)
				this.extensions.Add(GL.GetString(StringNameIndexed.Extensions, i));

			
#if DEBUG
			if (this.HasExtension("GL_ARB_debug_output"))
			{
				this.debugProc = log;
				GL.DebugMessageCallback(this.debugProc, IntPtr.Zero);
				GL.Enable(EnableCap.DebugOutputSynchronous);
				Trace.WriteLine("Enabled debug output extension.");
			}
#endif
			

			// extract OpenGL version in XXX format.
			string glVersionStr = GL.GetString(StringName.Version);
			Match versionMatch = Regex.Match(glVersionStr, @"^(\d+)\.(\d)");
			if (versionMatch.Success)
			{
				this.glVersion = Int32.Parse(versionMatch.Groups[1].Value) * 100 + Int32.Parse(versionMatch.Groups[2].Value) * 10;
			}
			else
			{
				Trace.WriteLine(String.Format("Warning: Failed to retrieve OpenGL version due to a malformed version string \"{0}\".", glVersionStr));
				this.glVersion = 100;
			}

			// extract GLSL version in XXX format.
			string glslVersionStr = GL.GetString(StringName.ShadingLanguageVersion);
			versionMatch = Regex.Match(glslVersionStr, @"^(\d+)\.(\d)(\d)?");
			if (versionMatch.Success)
			{
				this.glslVersion = Int32.Parse(versionMatch.Groups[1].Value) * 100 + Int32.Parse(versionMatch.Groups[2].Value) * 10;
				if (versionMatch.Groups[3].Success) this.glslVersion += Int32.Parse(versionMatch.Groups[3].Value);
			}
			else
			{
				if (this.glVersion < 200) this.glslVersion = 100;
				else if (this.glVersion >= 200) this.glslVersion = 110;
				else if (this.glVersion >= 210) this.glslVersion = 120;
				else if (this.glVersion >= 300) this.glslVersion = 130;
				else if (this.glVersion >= 310) this.glslVersion = 140;
				else if (this.glVersion >= 320) this.glslVersion = 150;
				else if (this.glVersion >= 330) this.glslVersion = this.glVersion;
				Trace.WriteLine(String.Format("Warning: Failed to retrieve GLSL version due to a malformed version string \"{0}\". Assuming GLSL version {1}.", glslVersionStr, this.glslVersion));
			}

			// some (probably) helpful info.
			this.renderer = GL.GetString(StringName.Renderer);
			this.vendor = GL.GetString(StringName.Vendor);

			Trace.WriteLine(String.Format("OpenGL Version: {0}.", this.glVersion));
			Trace.WriteLine(String.Format("GLSL Version: {0}.", this.glslVersion));
			Trace.WriteLine(String.Format("Renderer: {0}.", this.renderer));
			Trace.WriteLine(String.Format("Vendor: {0}.", this.vendor));

			GL.Enable(EnableCap.Blend);
		}

		private static string getDebugSourceText(DebugSource source)
		{
			switch (source)
			{
				case DebugSource.DebugSourceApi: return "API";
				case DebugSource.DebugSourceApplication: return "Application";
				case DebugSource.DebugSourceOther: return "Other";
				case DebugSource.DebugSourceShaderCompiler: return "Shader compiler";
				case DebugSource.DebugSourceThirdParty: return "Third party";
				case DebugSource.DebugSourceWindowSystem: return "Window system";
			}

			return "Unknown";
		}

		private static string getDebugTypeText(DebugType type)
		{
			switch (type)
			{
				case DebugType.DebugTypeDeprecatedBehavior: return "Deprecated";
				case DebugType.DebugTypeError: return "Error";
				case DebugType.DebugTypeMarker: return "Marker";
				case DebugType.DebugTypeOther: return "Other";
				case DebugType.DebugTypePerformance: return "Performance";
				case DebugType.DebugTypePopGroup: return "PopGroup";
				case DebugType.DebugTypePortability: return "Portability";
				case DebugType.DebugTypePushGroup: return "PushGroup";
				case DebugType.DebugTypeUndefinedBehavior: return "Undefined";
			}

			return "Unknown";
		}

		private static void log(DebugSource source, DebugType type, int id, DebugSeverity severity, int length, IntPtr message, IntPtr userParam)
		{
			string messageStr = Marshal.PtrToStringAnsi(message, length).Trim();
			string sourceStr = getDebugSourceText(source);
			string typeStr = getDebugTypeText(type);
			Trace.WriteLine(String.Format("[{0}/{1}] {2} ({3:X})", sourceStr, typeStr, messageStr, id));
		}

		public bool HasExtension(string ext)
		{
			return this.extensions.Contains(ext);
		}

		#endregion
	}

}
