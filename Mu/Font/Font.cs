﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Mu.Properties;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Represents a font.
	/// </summary>
	internal sealed class Font : DisposableObject
	{
		#region Nested Types

		[StructLayout(LayoutKind.Sequential)]
		private struct GlyphData
		{
			#region Fields

			public readonly int X;
			public readonly int Y;
			public readonly int Width;
			public readonly int Height;
			public readonly int XOffset;
			public readonly int YOffset;
			public readonly int XAdvance;
			public readonly int Page;

			#endregion

			#region Constructors

			public GlyphData(int x, int y, int width, int height, int xOffset, int yOffset, int xAdvance, int page)
			{
				this.X = x;
				this.Y = y;
				this.Width = width;
				this.Height = height;
				this.XOffset = xOffset;
				this.YOffset = yOffset;
				this.XAdvance = xAdvance;
				this.Page = page;
			}

			#endregion
		}

		#endregion

		#region Fields

		private readonly GLContext context;
		private readonly string name;
		private readonly int lineHeight;
		
		internal readonly int[] advance;
		internal readonly Dictionary<char, int> chars;

		internal readonly GLTexture texture;
		internal readonly GLBuffer glyphBuffer;
		internal readonly GLProgram program;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the font face.
		/// </summary>
		public string Name
		{
			get { return this.name; }
		}

		/// <summary>
		/// Gets the line height.
		/// </summary>
		public int LineHeight
		{
			get { return this.lineHeight; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Font.
		/// </summary>
		public Font(GLContext context, Stream stream)
		{
			this.context = context;
			this.chars = new Dictionary<char, int>();

			// magic.
			if (stream.ReadUInt32B() != 0x424D4603u)
				throw new InvalidDataException();

			this.texture = new GLTexture(context, TextureTarget.Texture2DArray);

			while (true)
			{
				int blockType = stream.ReadByte();
				if (blockType < 0)
					break;

				int blockLength = stream.ReadInt32L();
				byte[] block = stream.Read(blockLength);

				switch (blockType)
				{
					case 1: // info
					{
						for (int i = 14; i < blockLength; i++)
						{
							if (block[i] == 0)
								this.name = Encoding.UTF8.GetString(block, 14, i - 14);
						}

						break;
					}

					case 2: // common
					{
						this.lineHeight = block.ReadUInt16L(0);
						break;
					}

					case 3: // pages
					{
						List<Bitmap> bitmaps = new List<Bitmap>();

						int start = 0;
						for (int i = 0; i < blockLength; i++)
						{
							if (block[i] == 0)
							{
								if (i > start)
								{
									string pageName = Encoding.UTF8.GetString(block, start, i - start);
									Bitmap bmp = (Bitmap)(Resources.ResourceManager.GetObject(pageName) ?? Resources.ResourceManager.GetObject(Path.GetFileNameWithoutExtension(pageName)));
									bitmaps.Add(bmp);
								}

								start = i + 1;
							}
						}

						int width = bitmaps[0].Width;
						int height = bitmaps[0].Height;

						this.context.Invoke(() =>
						{
							// create texture array.
							GL.BindTexture(TextureTarget.Texture2DArray, this.texture.ID);
							GL.TexImage3D(TextureTarget.Texture2DArray, 0, PixelInternalFormat.Rgba8, width, height, bitmaps.Count, 0, OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
							GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
							GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

							// upload grayscale version of font.
							for (int i = 0; i < bitmaps.Count; i++)
							{
								Bitmap bmp = bitmaps[i];

								// copy data.
								BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
								GL.TexSubImage3D(TextureTarget.Texture2DArray, 0, 0, 0, i, bmp.Width, bmp.Height, 1, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
								bmp.UnlockBits(data);
							}
						});

						break;
					}

					case 4: // chars
					{
						GlyphData[] glyphs = new GlyphData[blockLength / 20];
						this.advance = new int[glyphs.Length];
						for (int i = 0; i < glyphs.Length; i++)
						{
							uint id = block.ReadUInt32L(20 * i + 0);
							int x = block.ReadUInt16L(20 * i + 4);
							int y = block.ReadUInt16L(20 * i + 6);
							int width = block.ReadUInt16L(20 * i + 8);
							int height = block.ReadUInt16L(20 * i + 10);
							int xOffset = block.ReadInt16L(20 * i + 12);
							int yOffset = block.ReadInt16L(20 * i + 14);
							int xAdvance = block.ReadInt16L(20 * i + 16);
							int page = block[20 * i + 18];
							glyphs[i] = new GlyphData(x, y, width, height, xOffset, yOffset, xAdvance, page);
							this.chars[(char)id] = i;
							this.advance[i] = xAdvance;
						}

						// upload glyph data.
						this.glyphBuffer = new GLBuffer(context, (IntPtr)(glyphs.Length * 32));
						this.glyphBuffer.Set(glyphs);
						break;
					}

					default:
						throw new InvalidDataException();
				}
			}

			this.program = new GLProgram(context, "Font");
			this.program.Attach(ShaderType.VertexShader, Helper.GetCode("FontVertex"));
			this.program.Attach(ShaderType.GeometryShader, Helper.GetCode("FontGeometry", s => s.Replace("MAXGLYPHS", this.advance.Length.ToString(CultureInfo.InvariantCulture))));
			this.program.Attach(ShaderType.FragmentShader, Helper.GetCode("FontFragment"));
			this.program.Link();
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Creates a new text buffer that uses this font.
		/// </summary>
		/// <returns></returns>
		public Text CreateText()
		{
			return new Text(this.context, this);
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			this.texture.Dispose();
			this.glyphBuffer.Dispose();
			this.program.Dispose();
		}

		#endregion
	}
}
