#version 330 core

layout (location = 0) in ivec3 inGlyph;

flat out ivec3 glyphInfo;

void main(void)
{
	glyphInfo = inGlyph;
}