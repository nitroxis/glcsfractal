#version 330 core

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

struct glyphData
{
	int x;
	int y;
	int width;
	int height;
	int xOffset;
	int yOffset;
	int xAdvance;
	int page;
};

uniform vec2 scale;
layout (std140) uniform glyphBuffer { glyphData glyphs[MAXGLYPHS]; };

flat in ivec3 glyphInfo[];
out vec3 texCoord;

void emit(ivec4 x, ivec4 y)
{
	texCoord = vec3(x.z, y.w, glyphs[glyphInfo[0].z].page);
	gl_Position = vec4(x.x * scale.x - 1.0, 1.0 - (y.y * scale.y), 0.0, 1.0);
	EmitVertex();
}

void main(void)
{
	glyphData g = glyphs[glyphInfo[0].z];

	ivec4 v1 = ivec4(glyphInfo[0].xy + ivec2(g.xOffset, g.yOffset), ivec2(g.x, g.y));
	ivec4 v2 = v1 + ivec2(g.width, g.height).xyxy;

	emit(v1, v1);
	emit(v1, v2);
	emit(v2, v1);
	emit(v2, v2);
	EndPrimitive();
}

