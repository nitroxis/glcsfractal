﻿using System;
using System.Runtime.InteropServices;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	/// <summary>
	/// Represents a text buffer.
	/// </summary>
	internal sealed class Text : DisposableObject
	{
		#region Nested Types

		[StructLayout(LayoutKind.Sequential)]
		private struct GlyphVertex
		{
			public readonly int X;
			public readonly int Y;
			public readonly int ID;

			public GlyphVertex(int x, int y, int id)
			{
				this.X = x;
				this.Y = y;
				this.ID = id;
			}
		}

		#endregion

		#region Fields

		private readonly GLContext context;
		private readonly Font font;

		private Color4 color;

		private GlyphVertex[] vboData;
		private int vboOffset;
		private readonly int vbo;
		private readonly int vao;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the color of the text.
		/// </summary>
		public Color4 Color
		{
			get { return this.color; }
			set { this.color = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Text.
		/// </summary>
		internal Text(GLContext context, Font font)
		{
			this.context = context;
			this.font = font;

			this.vboData = new GlyphVertex[0];
			this.vbo = this.context.Invoke(() => GL.GenBuffer());
			this.vao = this.context.Invoke(() => GL.GenVertexArray());
			this.context.Invoke(() =>
			{
				GL.BindBuffer(BufferTarget.ArrayBuffer, this.vbo);
				GL.BindVertexArray(this.vao);
				GL.EnableVertexAttribArray(0);
				GL.VertexAttribIPointer(0, 3, VertexAttribIntegerType.Int, 12, IntPtr.Zero);
			});
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clears the text buffer.
		/// </summary>
		public void Clear()
		{
			lock (this)
			{
				this.vboOffset = 0;
			}
		}

		/// <summary>
		/// Writes formatted text.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="format"></param>
		/// <param name="args"></param>
		public void Write(int x, int y, string format, params object[] args)
		{
			this.Write(x, y, string.Format(format, args));
		}

		/// <summary>
		/// Writes text.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="text"></param>
		public void Write(int x, int y, string text)
		{
			lock (this)
			{
				if (text.Length == 0)
					return;

				if (this.vboOffset + text.Length > this.vboData.Length)
					Array.Resize(ref this.vboData, this.vboOffset + text.Length);

				int currentX = x;
				int currentY = y;
				for (int i = 0; i < text.Length; i++)
				{
					int id;
					if (!this.font.chars.TryGetValue(text[i], out id))
						continue;

					this.vboData[this.vboOffset++] = new GlyphVertex(currentX, currentY, id);

					currentX += this.font.advance[id];
				}
			}
		}

		/// <summary>
		/// Draws the text.
		/// </summary>
		public void Draw()
		{
			lock (this)
			{
				this.context.Invoke(() =>
				{
					// get viewport.
					int[] viewport = new int[4];
					GL.GetInteger(GetPName.Viewport, viewport);

					GL.BindVertexArray(this.vao);

					// setup program.
					this.font.program.Use();

					GL.ActiveTexture(TextureUnit.Texture0);
					this.font.texture.Bind();
					GL.Uniform1(this.font.program.GetLocation("tex"), 0);
					GL.Uniform2(this.font.program.GetLocation("scale"), 2.0f / viewport[2], 2.0f / viewport[3]);
					this.font.glyphBuffer.BindRange(BufferRangeTarget.UniformBuffer, 0);

					// update VBO.
					GL.BindBuffer(BufferTarget.ArrayBuffer, this.vbo);
					GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(this.vboData.Length * 12), this.vboData, BufferUsageHint.DynamicDraw);

					// draw outline.
					GL.Uniform4(this.font.program.GetLocation("color"), new Color4(0.0f, 0.0f, 0.0f, color.A));
					GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
					GL.DrawArrays(PrimitiveType.Points, 0, this.vboOffset);

					// draw normal text.
					GL.Uniform4(this.font.program.GetLocation("color"), color);
					GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.One);
					GL.DrawArrays(PrimitiveType.Points, 0, this.vboOffset);
				});
			}
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			this.context.Invoke(() => GL.DeleteBuffer(this.vbo));
			this.context.Invoke(() => GL.DeleteVertexArray(this.vao));
		}

		#endregion
	}
}
