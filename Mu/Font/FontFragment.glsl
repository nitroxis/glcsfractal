#version 330 core

uniform sampler2DArray tex;
uniform vec4 color;

in vec3 texCoord;

out vec4 outColor;

void main(void)
{
	outColor = color * texelFetch(tex, ivec3(texCoord), 0);
}
