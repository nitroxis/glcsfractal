﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Mu.Properties;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Mu
{
	public static class Program
	{
		private static GameWindow window;
		private static GLContext context;
		private static GLAlgorithm fractal;
		private static Renderer renderer;
		private static bool invalid;
		private static bool run; 
		private static Stopwatch computeWatch;

		private static Font font;
		private static Text text;
		private static float textFade;
		
		public static void Main(string[] args)
		{
			Fixed a = Fixed.Create(new FixedFormat(4, 2));
			Fixed b = Fixed.Create(new FixedFormat(5, 1));
			Fixed.Add(a, b, a);

			Trace.Listeners.Add(new ConsoleTraceListener());
			run = true;
			
			// create window on another thread.
			var windowEvent = new ManualResetEvent(false);
			var windowThread = new Thread(() =>
			{
				Trace.WriteLine("Creating window...");
				window = new GameWindow(512, 512, GraphicsMode.Default, "Mu");
				// show the window.
				window.VSync = VSyncMode.Off;
				window.Visible = true;

				Trace.WriteLine("Releasing context...");
				window.Context.MakeCurrent(null);
				windowEvent.Set();

				Trace.WriteLine("Processing events...");
				Stopwatch watch = new Stopwatch();
				while (true)
				{
					watch.Restart();
					window.ProcessEvents();
					if (!window.Exists || window.IsExiting)
						break;
					Thread.Sleep(1);
					onUpdate(window, new FrameEventArgs(watch.Elapsed.TotalSeconds));
				}

				windowEvent.WaitOne();
				context.Stop();
				window.Dispose();
			});

			var glEvent = new ManualResetEvent(false);
			var glThread = new Thread(() =>
			{
				windowEvent.WaitOne();
				Trace.WriteLine("Acquiring context...");
				window.MakeCurrent();

				Trace.WriteLine("Processing OpenGL calls...");
				context = new GLContext();
				context.Run(glEvent);
			});

			windowThread.Start();
			glThread.Start();
			glEvent.WaitOne();
			
			//consoleFractal();

			// the fucking manual.
			Console.WriteLine("*******************************************");
			Console.WriteLine("ESC          Quit");
			Console.WriteLine("F11          Fullscreen");
			Console.WriteLine("F12          Screenshot");
			Console.WriteLine("1-4          Select algorithm.");
			Console.WriteLine("Ctrl+C       Copy SFT parameters.");
			Console.WriteLine("Ctrl+V       Paste SFT parameters.");
			Console.WriteLine();
			Console.WriteLine("Space        Reset");
			Console.WriteLine("LMB          Pan");
			Console.WriteLine("Shift+LMB    Center");
			Console.WriteLine("RMB / Wheel  Zoom");
			Console.WriteLine("+            Increase iterations (detail-level)");
			Console.WriteLine("-            Decrease iterations");
			Console.WriteLine();
			Console.WriteLine("Left Arrow   Increase gradient scale");
			Console.WriteLine("Right Arrow  Decrease gradient scale");
			Console.WriteLine("Up Arrow     Shift gradient");
			Console.WriteLine("Down Arrow   Shift gradient in the other direction");
			Console.WriteLine("KP0          Reset gradient");
			Console.WriteLine("M            Toggle debug markers (MandelbrotDelta only)");
			Console.WriteLine("*******************************************");

			// register some events.
			window.Closing += onClosing;
			window.UpdateFrame += onUpdate;
			window.KeyDown += onKeyDown;
			window.MouseMove += onMouseMove;
			window.MouseDown += onMouseDown;
			window.MouseWheel += onMouseWheel;
			window.Resize += onResize;

			// x11 workaround.
			while (!window.Visible)
				window.ProcessEvents();

			font = new Font(context, new MemoryStream((byte[])Resources.ResourceManager.GetObject("ProFont"), false));
			text = font.CreateText();

			setFractal(new MandelbrotFloat32(context));
			renderer = new Renderer(context);
			reset();

			computeWatch = new Stopwatch();
			windowEvent.Reset();

			while (run)
			{
				if (invalid)
				{
					// get coordinates.
					FixedComplex center = FixedComplex.Create(fractal.FixedFormat);
					Fixed zoom = Fixed.Create(fractal.FixedFormat);
					fractal.GetCoordinates(center, zoom);

					// write info.
					lock (font)
					{
						text.Clear();
						text.Write(1, 0 * font.LineHeight, "{0} with {1} iterations. {2}x{3}, {4}-bit", fractal.GetType().Name, fractal.MaxIterations, fractal.Width, fractal.Height, fractal.FixedFormat.Digits * 32);
						text.Write(1, 1 * font.LineHeight, "Re {0}", center.Real);
						text.Write(1, 2 * font.LineHeight, "Im {0}", center.Imaginary);
						text.Write(1, 3 * font.LineHeight, " @ {0}", zoom);
						textFade = float.PositiveInfinity;
					}

					// compute fractal.
					lock (fractal)
					{
						invalid = false;

						computeWatch.Restart();
						fractal.Compute();
						computeWatch.Stop();
						Trace.WriteLine(string.Format("{0:0.0} ms", computeWatch.Elapsed.TotalMilliseconds));
					}

					textFade = 2.0f;
				}
				else
				{
					// idle render.
					Thread.Sleep(10);
					render();
				}
			}

			Trace.WriteLine("Main loop exited.");
			
			fractal.Dispose();
			renderer.Dispose();
			text.Dispose();
			font.Dispose();

			GC.Collect();
			GC.WaitForPendingFinalizers();

			window.Closing -= onClosing;
			
			// TODO: x11 crashes after the window has been closed, investigate.
			window.Close();
			windowEvent.Set();
			windowThread.Join();
		}

		private static void onComputeProgress(object sender, EventArgs e)
		{
			render();
		}

		private static void setFractal(GLAlgorithm newFractal)
		{
			Algorithm old = fractal;

			if (old != null)
			{
				old.Cancel();
				old.ComputeProgress -= onComputeProgress;
			}

			fractal = newFractal;
			fractal.ComputeProgress += onComputeProgress;
			fractal.SetSize(window.ClientSize.Width, window.ClientSize.Height);

			if (old != null)
			{
				fractal.CopyFrom(old);
				old.Dispose();	
			}

			invalidate();
		}

		private static void reset()
		{
			fractal.Cancel();

			lock (fractal)
			{
				renderer.Scale = 1.0f;
				renderer.Offset = 0.0f;
				fractal.MaxIterations = 2048;
				fractal.Reset();
				invalidate();
			}
		}

		private static void invalidate()
		{
			invalid = true;
		}

		private static void saveScreenshot()
		{
			byte[] buffer = new byte[window.ClientSize.Width * window.ClientSize.Height * 3];
			GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

			context.Invoke(() => GL.ReadPixels(0, 0, window.ClientSize.Width, window.ClientSize.Height, PixelFormat.Bgr, PixelType.UnsignedByte, handle.AddrOfPinnedObject()));

			Bitmap bmp = new Bitmap(window.ClientSize.Width, window.ClientSize.Height, window.ClientSize.Width * 3, System.Drawing.Imaging.PixelFormat.Format24bppRgb, handle.AddrOfPinnedObject());

			string fileName = string.Format("{0:yyyyMMddHHmmss}.png", DateTime.Now);
			bmp.Save(fileName);

			handle.Free();

			Console.WriteLine("Screenshot saved as {0}.", fileName);
		}

		private static void render()
		{
			context.Invoke(() =>
			{
				GL.Viewport(0, 0, window.ClientSize.Width, window.ClientSize.Height);

				renderer.Render(fractal);
				text.Draw();
				
				window.SwapBuffers();
			});
		}

		#region Input Handling

		private static int mouseX;
		private static int mouseY;
		
		private static void toRelative(int ax, int ay, out double rx, out double ry)
		{
			double aspect = 1.0d / ((window.ClientSize.Height < window.ClientSize.Width) ? window.ClientSize.Height : window.ClientSize.Width);
			rx = ax * aspect;
			ry = ay * -aspect;
		}

		private static void toRelativeCenter(int ax, int ay, out double rx, out double ry)
		{
			double aspect = 1.0d / ((window.ClientSize.Height < window.ClientSize.Width) ? window.ClientSize.Height : window.ClientSize.Width);
			rx = (ax - window.ClientSize.Width / 2) * aspect;
			ry = (ay - window.ClientSize.Height / 2) * -aspect;
		}

		private static void onClosing(object sender, CancelEventArgs e)
		{
			e.Cancel = true;
			run = false;
			fractal.Cancel();
		}

		private static void onResize(object sender, EventArgs e)
		{
			fractal.Cancel();
			fractal.SetSize(window.ClientSize.Width, window.ClientSize.Height);
			invalidate();
		}

		private static void onUpdate(object sender, FrameEventArgs e)
		{
			if (fractal == null) return;

			// smooth zoom.
			if (window.Mouse[OpenTK.Input.MouseButton.Right])
			{
				double rx, ry;
				toRelativeCenter(window.Mouse.X, window.Mouse.Y, out rx, out ry);

				double factor = window.Mouse[OpenTK.Input.MouseButton.Left] ? 10.0d / 9.0d : 0.9d;

				fractal.Cancel();
				lock (fractal)
				{
					fractal.ZoomToPoint(Math.Pow(factor, e.Time * 100.0d), rx, ry);
					invalidate();
				}
			}

			if (text != null)
			{
				textFade -= (float)e.Time;
				text.Color = new Color4(1.0f, 1.0f, 1.0f, Math.Min(Math.Max(textFade, 0.0f), 1.0f));
			}
		}
		
		private static void onKeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			if (fractal == null) return;

			if (e.Key == OpenTK.Input.Key.Left)
			{
				renderer.Scale *= 1.025f;
				render();
			}

			if (e.Key == OpenTK.Input.Key.Right)
			{
				renderer.Scale /= 1.025f;
				render();
			}

			if (e.Key == OpenTK.Input.Key.Up)
			{
				renderer.Offset += 0.01f;
				render();
			}

			if (e.Key == OpenTK.Input.Key.Down)
			{
				renderer.Offset -= 0.01f;
				render();
			}
			
			if (e.Key == OpenTK.Input.Key.Plus || e.Key == OpenTK.Input.Key.KeypadPlus)
			{
				fractal.Cancel();
				lock (fractal)
				{
					fractal.MaxIterations += Math.Max(1u, fractal.MaxIterations / 10);
					invalidate();
				}
			}

			if (e.Key == OpenTK.Input.Key.Minus || e.Key == OpenTK.Input.Key.KeypadMinus)
			{
				fractal.Cancel();

				lock (fractal)
				{
					fractal.MaxIterations -= Math.Max(1u, fractal.MaxIterations / 10);

					if (fractal.MaxIterations < 1)
						fractal.MaxIterations = 1;

					invalidate();
				}
			}

			if (e.IsRepeat)
				return;

			if (e.Key == OpenTK.Input.Key.Escape)
			{
				window.Close();
			}
			else if (e.Key == OpenTK.Input.Key.F11)
			{
				if (window.WindowState == WindowState.Fullscreen)
					window.WindowState = WindowState.Normal;
				else
					window.WindowState = WindowState.Fullscreen;
			}
			else if (e.Key == OpenTK.Input.Key.F12)
			{
				saveScreenshot();
			}
			else if (e.Key == OpenTK.Input.Key.Space)
			{
				reset();
			}
			else if (e.Key == OpenTK.Input.Key.Keypad0)
			{
				renderer.Scale = 1.0f;
				renderer.Offset = 0.0f;
				render();
			}
			else if (e.Key == OpenTK.Input.Key.R)
			{
				renderer.RandomizePalette();
				render();
			}
			else if (e.Key == OpenTK.Input.Key.M)
			{
				renderer.ShowMarkers = !renderer.ShowMarkers;
				render();
			}
			else if (e.Key == OpenTK.Input.Key.C && e.Modifiers.HasFlag(OpenTK.Input.KeyModifiers.Control))
			{
				try
				{
					string str = toSFT();

					// set clipboard text in a STA thread.
					Thread thread = new Thread(() => System.Windows.Forms.Clipboard.SetText(str));
					thread.SetApartmentState(ApartmentState.STA);
					thread.Start();
				}
				catch (Exception bug)
				{
					Trace.WriteLine(string.Format("Failed to copy to clipboard: {0}", bug.Message));
				}
			}
			else if (e.Key == OpenTK.Input.Key.V && e.Modifiers.HasFlag(OpenTK.Input.KeyModifiers.Control))
			{
				try
				{
					string str = System.Windows.Forms.Clipboard.GetText();

					// read text from clipboard in a STA thread.
					ManualResetEvent signal = new ManualResetEvent(false);
					Thread thread = new Thread(() =>
					{
						str = System.Windows.Forms.Clipboard.GetText();
						signal.Set();
					});
					thread.SetApartmentState(ApartmentState.STA);
					thread.Start();
					signal.WaitOne();

					// check data.
					if (string.IsNullOrWhiteSpace(str)) return;

					// try to parse it.
					try
					{
						fromSFT(str);
					}
					catch (Exception bug)
					{
						Trace.WriteLine(string.Format("Failed to parse parameters: {0}", bug.Message));
					}
				}
				catch (Exception bug)
				{
					Trace.WriteLine(string.Format("Failed to paste from clipboard: {0}", bug.Message));
				}
			}
			else if (e.Key == OpenTK.Input.Key.Number1)
			{
				fractal.Cancel();
				setFractal(new MandelbrotFloat32(context));
			}
			else if (e.Key == OpenTK.Input.Key.Number2)
			{
				fractal.Cancel();
				setFractal(new MandelbrotFloat64(context));
			}
			else if (e.Key == OpenTK.Input.Key.Number3)
			{
				fractal.Cancel();
				setFractal(new MandelbrotFixed(context));
			}
			else if (e.Key == OpenTK.Input.Key.Number4)
			{
				fractal.Cancel();
				setFractal(new MandelbrotDelta(context));
			}
			

			else if (e.Key == OpenTK.Input.Key.PageUp)
			{
				fractal.Cancel();
				lock (fractal)
				{
					fractal.ZoomToCenter(10.0d);
					invalidate();
				}
			}

			MandelbrotDelta m = fractal as MandelbrotDelta;
			if (m != null)
			{
				if (e.Key == OpenTK.Input.Key.End)
				{
					if (e.Modifiers == OpenTK.Input.KeyModifiers.Shift)
						m.ApproximatedIterations -= Math.Min(m.ApproximatedIterations, 100);
					else if (m.ApproximatedIterations > 0)
						m.ApproximatedIterations--;
					
					Console.WriteLine("approx: {0}", m.ApproximatedIterations);
					invalidate();
				}
				else if (e.Key == OpenTK.Input.Key.Home)
				{
					if (e.Modifiers == OpenTK.Input.KeyModifiers.Shift)
						m.ApproximatedIterations += 100;
					else
						m.ApproximatedIterations++;

					Console.WriteLine("approx: {0}", m.ApproximatedIterations);
					invalidate();
				}
			}
		}

		private static void onMouseMove(object sender, OpenTK.Input.MouseMoveEventArgs e)
		{
			if (fractal == null) return;

			if (e.Mouse[OpenTK.Input.MouseButton.Left] && !window.Keyboard[OpenTK.Input.Key.ShiftLeft])
			{
				double rx, ry;
				toRelative(mouseX - e.X, mouseY - e.Y, out rx, out ry);
				fractal.Cancel();
				lock (fractal)
				{
					fractal.MoveCenter(rx, ry);
					invalidate();
				}
			}

			mouseX = e.X;
			mouseY = e.Y;
		}

		private static void onMouseDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
		{
			if (fractal == null) return;

			if (e.Mouse[OpenTK.Input.MouseButton.Left] && window.Keyboard[OpenTK.Input.Key.ShiftLeft])
			{
				double rx, ry;
				toRelative(e.X - window.ClientSize.Width / 2, e.Y - window.ClientSize.Height / 2, out rx, out ry);
				fractal.Cancel();
				lock (fractal)
				{
					fractal.MoveCenter(rx, ry);
					invalidate();
				}
			}
		}

		private static void onMouseWheel(object sender, OpenTK.Input.MouseWheelEventArgs e)
		{
			if (fractal == null) return;

			double rx, ry;
			toRelativeCenter(e.X, e.Y, out rx, out ry);
			fractal.Cancel();
			lock (fractal)
			{
				fractal.ZoomToPoint(Math.Pow(0.8, e.DeltaPrecise), rx, ry);
				invalidate();
			}
		}

		#endregion

		#region IO

		private static string toSFT()
		{
			StringBuilder str = new StringBuilder();

			FixedComplex center = FixedComplex.Create(fractal.FixedFormat);
			Fixed zoom = Fixed.Create(fractal.FixedFormat);
			fractal.GetCoordinates(center, zoom);
			Fixed.Negate(center.Imaginary, center.Imaginary);

			str.Append("s=");
			str.AppendLine(zoom.ToString(10000));

			str.Append("r=");
			str.AppendLine(center.Real.ToString(10000));

			str.Append("i=");
			str.AppendLine(center.Imaginary.ToString(10000));

			str.Append("iteration_limit=");
			str.AppendLine(fractal.MaxIterations.ToString());

			return str.ToString();
		}

		private static void fromSFT(string str)
		{
			FixedFormat format = new FixedFormat(32, 31);

			FixedComplex center = FixedComplex.Create(format);
			Fixed zoom = Fixed.Create(format);
			fractal.GetCoordinates(center, zoom);
			uint maxIterations = fractal.MaxIterations;

			Match match = Regex.Match(str, @"^\s*((\+|\-)?\s*[0-9\.eE\+\-]+)\s*((\+|\-)?\s*[0-9\.eE\+\-]+)\s*i(\s*@\s*((\+|\-)?\s*[0-9\.eE\+\-]+))?\s*$");
			if (match.Success)
			{
				center.Real.Set(match.Groups[1].Value);
				center.Imaginary.Set(match.Groups[3].Value);

				if(match.Groups[6].Success)
					zoom.Set(match.Groups[6].Value);
			}
			else
			{
				MatchCollection matches = Regex.Matches(str, @"(s|r|i|iteration_limit)\s*=\s*((\+|\-)?\s*[0-9\.eE\+\-]+)", RegexOptions.CultureInvariant | RegexOptions.Multiline);

				for (int i = 0; i < matches.Count; i++)
				{
					string name = matches[i].Groups[1].Value;

					if (name == "s")
						zoom.Set(matches[i].Groups[2].Value);
					else if (name == "r")
						center.Real.Set(matches[i].Groups[2].Value);
					else if (name == "i")
						center.Imaginary.Set(matches[i].Groups[2].Value);
					else if (name == "iteration_limit")
						maxIterations = uint.Parse(matches[i].Groups[2].Value);
				}

				// SFT has -1 - 1i in the upper left corner.
				// we have -1 - 1i in the lower left corner, like in a standard math graph.
				// so we just negate the imaginary component
				Fixed.Negate(center.Imaginary, center.Imaginary);
			}

			int fpLength = (zoom.Format.Digits * 32 - zoom.MostSignificantBit + 64) / 32;

			if (fpLength < 2)
				fpLength = 2;

			fractal.Cancel();
			lock (fractal)
			{
				fractal.FixedFormat = new FixedFormat(fpLength, fpLength - 1);
				fractal.SetCoordinates(center, zoom);
				fractal.MaxIterations = maxIterations;
				invalidate();
			}
		}

		#endregion

		#region Console

		private static void consoleFractal()
		{
			using (Mandelbrot testFractal = new MandelbrotFixed(context))
			{
				testFractal.SetSize(32, 32);
				testFractal.MaxIterations = 12;
				testFractal.Compute();

				uint[] img = new uint[testFractal.Width * testFractal.Height];
				context.Invoke(() =>
				{
					testFractal.Texture.Bind();
					GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.RedInteger, PixelType.UnsignedInt, img);
				});

				int i = 0;
				for (int y = 0; y < testFractal.Height; y++)
				{
					for (int x = 0; x < testFractal.Width; x++)
					{
						uint v = img[i++];
						writeBrightness(v);
						writeBrightness(v);
					}
					Console.WriteLine();
				}
			}

			Console.ResetColor();
		}

		private static void writeBrightness(uint level)
		{
			switch (level / 4)
			{
				case 0:
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.DarkGray;
					break;
				case 1:
					Console.BackgroundColor = ConsoleColor.DarkGray;
					Console.ForegroundColor = ConsoleColor.Gray;
					break;
				case 2:
					Console.BackgroundColor = ConsoleColor.Gray;
					Console.ForegroundColor = ConsoleColor.White;
					break;
				default:
					Console.BackgroundColor = ConsoleColor.White;
					Console.ForegroundColor = ConsoleColor.Red;
					break;
			}

			switch (level % 4)
			{
				case 0:
					Console.Write(" ");
					break;
				case 1:
					Console.Write(".");
					break;
				case 2:
					Console.Write("v");
					break;
				case 3:
					Console.Write("W");
					break;
			}
		}

		#endregion
	}
}
