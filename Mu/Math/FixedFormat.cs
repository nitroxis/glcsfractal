﻿using System;

namespace Mu
{
	public struct FixedFormat
	{
		#region Fields

		public readonly ushort Digits;
		public readonly ushort RadixPoint;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FixedFormat.
		/// </summary>
		public FixedFormat(ushort digits, ushort radixPoint)
		{
			this.Digits = digits;
			this.RadixPoint = radixPoint;
		}

		/// <summary>
		/// Creates a new FixedFormat.
		/// </summary>
		public FixedFormat(int digits, int radixPoint)
		{
			if (digits < 0 || digits > ushort.MaxValue || radixPoint < 0 || radixPoint > ushort.MaxValue)
				throw new ArgumentException();

			this.Digits = (ushort)digits;
			this.RadixPoint = (ushort)radixPoint;
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			if ((obj is FixedFormat))
			{
				FixedFormat format = (FixedFormat)obj;
				return format.Digits == this.Digits && format.RadixPoint == this.RadixPoint;
			}

			return false;
		}

		public override int GetHashCode()
		{
			return this.Digits | (this.RadixPoint << 16);
		}

		public static bool operator ==(FixedFormat a, FixedFormat b)
		{
			return a.Digits == b.Digits && a.RadixPoint == b.RadixPoint;
		}

		public static bool operator !=(FixedFormat a, FixedFormat b)
		{
			return a.Digits != b.Digits || a.RadixPoint != b.RadixPoint;
		}

		#endregion
	}
}
