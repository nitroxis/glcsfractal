﻿using System;

namespace Mu
{
	/// <summary>
	/// Represents a multi-precision fixed-point number.
	/// A fixed point number consists of digits (aka limbs) in base(2^32), i.e. unsigned 32-bit integers.
	/// </summary>
	public abstract partial class Fixed : IComparable<Fixed>
	{
		#region Fields

		public readonly FixedFormat Format;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets a value that indicates whether this number is negative.
		/// </summary>
		public virtual bool IsNegative
		{
			get { return (this.GetDigit(this.Format.Digits - 1) & 0x80000000u) != 0; }
		}

		/// <summary>
		/// Gets a value that indicates whether this number is zero.
		/// </summary>
		public virtual bool IsZero
		{
			get
			{
				for (int i = 0; i < this.Format.Digits; i++)
				{
					if (this.GetDigit(i) != 0)
						return false;
				}
				return true;
			}
		}

		/// <summary>
		/// Gets the index of the most significant set bit.
		/// </summary>
		/// <returns></returns>
		public virtual int MostSignificantBit
		{
			get
			{
				for (int i = this.Format.Digits - 1; i >= 0; i--)
				{
					uint digit = this.GetDigit(i);
					for (int j = 31; j >= 0; j--)
					{
						if ((digit & (1u << j)) != 0)
							return i * 32 + j;
					}
				}
				return -1;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		protected Fixed(FixedFormat format)
		{
			this.Format = format;
		}

		#endregion

		#region Methods

		#region Implementation

		/// <summary>
		/// Returns the digit of the number.
		/// </summary>
		/// <param name="index">The index of the digit.</param>
		/// <returns>The current value.</returns>
		protected abstract uint GetDigit(int index);

		/// <summary>
		/// Sets a digit of the number.
		/// </summary>
		/// <param name="index">The index of the digit.</param>
		/// <param name="value">The new value.</param>
		protected abstract void SetDigit(int index, uint value);

		/// <summary>
		/// Gets the digits of the number.
		/// </summary>
		/// <returns></returns>
		public virtual uint[] GetDigits()
		{
			uint[] array = new uint[this.Format.Digits];

			for (int i = 0; i < array.Length; i++)
				array[i] = this.GetDigit(i);

			return array;
		}

		/// <summary>
		/// Sets all digits.
		/// </summary>
		/// <param name="digits"></param>
		protected virtual void SetDigits(uint[] digits)
		{
			if (digits.Length != this.Format.Digits)
				throw new ArgumentException("Invalid number of digits.");

			for (int i = 0; i < digits.Length; i++)
				this.SetDigit(i, digits[i]);
		}

		/// <summary>
		/// Copies the digits of this number into the specified array at the specified offset.
		/// </summary>
		/// <param name="array">The array to copy to.</param>
		/// <param name="arrayOffset">The offset in the array.</param>
		public virtual void CopyDigits(uint[] array, int arrayOffset)
		{
			for (int i = 0; i < this.Format.Digits; i++)
				array[arrayOffset + i] = this.GetDigit(i);
		}

		#endregion

		#region Instance

		/// <summary>
		/// Sets the value of the fixed-point number to an integer value.
		/// </summary>
		/// <param name="value"></param>
		public virtual void Set(int value)
		{
			// clear fraction.
			for (int i = 0; i < this.Format.RadixPoint; i++)
				this.SetDigit(i, 0u);

			// set our value.
			this.SetDigit(this.Format.RadixPoint, unchecked((uint)value));

			// extend sign of value.
			uint signExtend = value < 0 ? uint.MaxValue : 0;
			for (int i = this.Format.RadixPoint + 1; i < this.Format.Digits; i++)
				this.SetDigit(i, signExtend);
		}

		/// <summary>
		/// Sets the value of the fixed-point number to a double-precision floating-point value.
		/// </summary>
		/// <param name="value"></param>
		public virtual void Set(double value)
		{
			bool negate = false;

			if (value < 0.0d)
			{
				negate = true;
				value = -value;
			}

			for (int i = this.Format.Digits - 1; i >= 0; i--)
				this.SetDigit(i, (uint)(value / getFactor(i - this.Format.RadixPoint)));
			
			if (negate)
				this.AssignNegate(this);
		}

		/// <summary>
		/// Copies the digits of the specified fixed-point number to this number.
		/// </summary>
		/// <param name="value"></param>
		public virtual void Set(Fixed value)
		{
			for (int i = 0; i < this.Format.Digits; i++)
			{
				int j = i - this.Format.RadixPoint + value.Format.RadixPoint;
				if (j >= 0 && j < value.Format.Digits)
					this.SetDigit(i, value.GetDigit(j));
				else
					this.SetDigit(i, 0u);
			}
		}

		/// <summary>
		/// Parses a string and sets the value of the fixed-point number to the parsed value.
		/// </summary>
		/// <param name="str"></param>
		public void Set(string str)
		{
			str = str.Trim().ToLowerInvariant();
			if (str.Length == 0)
				throw new FormatException("Empty string.");

			bool negative = false;
			if (str[0] == '-')
			{
				negative = true;
				str = str.Substring(1).TrimStart();
			}
			else if (str[0] == '+')
			{
				str = str.Substring(1).TrimStart();
			}

			// determine exponent.
			int exponent = 0;
			int exponentIndex = str.IndexOf('e');

			if (exponentIndex == 0)
				throw new FormatException("Invalid exponent format.");

			if (exponentIndex > 0)
			{
				string exponentStr = str.Substring(exponentIndex + 1, str.Length - exponentIndex - 1);
				exponent = int.Parse(exponentStr);
				str = str.Substring(0, exponentIndex);
			}

			// adjust exponent based on period position.
			int periodIndex = str.IndexOf('.');
			if (periodIndex >= 0)
			{
				str = str.Substring(0, periodIndex) + str.Substring(periodIndex + 1);
				exponent -= str.Length - periodIndex;
			}

			int fractOffset = str.Length + exponent;
			int fractSkip = 0;
			if (fractOffset < 0)
			{
				fractSkip = -fractOffset;
				fractOffset = 0;
			}

			// convert integer part from base 10 to base 256
			byte[] base10 = new byte[fractOffset];
			for (int i = 0, j = base10.Length - 1; i < str.Length && i < base10.Length; i++,j--)
			{
				if (str[i] < '0' || str[i] > '9')
					throw new FormatException();

				base10[j] = (byte)(str[i] - '0');
			}
			byte[] base256 = convertBase(10, 256, base10, 0, base10.Length, (this.Format.Digits - this.Format.RadixPoint) * 4);

			// store bytes in digits.
			for (int i = this.Format.RadixPoint, j = 0; i < this.Format.Digits; i++)
			{
				uint digit = 0;
				digit |= (uint)base256[j++];
				digit |= (uint)base256[j++] << 8;
				digit |= (uint)base256[j++] << 16;
				digit |= (uint)base256[j++] << 24;
				this.SetDigit(i, digit);
			}

			// convert fractional part from base 10 to base 256
			base10 = new byte[Math.Max(0, -exponent)];
			for (int i = fractOffset, j = fractSkip; i < str.Length; i++, j++)
			{
				if (str[i] < '0' || str[i] > '9')
					throw new FormatException();

				base10[j] = (byte)(str[i] - '0');
			}
			base256 = convertBaseFract(10, 256, base10, 0, base10.Length, 4 * this.Format.RadixPoint);

			// store bytes in digits.
			for (int i = this.Format.RadixPoint - 1, j = 0; i >= 0; i--)
			{
				uint digit = 0;
				digit |= (uint)base256[j++] << 24;
				digit |= (uint)base256[j++] << 16;
				digit |= (uint)base256[j++] << 8;
				digit |= (uint)base256[j++];
				this.SetDigit(i, digit);
			}

			// negate if needed
			if (negative)
				this.AssignNegate(this);
		}

		/// <summary>
		/// Converts the fixed-point number to a double.
		/// </summary>
		/// <returns></returns>
		public virtual double ToDouble()
		{
			Fixed absolute;
			if (this.IsNegative)
			{
				absolute = Fixed.Create(this.Format);
				absolute.AssignNegate(this);
			}
			else
			{
				absolute = this;
			}

			double value = 0.0d;

			for (int i = this.Format.Digits - 1; i >= 0; i--)
				value += absolute.GetDigit(i) * getFactor(i - this.Format.RadixPoint);
			
			if (this.IsNegative)
				return -value;

			return value;
		}

		/// <summary>
		/// Gets the hash code of the number.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			uint[] digits = this.GetDigits();
			uint hash = 0;

			for (int i = 0; i < digits.Length; i++)
				hash ^= digits[i];

			return unchecked((int)hash ^ this.Format.GetHashCode());
		}

		#endregion

		#region Static

		private static double getFactor(int i)
		{
			const double scale = 1ul << 32;
			const double scaleInv = 1.0d / scale;

			double weight = 1.0d;

			while (i > 0)
			{
				weight *= scale;
				i--;
			}

			while (i < 0)
			{
				weight *= scaleInv;
				i++;
			}

			return weight;
		}

		/// <summary>
		/// Creates a new fixed-point number and sets it to the specified initial value.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Fixed Create(FixedFormat format, int value)
		{
			Fixed f = Fixed.Create(format);
			f.Set(value);
			return f;
		}

		/// <summary>
		/// Creates a new fixed-point number and sets it to the specified initial value.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Fixed Create(FixedFormat format, double value)
		{
			Fixed f = Fixed.Create(format);
			f.Set(value);
			return f;
		}

		/// <summary>
		/// Creates a new fixed-point number and sets it to the specified initial value.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Fixed Create(FixedFormat format, Fixed value)
		{
			Fixed f = Fixed.Create(format);
			f.Set(value);
			return f;
		}

		/// <summary>
		/// Creates a new fixed-point number and sets it to the specified initial value.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Fixed Create(FixedFormat format, string value)
		{
			Fixed f = Fixed.Create(format);
			f.Set(value);
			return f;
		}

		#endregion

		#endregion
	}
}
