﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace Mu
{
	public abstract partial class Fixed
	{
		#region Nested Types

		private sealed class Context
		{
			public readonly FixedFormat Format;
			public TypeBuilder FixedType;
			public ConstructorInfo FixedConstructor;
			public readonly FieldBuilder[] Fields;

			public Context(FixedFormat format)
			{
				this.Format = format;
				this.Fields = new FieldBuilder[format.Digits];
			}
		}

		#endregion

		#region Delegates

		private delegate Fixed CreateFunc();

		#endregion

		#region Fields

		private static readonly AssemblyBuilder assembly;
		private static readonly ModuleBuilder module;
		private static readonly Dictionary<FixedFormat, CreateFunc> createFuncs;
		
		#endregion

		#region Constructors

		static Fixed()
		{
			createFuncs = new Dictionary<FixedFormat, CreateFunc>();

			AssemblyName assemblyName = new AssemblyName("FixedPoint");

			assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
			module = assembly.DefineDynamicModule(assemblyName.Name);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a new fixed-point number.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public static Fixed Create(FixedFormat format)
		{
			CreateFunc func;

			lock (createFuncs)
			{
				if (!createFuncs.TryGetValue(format, out func))
				{
					// create generator context.
					Context context = new Context(format);
					context.FixedType = module.DefineType(string.Format("FP{0}_{1}", 32 * (format.Digits - format.RadixPoint), 32 * (format.RadixPoint)), TypeAttributes.Sealed | TypeAttributes.Public, typeof(Fixed));
					Trace.WriteLine(string.Format("Generating type \"{0}\".", context.FixedType.Name));

					// add digit fields.
					for (int i = 0; i < format.Digits; i++)
						context.Fields[i] = context.FixedType.DefineField("_" + i, typeof(uint), FieldAttributes.Assembly);

					generateFixedConstructor(context);

					generateIsZero(context);
					generateIsNegative(context);
					generateMostSignificantBit(context);

					generateSetDigit(context);
					generateGetDigit(context);
					generateSetDigits(context);
					generateCopyDigits(context);

					generateSetDouble(context);
					generateToDouble(context);
					
					generateCompareTo(context);

					generateAssignNegate(context);
					generateAssignAdd(context);
					generateAssignSubtract(context);
					generateAssignMultiply(context);

					MethodBuilder method = context.FixedType.DefineMethod("Create", MethodAttributes.Public | MethodAttributes.Static, typeof(Fixed), new Type[0]);
					ILGenerator il = method.GetILGenerator();
					il.Emit(OpCodes.Newobj, context.FixedConstructor);
					il.Emit(OpCodes.Ret);

					// create delegate.
					Type type = context.FixedType.CreateType();
					MethodInfo createImpl = type.GetMethod(method.Name, BindingFlags.Public | BindingFlags.Static);
					func = (CreateFunc)createImpl.CreateDelegate(typeof(CreateFunc));
					createFuncs[format] = func;
				}
			}

			return func();
		}

		private static MethodBuilder overrideMethod(TypeBuilder typeBuilder, string name, Type returnType, params Type[] parameters)
		{
			MethodBuilder method = typeBuilder.DefineMethod(name, MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.Final, returnType, parameters);
			MethodInfo baseMethod = typeBuilder.BaseType.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, parameters, null);
			typeBuilder.DefineMethodOverride(method, baseMethod);
			return method;
		}
		
		private static void generateTypeCheck(Context context, ILGenerator il, Action loadSource)
		{
			FieldInfo formatField = typeof(Fixed).GetField("Format", BindingFlags.Public | BindingFlags.Instance);
			FieldInfo digitsFields = typeof(FixedFormat).GetField("Digits", BindingFlags.Public | BindingFlags.Instance);
			FieldInfo radixPointField = typeof(FixedFormat).GetField("RadixPoint", BindingFlags.Public | BindingFlags.Instance);
			
			Label throwLabel = il.DefineLabel();
			Label end = il.DefineLabel();

			loadSource();
			il.Emit(OpCodes.Ldfld, formatField);
			il.Emit(OpCodes.Ldfld, digitsFields);
			il.Emit(OpCodes.Ldc_I4, context.Format.Digits);
			il.Emit(OpCodes.Bne_Un, throwLabel);

			loadSource(); 
			il.Emit(OpCodes.Ldfld, formatField);
			il.Emit(OpCodes.Ldfld, radixPointField);
			il.Emit(OpCodes.Ldc_I4, context.Format.RadixPoint);
			il.Emit(OpCodes.Bne_Un, throwLabel);
			il.Emit(OpCodes.Br, end);

			il.MarkLabel(throwLabel);
			il.Emit(OpCodes.Newobj, typeof(ArgumentException).GetConstructor(new Type[0]));
			il.Emit(OpCodes.Throw);
			il.MarkLabel(end);
		}

		private static void generateFixedConstructor(Context context)
		{
			ConstructorBuilder constructor = context.FixedType.DefineConstructor(MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName, CallingConventions.HasThis, null);
			ILGenerator il = constructor.GetILGenerator();
			
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldc_I4, context.Format.Digits);
			il.Emit(OpCodes.Ldc_I4, context.Format.RadixPoint);
			il.Emit(OpCodes.Newobj, typeof(FixedFormat).GetConstructor(new[] {typeof(int), typeof(int)}));
			il.Emit(OpCodes.Call, typeof(Fixed).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(FixedFormat) }, null));
			il.Emit(OpCodes.Ret);

			context.FixedConstructor = constructor;
		}

		private static void generateIsNegative(Context context)
		{
			MethodBuilder method = context.FixedType.DefineMethod("get_IsNegative", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.Final | MethodAttributes.SpecialName, typeof(bool), null);
			ILGenerator il = method.GetILGenerator();

			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, 0x80000000);
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Ldc_I4, 0x80000000);
			il.Emit(OpCodes.Ceq);
			il.Emit(OpCodes.Ret);

			context.FixedType.DefineMethodOverride(method, typeof(Fixed).GetMethod("get_IsNegative"));
		}

		private static void generateIsZero(Context context)
		{
			MethodBuilder method = context.FixedType.DefineMethod("get_IsZero", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.Final | MethodAttributes.SpecialName, typeof(bool), null);
			ILGenerator il = method.GetILGenerator();

			Label falseLabel = il.DefineLabel();

			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Brtrue, falseLabel);
			}
			
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Ret);

			il.MarkLabel(falseLabel);
			il.Emit(OpCodes.Ldc_I4_0);
			il.Emit(OpCodes.Ret);

			context.FixedType.DefineMethodOverride(method, typeof(Fixed).GetMethod("get_IsZero"));
		}

		private static void generateMostSignificantBit(Context context)
		{
			MethodBuilder method = context.FixedType.DefineMethod("get_MostSignificantBit", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.ReuseSlot | MethodAttributes.Virtual | MethodAttributes.Final | MethodAttributes.SpecialName, typeof(int), null);
			ILGenerator il = method.GetILGenerator();

			for (int i = context.Format.Digits - 1; i >= 0; i--)
			{
				for (int j = 31; j >= 0; j--)
				{
					Label skipLabel = il.DefineLabel();

					il.Emit(OpCodes.Ldarg_0);
					il.Emit(OpCodes.Ldfld, context.Fields[i]);
					il.Emit(OpCodes.Ldc_I4, 1 << j);
					il.Emit(OpCodes.And);
					il.Emit(OpCodes.Brfalse, skipLabel);
					il.Emit(OpCodes.Ldc_I4, i * 32 + j);
					il.Emit(OpCodes.Ret);

					il.MarkLabel(skipLabel);
				}
			}

			il.Emit(OpCodes.Ldc_I4_M1);
			il.Emit(OpCodes.Ret);
		}

		private static void generateGetDigit(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "GetDigit", typeof(uint), typeof(int));
			ILGenerator il = method.GetILGenerator();

			Label[] jumpTable = new Label[context.Format.Digits];
			for (int i = 0; i < context.Format.Digits; i++)
				jumpTable[i] = il.DefineLabel();

			Label defaultCase = il.DefineLabel();
			Label end = il.DefineLabel();

			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Switch, jumpTable);
			il.Emit(OpCodes.Br, defaultCase);

			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.MarkLabel(jumpTable[i]);
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Br, end);
			}

			il.MarkLabel(defaultCase);
			il.Emit(OpCodes.Newobj, typeof(IndexOutOfRangeException).GetConstructor(new Type[0]));
			il.Emit(OpCodes.Throw);
			il.MarkLabel(end);
			il.Emit(OpCodes.Ret);
		}

		private static void generateSetDigit(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "SetDigit", null, typeof(int), typeof(uint));
			ILGenerator il = method.GetILGenerator();

			Label[] jumpTable = new Label[context.Format.Digits];
			for (int i = 0; i < context.Format.Digits; i++)
				jumpTable[i] = il.DefineLabel();

			Label defaultCase = il.DefineLabel();
			Label end = il.DefineLabel();

			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Switch, jumpTable);
			il.Emit(OpCodes.Br, defaultCase);

			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.MarkLabel(jumpTable[i]);
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldarg_2);
				il.Emit(OpCodes.Stfld, context.Fields[i]);
				il.Emit(OpCodes.Br, end);
			}

			il.MarkLabel(defaultCase);
			il.Emit(OpCodes.Newobj, typeof(IndexOutOfRangeException).GetConstructor(new Type[0]));
			il.Emit(OpCodes.Throw);
			il.MarkLabel(end);
			il.Emit(OpCodes.Ret);
		}

		private static void generateSetDouble(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "Set", null, typeof(double));
			ILGenerator il = method.GetILGenerator();

			LocalBuilder negate = il.DeclareLocal(typeof(bool));
			LocalBuilder temp = il.DeclareLocal(typeof(double));

			Label positive = il.DefineLabel();
			Label begin = il.DefineLabel();

			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldc_R8, 0.0d);
			il.Emit(OpCodes.Bgt, positive);

			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Neg);
			il.Emit(OpCodes.Stloc, temp);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Stloc, negate);
			il.Emit(OpCodes.Br, begin);

			il.MarkLabel(positive);
			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Stloc, temp);

			il.MarkLabel(begin);
			for (int i = context.Format.Digits - 1; i >= 0; i--)
			{
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldloc, temp);
				il.Emit(OpCodes.Ldc_R8, getFactor(i - context.Format.RadixPoint));
				il.Emit(OpCodes.Div);
				il.Emit(OpCodes.Conv_U4);
				il.Emit(OpCodes.Stfld, context.Fields[i]);
			}

			Label returnLabel = il.DefineLabel();

			il.Emit(OpCodes.Ldloc, negate);
			il.Emit(OpCodes.Brfalse, returnLabel);

			generateNegateBody(context, il, () => il.Emit(OpCodes.Ldarg_0), () => il.Emit(OpCodes.Ldarg_0));

			il.MarkLabel(returnLabel);
			il.Emit(OpCodes.Ret);
		}

		private static void generateToDouble(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "ToDouble", typeof(double));
			ILGenerator il = method.GetILGenerator();

			LocalBuilder absolute = il.DeclareLocal(context.FixedType);
			Label positive = il.DefineLabel();
			Label beginLabel = il.DefineLabel();

			// check sign.
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, 0x80000000);
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Brfalse, positive); // not negative? skip the following.

			// absolute value.
			il.Emit(OpCodes.Newobj, context.FixedConstructor);
			il.Emit(OpCodes.Stloc, absolute);
			generateNegateBody(context, il, () => il.Emit(OpCodes.Ldarg_0), () => il.Emit(OpCodes.Ldloc, absolute)); // negate.
			il.Emit(OpCodes.Br, beginLabel);

			// this value.
			il.MarkLabel(positive);
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Stloc, absolute);

			// conversion.
			il.MarkLabel(beginLabel);
			for (int i = context.Format.Digits - 1; i >= 0; i--)
			{
				il.Emit(OpCodes.Ldloc, absolute);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Conv_U8); // IL is dumb...
				il.Emit(OpCodes.Conv_R8);
				il.Emit(OpCodes.Ldc_R8, getFactor(i - context.Format.RadixPoint));
				il.Emit(OpCodes.Mul);

				if (i < context.Format.Digits - 1) 
					il.Emit(OpCodes.Add);
			}

			Label returnLabel = il.DefineLabel();

			// negate if needed.
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, 0x80000000);
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Brfalse, returnLabel);
			il.Emit(OpCodes.Neg);
			il.MarkLabel(returnLabel);
			il.Emit(OpCodes.Ret);
		}


		private static void generateSetDigits(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "SetDigits", null, typeof(uint[]));
			ILGenerator il = method.GetILGenerator();

			LocalBuilder length = il.DeclareLocal(typeof(int));

			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldlen);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Sub);
			il.Emit(OpCodes.Stloc, length);

			for (int i = 0; i < context.Format.Digits; i++)
			{
				Label load0Label = il.DefineLabel();
				Label setLabel = il.DefineLabel();

				il.Emit(OpCodes.Ldarg_0);

				// if (length < 0)
				il.Emit(OpCodes.Ldloc, length);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Clt);
				il.Emit(OpCodes.Brtrue, load0Label);

				// false: load array value.
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldloc, length);
				il.Emit(OpCodes.Ldelem_U4);

				// length--
				il.Emit(OpCodes.Ldloc, length);
				il.Emit(OpCodes.Ldc_I4_1);
				il.Emit(OpCodes.Sub);
				il.Emit(OpCodes.Stloc, length);
				il.Emit(OpCodes.Br, setLabel);

				// true: load 0.
				il.MarkLabel(load0Label);
				il.Emit(OpCodes.Ldc_I4_0);

				// set field.
				il.MarkLabel(setLabel);
				il.Emit(OpCodes.Stfld, context.Fields[context.Format.Digits - 1 - i]);
			}

			il.Emit(OpCodes.Ret);
		}

		private static void generateCopyDigits(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "CopyDigits", null, typeof(uint[]), typeof(int));
			ILGenerator il = method.GetILGenerator();

			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldarg_2);
				il.Emit(OpCodes.Ldc_I4, i);
				il.Emit(OpCodes.Add);
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Stelem_I4);
			}

			il.Emit(OpCodes.Ret);
		}
				
		private static void generateAssignNegate(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "AssignNegate", null, typeof(Fixed));
			ILGenerator il = method.GetILGenerator();

#if DEBUG
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_1));
#endif

			generateNegateBody(context, il, () => il.Emit(OpCodes.Ldarg_1), () => il.Emit(OpCodes.Ldarg_0));
			il.Emit(OpCodes.Ret);
		}

		private static void generateNegateBody(Context context, ILGenerator il, Action loadA, Action loadResult)
		{
			LocalBuilder sum = il.DeclareLocal(typeof(ulong));
			
			// negate
			for (int i = 0; i < context.Format.Digits; i++)
			{
				loadResult();
				loadA();
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Not);
				il.Emit(OpCodes.Stfld, context.Fields[i]);
			}

			for (int i = 0; i < context.Format.Digits; i++)
			{
				loadResult();
				il.Emit(OpCodes.Ldfld, context.Fields[i]); // load a[i]
				il.Emit(OpCodes.Conv_U8);

				if (i > 0)
					il.Emit(OpCodes.Ldloc, sum); // load sum.
				else
					il.Emit(OpCodes.Ldc_I8, 1L);

				il.Emit(OpCodes.Add); // a[i] + 1 or sum
				il.Emit(OpCodes.Stloc, sum); // store sum.

				loadResult();
				il.Emit(OpCodes.Ldloc, sum);
				il.Emit(OpCodes.Conv_U4);
				il.Emit(OpCodes.Stfld, context.Fields[i]); // store sum in result[i];

				if (i < context.Format.Digits - 1)
				{
					// sum >>= 32
					il.Emit(OpCodes.Ldloc, sum);
					il.Emit(OpCodes.Ldc_I4, 32);
					il.Emit(OpCodes.Shr_Un);
					il.Emit(OpCodes.Stloc, sum);
				}
			}
		}

		private static void generateAssignAdd(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "AssignAdd", null, typeof(Fixed), typeof(Fixed));
			ILGenerator il = method.GetILGenerator();

#if DEBUG
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_1));
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_2));
#endif
			
			LocalBuilder carry = il.DeclareLocal(typeof(ulong));

			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldfld, context.Fields[i]); // load a[i]
				il.Emit(OpCodes.Conv_U8);

				il.Emit(OpCodes.Ldarg_2);
				il.Emit(OpCodes.Ldfld, context.Fields[i]); // load b[i]
				il.Emit(OpCodes.Conv_U8);

				il.Emit(OpCodes.Add); // a[i] + b[i]

				if (i > 0)
				{
					il.Emit(OpCodes.Ldloc, carry); // load sum.
					il.Emit(OpCodes.Add); // a[i] + b[i] + sum
				}

				il.Emit(OpCodes.Stloc, carry); // store sum.

				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldloc, carry);
				il.Emit(OpCodes.Conv_U4);
				il.Emit(OpCodes.Stfld, context.Fields[i]); // store sum in result[i];

				if (i < context.Format.Digits - 1)
				{
					// sum >>= 32
					il.Emit(OpCodes.Ldloc, carry);
					il.Emit(OpCodes.Ldc_I4, 32);
					il.Emit(OpCodes.Shr_Un);
					il.Emit(OpCodes.Stloc, carry);
				}
			}

			il.Emit(OpCodes.Ret);
		}

		private static void generateAssignSubtract(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "AssignSubtract", null, typeof(Fixed), typeof(Fixed));
			ILGenerator il = method.GetILGenerator();
			LocalBuilder carry = il.DeclareLocal(typeof(ulong));


#if DEBUG
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_1));
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_2));
#endif
			
			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldfld, context.Fields[i]); // load a[i]
				il.Emit(OpCodes.Conv_U8);

				il.Emit(OpCodes.Ldarg_2);
				il.Emit(OpCodes.Ldfld, context.Fields[i]); // load b[i]
				il.Emit(OpCodes.Conv_U8);

				il.Emit(OpCodes.Sub); // a[i] - b[i]

				if (i > 0)
				{
					il.Emit(OpCodes.Ldloc, carry); // load sum.
					il.Emit(OpCodes.Sub); // a[i] - b[i] - sum
				}

				il.Emit(OpCodes.Stloc, carry); // store sum.

				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldloc, carry);
				il.Emit(OpCodes.Conv_U4);
				il.Emit(OpCodes.Stfld, context.Fields[i]); // store sum in result[i];

				if (i < context.Format.Digits - 1)
				{
					// sum = (sum >> 32) & 1
					il.Emit(OpCodes.Ldloc, carry);
					il.Emit(OpCodes.Ldc_I4, 32);
					il.Emit(OpCodes.Shr_Un);
					il.Emit(OpCodes.Ldc_I8, 1L);
					il.Emit(OpCodes.And);
					il.Emit(OpCodes.Stloc, carry);
				}
			}

			il.Emit(OpCodes.Ret);
		}

		private static void generateAssignMultiply(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "AssignMultiply", null, typeof(Fixed), typeof(Fixed));
			ILGenerator il = method.GetILGenerator();
			
#if DEBUG
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_1));
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_2));
#endif
			
			LocalBuilder signA = il.DeclareLocal(typeof(uint));
			LocalBuilder signB = il.DeclareLocal(typeof(uint));
			LocalBuilder carry = il.DeclareLocal(typeof(ulong));
			LocalBuilder[] temps = new LocalBuilder[context.Format.Digits + context.Format.RadixPoint];

			// load sign extension value for A.
			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, unchecked((int)0x80000000u));
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Ldc_I4_0);
			il.Emit(OpCodes.Ceq);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Sub);
			il.Emit(OpCodes.Stloc, signA);

			// load sign extension value for B.
			il.Emit(OpCodes.Ldarg_2);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, unchecked((int)0x80000000u));
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Ldc_I4_0);
			il.Emit(OpCodes.Ceq);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Sub);
			il.Emit(OpCodes.Stloc, signB);

			for (int i = 0; i < context.Format.Digits + context.Format.RadixPoint; i++)
			{
				for (int j = 0; i + j < context.Format.Digits + context.Format.RadixPoint; j++)
				{
					if (j > 0)
					{
						// carry <<= 32.
						il.Emit(OpCodes.Ldloc, carry);
						il.Emit(OpCodes.Ldc_I4, 32);
						il.Emit(OpCodes.Shr_Un);
						il.Emit(OpCodes.Stloc, carry);
					}

					// load a[i].
					if (i < context.Format.Digits)
					{
						il.Emit(OpCodes.Ldarg_1);
						il.Emit(OpCodes.Ldfld, context.Fields[i]);
					}
					else
					{
						il.Emit(OpCodes.Ldloc, signA);
					}
					il.Emit(OpCodes.Conv_U8);

					// load b[j].
					if (j < context.Format.Digits)
					{
						il.Emit(OpCodes.Ldarg_2);
						il.Emit(OpCodes.Ldfld, context.Fields[j]);
					}
					else
					{
						il.Emit(OpCodes.Ldloc, signB);
					}
					il.Emit(OpCodes.Conv_U8);

					// a[i] * b[i]
					il.Emit(OpCodes.Mul);

					// add carry.
					if (j > 0)
					{
						// a[i] * b[i] + carry
						il.Emit(OpCodes.Ldloc, carry);
						il.Emit(OpCodes.Add);
					}

					// add current value.
					if (temps[i + j] != null)
					{
						// a[i] * b[i] + carry + r[i+j]
						il.Emit(OpCodes.Ldloc, temps[i + j]);
						il.Emit(OpCodes.Conv_U8);
						il.Emit(OpCodes.Add);
					}
					else
					{
						temps[i + j] = il.DeclareLocal(typeof(uint));
					}
					il.Emit(OpCodes.Stloc, carry);

					// store in r[i+j].
					il.Emit(OpCodes.Ldloc, carry);
					il.Emit(OpCodes.Conv_U4);
					il.Emit(OpCodes.Stloc, temps[i + j]);
				}
			}

			// copy result.
			for (int i = 0; i < context.Format.Digits; i++)
			{
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldloc, temps[i + context.Format.RadixPoint]);
				il.Emit(OpCodes.Stfld, context.Fields[i]);
			}

			il.Emit(OpCodes.Ret);
		}
		
		private static void generateCompareTo(Context context)
		{
			MethodBuilder method = overrideMethod(context.FixedType, "CompareTo", typeof(int), typeof(Fixed));
			ILGenerator il = method.GetILGenerator();

#if DEBUG
			generateTypeCheck(context, il, () => il.Emit(OpCodes.Ldarg_1));
#endif

			Label return1Label = il.DefineLabel();
			Label returnM1Label = il.DefineLabel();

			LocalBuilder neg1 = il.DeclareLocal(typeof(uint));
			LocalBuilder neg2 = il.DeclareLocal(typeof(uint));

			// neg1 = sign of a
			il.Emit(OpCodes.Ldarg_0);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, unchecked((int)0x80000000u));
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Stloc, neg1);

			// neg1 = sign of b
			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldfld, context.Fields[context.Format.Digits - 1]);
			il.Emit(OpCodes.Ldc_I4, unchecked((int)0x80000000u));
			il.Emit(OpCodes.And);
			il.Emit(OpCodes.Stloc, neg2);

			// if(!neg1 && neg2) return true;
			{
				// neg1 == false?
				il.Emit(OpCodes.Ldloc, neg1);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);

				// neg2 == true?
				il.Emit(OpCodes.Ldloc, neg2);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);

				// neg1 == false && neg2 == true?
				il.Emit(OpCodes.And);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Brtrue, return1Label);
			}

			// if(neg1 && !neg2) return false;
			{
				// neg1 == true?
				il.Emit(OpCodes.Ldloc, neg1);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);

				// neg2 == false?
				il.Emit(OpCodes.Ldloc, neg2);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);

				// neg1 == true && neg2 == false?
				il.Emit(OpCodes.And);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Ldc_I4_0);
				il.Emit(OpCodes.Ceq);
				il.Emit(OpCodes.Brtrue, returnM1Label);
			}

			for (int i = context.Format.Digits - 1; i >= 0; i--)
			{
				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Cgt_Un);
				il.Emit(OpCodes.Brtrue, return1Label);

				il.Emit(OpCodes.Ldarg_0);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Ldarg_1);
				il.Emit(OpCodes.Ldfld, context.Fields[i]);
				il.Emit(OpCodes.Clt_Un);
				il.Emit(OpCodes.Brtrue, returnM1Label);
			}

			il.Emit(OpCodes.Ldc_I4_0);
			il.Emit(OpCodes.Ret);

			il.MarkLabel(return1Label);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Ret);

			il.MarkLabel(returnM1Label);
			il.Emit(OpCodes.Ldc_I4_M1);
			il.Emit(OpCodes.Ret);
		}
			
		#endregion
	}
}
