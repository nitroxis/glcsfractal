// add with carry.
uint addc(uint a, uint b, out uint carry)
{
	a += b;
	carry = uint(a < b);
	return a;
}
uvec4 addc(uvec4 a, uvec4 b, out uvec4 carry)
{
	a += b;
	carry = uvec4(lessThan(a, b));
	return a;
}

// sub with borrow.
uint subb(uint a, uint b, out uint borrow)
{
	b = a - b;
	borrow = uint(b > a);
	return b;
}
uvec4 subb(uvec4 a, uvec4 b, out uvec4 borrow)
{
	b = a - b;
	borrow = uvec4(greaterThan(b, a));
	return b;
}

#define uaddCarry(a, b, c) addc(a, b, c)
#define usubBorrow(a, b, c) subb(a, b, c)
