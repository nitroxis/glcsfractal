#include <BuiltInFix>

#define DECIMALPOINT NUMDIGITS - 1
#define real uint[((NUMDIGITS + 3) / 4) * 4]

// complex fixed-point number.
struct complex
{
	real r;
	real i;
};

bool bailout(real x)
{
	return x[DECIMALPOINT] >= 4;
}

// adds two fixed-point numbers.
void add(real a, real b, out real result)
{
	uint carry = 0;
	uint sumCarry;

	// addition with carry.
	for (int i = 0; i < NUMDIGITS; i++)
	{
		uint sum = uaddCarry(a[i], b[i], sumCarry);
		result[i] = uaddCarry(sum, carry, carry);
		carry += sumCarry;
	}
}

// subtracts two fixed-point numbers.
void sub(real a, real b, out real result)
{
	uint borrow = 0;
	uint diffBorrow;

	// substraction with borrow.
	for (int i = 0; i < NUMDIGITS; i++)
	{
		uint diff = usubBorrow(a[i], b[i], diffBorrow);
		result[i] = usubBorrow(diff, borrow, borrow);
		borrow += diffBorrow;
	}
}

// multiplies a fixed-point number with an unsigned integer (i.e. single limb).
void mul(real a, uint b, out real result)
{
	// multiplicate every digit with b.
	for (int i = 0; i < NUMDIGITS; i++)
	{
		// multiply a[i] and b.
		uint lsb, msb;
		umulExtended(a[i], b, msb, lsb);

		// add current value.
		result[i] = uaddCarry(result[i], lsb, lsb);

		if(i < NUMDIGITS - 1)
			result[i + 1] = msb + lsb;
	}
}

// squares a fixed-point number.
void sqr(real a, out real result)
{
	uint[2 * (NUMDIGITS + DECIMALPOINT)] temp;
	for (int i = 0; i < NUMDIGITS + DECIMALPOINT; i++)
		temp[i] = 0;

	// sign extension placeholder.
	uint e = uint((a[NUMDIGITS - 1] & 0x80000000) == 0) + uint(-1);

	// multiply every a[i] with every a[j].
	for (int i = 0; i < NUMDIGITS + DECIMALPOINT; i++)
	{
		uint carry = 0;

		// get a[i].
		uint ai = i < NUMDIGITS ? a[i] : e;

		for (int j = 0; j < NUMDIGITS + DECIMALPOINT; j++)
		{
			// get a[j].
			uint aj = j < NUMDIGITS ? a[j] : e;

			// multiply a[i] and a[j].
			uint lsb, msb;
			umulExtended(ai, aj, msb, lsb);

			// add current value.
			temp[i + j] = uaddCarry(temp[i + j], lsb, lsb);
			temp[i + j + 1] = uaddCarry(temp[i + j + 1], msb + lsb + carry, carry);
		}
	}

	// theoretically faster, more ALU dependencies.
	/*for (int i = 0; i < NUMDIGITS + DECIMALPOINT; i++)
	{
		uint carry = 0;
		uint lsb, msb;

		// get a[i].
		uint ai = i < NUMDIGITS ? a[i] : e;

		for (int j = 0; j < i; j++)
		{
			// get a[j].
			uint aj = j < NUMDIGITS ? a[j] : e;

			// multiply a[i] and a[j].
			umulExtended(ai, aj, msb, lsb);

			uint ovf = (msb >> 31);
			msb = (lsb >> 31) | (msb << 1);
			lsb = (lsb << 1);

			// add current value.
			temp[i + j] = uaddCarry(temp[i + j], lsb, lsb);
	
			// add carry.
			temp[i + j + 1] = uaddCarry(temp[i + j + 1], uaddCarry(msb, carry + lsb, lsb), carry);
			carry += ovf + lsb;
		}

		// multiply a[i] with itself.
		umulExtended(ai, ai, msb, lsb);

		// add current value.
		temp[i + i] = uaddCarry(temp[i + i], lsb, lsb);

		// add carry.
		temp[i + i + 1] += msb + lsb + carry;
	}*/
	
	// store shifted result.
	for (int i = 0; i < NUMDIGITS; i++)
		result[i] = temp[i + DECIMALPOINT];
}
