﻿namespace Mu
{
	internal struct Vector2us
	{
		public ushort X;
		public ushort Y;

		public Vector2us(ushort x, ushort y)
		{
			this.X = x;
			this.Y = y;
		}

		public override string ToString()
		{
			return string.Format("{0}, {1}", this.X, this.Y);
		}
	}
}
