﻿namespace Mu
{
	/// <summary>
	/// Fixed-point number implementation based on an array.
	/// </summary>
	public sealed class FixedFallback : Fixed
	{
		#region Fields

		private readonly uint[] d;

		#endregion

		#region Constructors

		public FixedFallback(FixedFormat format)
			: base(format)
		{
			this.d = new uint[format.Digits];
		}

		#endregion

		#region Methods

		protected override uint GetDigit(int index)
		{
			return this.d[index];
		}
		
		protected override void SetDigit(int index, uint value)
		{
			this.d[index] = value;
		}
		
		#endregion
	}
}
