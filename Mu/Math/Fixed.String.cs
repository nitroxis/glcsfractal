﻿using System.Text;

namespace Mu
{
	public abstract partial class Fixed
	{	
		#region Methods

		/// <summary>
		/// Converts the fixed-point number to a string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.ToString(this.Format.Digits * 16);
		}

		/// <summary>
		/// Converts the fixed-point number to a string.
		/// </summary>
		/// <returns></returns>
		public string ToString(int maxFractDigits)
		{
			Fixed absolute;

			if (this.IsNegative)
			{
				absolute = Fixed.Create(this.Format);
				absolute.AssignNegate(this);
			}
			else
			{
				absolute = this;
			}

			uint[] digits = absolute.GetDigits();

			byte[] intBytes = toByteArray(digits, this.Format.RadixPoint, this.Format.Digits - this.Format.RadixPoint);
			byte[] fractBytes = toByteArrayReverse(digits, 0, this.Format.RadixPoint);

			byte[] int10 = convertBase(256, 10, intBytes, 0, intBytes.Length, intBytes.Length * 4);
			byte[] fract10 = convertBaseFract(256, 10, fractBytes, 0, fractBytes.Length, maxFractDigits);

			string intStr = concatDigitsReverse(int10);
			string fractStr = concatDigits(fract10);

			if (string.IsNullOrWhiteSpace(intStr))
				intStr = "0";

			if (string.IsNullOrWhiteSpace(fractStr))
				return (this.IsNegative ? "-" : "") + intStr;

			return (this.IsNegative ? "-" : "") + intStr + "." + fractStr;
		}

		/// <summary>
		/// Converts the fixed-point number to a hexadecimal string representation.
		/// </summary>
		/// <returns></returns>
		public string ToStringHex()
		{
			StringBuilder str = new StringBuilder();

			for (int i = this.Format.Digits - 1; i >= 0; i--)
			{
				if (i == this.Format.RadixPoint)
					str.Append('.');
				str.Append(this.GetDigit(i).ToString("X8"));
			}

			return str.ToString();
		}

		private static byte[] toByteArray(uint[] buffer, int offset, int count)
		{
			byte[] bytes = new byte[count * 4];

			for (int i = 0; i < count; i++)
			{
				bytes[i * 4 + 0] = (byte)(buffer[offset + i] >> 0);
				bytes[i * 4 + 1] = (byte)(buffer[offset + i] >> 8);
				bytes[i * 4 + 2] = (byte)(buffer[offset + i] >> 16);
				bytes[i * 4 + 3] = (byte)(buffer[offset + i] >> 24);
			}

			return bytes;
		}

		private static byte[] toByteArrayReverse(uint[] buffer, int offset, int count)
		{
			byte[] bytes = new byte[count * 4];

			for (int i = 0, j = count - 1; i < count; i++, j--)
			{
				bytes[i * 4 + 0] = (byte)(buffer[offset + j] >> 24);
				bytes[i * 4 + 1] = (byte)(buffer[offset + j] >> 16);
				bytes[i * 4 + 2] = (byte)(buffer[offset + j] >> 8);
				bytes[i * 4 + 3] = (byte)(buffer[offset + j] >> 0);
			}

			return bytes;
		}

		private static string concatDigits(byte[] buffer)
		{
			StringBuilder output = new StringBuilder();
			bool zero = true;

			for (int i = buffer.Length - 1; i >= 0; i--)
			{
				if (buffer[i] != 0)
					zero = false;

				if (zero)
					continue;

				if (buffer[i] < 10)
					output.Insert(0, (char)(buffer[i] + '0'));
				else
					output.Insert(0, (char)(buffer[i] + 'A' - 10));
			}

			return output.ToString();
		}

		private static string concatDigitsReverse(byte[] buffer)
		{
			StringBuilder output = new StringBuilder();
			bool zero = true;

			for (int i = buffer.Length - 1; i >= 0; i--)
			{
				if (buffer[i] != 0)
					zero = false;

				if (zero)
					continue;

				if (buffer[i] < 10)
					output.Append((char)(buffer[i] + '0'));
				else
					output.Append((char)(buffer[i] + 'A' - 10));
			}

			return output.ToString();
		}

		private static byte[] convertBase(int fromBase, int toBase, byte[] input, int inputOffset, int inputLength, int numDigits)
		{
			int[] output = new int[numDigits + 10];
			int[] t = new int[numDigits + 10];
			t[0] = 1;

			// convert base.
			for (int i = 0; i < inputLength; i++)
			{
				for (int j = 0; j < numDigits; j++)
				{
					output[j] += t[j] * input[inputOffset + i];
					int temp = output[j];
					int k = j;

					do
					{
						int rem = temp / toBase;
						output[k++] = temp - rem * toBase;
						output[k] += rem;
						temp = output[k];
					} while (temp >= toBase);
				}

				for (int j = 0; j < numDigits; j++)
				{
					t[j] = t[j] * fromBase;
				}

				for (int j = 0; j < numDigits; j++)
				{
					int temp = t[j];
					int k = j;

					do
					{
						int rem = temp / toBase;
						t[k++] = temp - rem * toBase;
						t[k] += rem;
						temp = t[k];
					} while (temp >= toBase);
				}
			}

			// convert to byte buffer.
			byte[] outputBuffer = new byte[numDigits];
			for (int i = 0; i < numDigits; i++)
				outputBuffer[i] = (byte)output[i];

			return outputBuffer;
		}

		private static byte[] convertBaseFract(int baseFrom, int baseTo, byte[] input, int inputOffset, int inputLength, int numDigits)
		{
			int[] output = new int[numDigits];
			int[] copy = new int[inputLength];

			for (int i = 0; i < inputLength; i++)
				copy[i] = input[inputOffset + i];

			for (int i = 0; i < numDigits; i++)
			{
				int carry = 0;

				for (int idx = inputLength - 1; idx >= 0; idx--)
				{
					int x = copy[idx] * baseTo + carry;
					int next = x % baseFrom;
					carry = x / baseFrom;

					copy[idx] = next;
				}

				output[i] = carry;
			}

			// convert to byte buffer.
			byte[] outputBuffer = new byte[output.Length];
			for (int i = 0; i < output.Length; i++)
				outputBuffer[i] = (byte)output[i];

			return outputBuffer;
		}


		#endregion
	}
}
