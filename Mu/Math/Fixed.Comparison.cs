﻿namespace Mu
{
	public abstract partial class Fixed
	{
		#region Comparison methods

		/// <summary>
		/// Compares two fixed point values.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public virtual int CompareTo(Fixed value)
		{
			bool an = this.IsNegative;
			bool bn = value.IsNegative;

			if (an && !bn)
				return -1;

			if (!an && bn)
				return 1;

			for (int i = this.Format.Digits - 1; i >= 0; i--)
			{
				uint a = this.GetDigit(i);
				uint b = value.GetDigit(i);

				if (a < b)
					return -1;

				if (a > b)
					return 1;
			}

			return 0;
		}

		/// <summary>
		/// Returns a value that indicates whether the specified value equals this instance.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool Equals(Fixed value)
		{
			return this.CompareTo(value) == 0;
		}

		/// <summary>
		/// Returns a value that indicates whether the specified object equals this instance.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			Fixed value = obj as Fixed;
			if (value == null)
				return false;

			return this.Equals(value);
		}

		/// <summary>
		/// Returns a value that indicates whether a is greater than b.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool GreaterThan(Fixed value)
		{
			return this.CompareTo(value) > 0;
		}

		/// <summary>
		/// Returns a value that indicates whether a is less than b.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool LessThan(Fixed value)
		{
			return this.CompareTo(value) < 0;
		}

		/// <summary>
		/// Returns a value that indicates whether a is greater than or equal to b.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool GreaterThanOrEqual(Fixed value)
		{
			return this.CompareTo(value) >= 0;
		}

		/// <summary>
		/// Returns a value that indicates whether a is less than or equal to b.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool LessThanOrEqual(Fixed value)
		{
			return this.CompareTo(value) <= 0;
		}

		#endregion

		#region Operators

		public static bool operator ==(Fixed a, Fixed b)
		{
			if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return true;
			if (ReferenceEquals(a, null)) return false;
			if (ReferenceEquals(b, null)) return false;
			return a.Equals(b);
		}

		public static bool operator !=(Fixed a, Fixed b)
		{
			if (ReferenceEquals(a, null) && ReferenceEquals(b, null)) return false;
			if (ReferenceEquals(a, null)) return true;
			if (ReferenceEquals(b, null)) return true;
			return !a.Equals(b);
		}

		public static bool operator >(Fixed a, Fixed b)
		{
			return a.GreaterThan(b);
		}

		public static bool operator <(Fixed a, Fixed b)
		{
			return a.LessThan(b);
		}

		public static bool operator >=(Fixed a, Fixed b)
		{
			return a.GreaterThanOrEqual(b);
		}

		public static bool operator <=(Fixed a, Fixed b)
		{
			return a.LessThanOrEqual(b);
		}

		#endregion
	}
}
