﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Mu
{
	/// <summary>
	/// GLSL code generator for FP math.
	/// </summary>
	internal sealed class GlslGenerator
	{
		private readonly FixedFormat format;
		private static readonly string[] vector = { "x", "y", "z", "w" };

		public GlslGenerator(FixedFormat format)
		{
			this.format = format;
		}

		public void Generate(TextWriter writer)
		{
			this.GenerateHeader(writer);
			this.GenerateConverterMethod(writer);
			this.GenerateAddMethod(writer, false);
			this.GenerateAddMethod(writer, true);
			this.GenerateMultiplyMethod(writer);
			this.GenerateSquareMethod(writer);
		}

		public void GenerateHeader(TextWriter writer)
		{
			writer.WriteLine("#define real uvec4[{0}]", (this.format.Digits + 3) / 4);
			writer.WriteLine("struct complex { real r; real i; };");
		}

		public void GenerateConverterMethod(TextWriter writer)
		{
			writer.WriteLine("bool bailout(real r)");
			writer.WriteLine("{");
			writer.WriteLine("return r[{0}].{1} >= 4;", this.format.RadixPoint / 4, vector[this.format.RadixPoint & 3]);
			writer.WriteLine("}");
		}

		public void GenerateAddMethod(TextWriter writer, bool subtract)
		{
			// header.
			writer.WriteLine("void {0}(real a, real b, out real r)", subtract ? "sub" : "add");
			writer.WriteLine("{");
			writer.WriteLine("uint s, c;");
			writer.WriteLine("uvec4 t, c1, c2 = uvec4(0);");

			// two's complement:
			/*if (subtract)
			{
				for (int j = 0; j < this.numDigits; j += 4)
				{
					writer.WriteLine("b[{0}] = ~b[{0}];", j / 4);
				}

				for (int j = 0; j < this.numDigits; j += 4)
				{
					writer.WriteLine("t = uvec4(equal(b[{0}], uvec4(-1)));", j / 4);

					if (j == 0)
					{
						writer.WriteLine("c1 = uvec4(1, t.x, t.x & t.y, t.x & t.y & t.z);");
						writer.WriteLine("b[{0}] += c1;", j / 4);
					}
					else
					{
						writer.WriteLine("c1 = uvec4(1, t.x, t.x & t.y, t.x & t.y & t.z) & c2.x;");
						writer.WriteLine("b[{0}] += c1;", j / 4);
					}
					writer.WriteLine("c2.x = c1.w & t.w;");
				}

				subtract = false;
			}*/

			string op = subtract ? "-" : "+";

			int i = 0;
			for (; i + 3 < this.format.Digits; i += 4)
			{
				writer.WriteLine();
				
				bool first = i == 0;
				bool last = i + 4 >= this.format.Digits;

				int j = i / 4;
				writer.WriteLine("r[{0}] = a[{0}] {1} b[{0}];", j, op);
				writer.WriteLine("t = uvec4({1}(r[{0}], a[{0}]));", j, subtract ? "greaterThan" : "lessThan");
				
				if(first)
					writer.WriteLine("c1 = uvec4(0, t.xyz);");
				else
					writer.WriteLine("c1 = uvec4(c, t.xyz);");

				if(!last)
					writer.WriteLine("c = t.w;");

				writer.WriteLine("t = uvec4(equal(r[{0}], uvec4({1})));", j, subtract ? "0" : "-1");

				if (first)
				{
					// there's no input carry so c2.x is 0
					// c2.y is always 0 so we can remove the or.
					writer.WriteLine("c2.z = t.y & c1.y;");
				}
				else
				{
					writer.WriteLine("c2.y = t.x & (c1.x | c2.x);");
					writer.WriteLine("c2.z = t.y & (c1.y | c2.y);");
				}
				writer.WriteLine("c2.w = t.z & (c1.z | c2.z);");
				
				writer.WriteLine("r[{0}] {1}= (c1 | c2);", j, op);

				if (!last)
					writer.WriteLine("c2.x = t.w & (c1.w | c2.w);");
			}

			if (i > 0 && i != this.format.Digits)
				writer.WriteLine("c += c2.x;");

			for (; i < this.format.Digits; i++)
			{
				writer.WriteLine();
				
				string x = string.Format("[{0}].{1}", i / 4, vector[i & 3]);

				writer.WriteLine("s = a{0} {1} b{0};", x, op);
				
				if(i > 0)
					writer.WriteLine("r{0} = s {1} c;", x, op);
				else
					writer.WriteLine("r{0} = s;", x);

				if (i < this.format.Digits - 1)
					writer.WriteLine("c = uint(s {1} a{0}) + uint(r{0} {1} s);", x, subtract ? ">" : "<");
			}

			// footer.
			writer.WriteLine("}");
		}

		public void GenerateMultiplyMethod(TextWriter writer)
		{
			// header.
			writer.WriteLine("void mul(real a, uint b, out real r)");
			writer.WriteLine("{");
			writer.WriteLine("uvec4 hi;");
			writer.WriteLine("uint c;");

			for (int i = 0; i < this.format.Digits; i++)
			{
				string x = string.Format("[{0}].{1}", i / 4, vector[i & 3]);
				string x1 = string.Format("[{0}].{1}", (i - 1) / 4, vector[(i - 1) & 3]);

				if (i > 0)
				{
					if (i > 1)
						writer.WriteLine("c = uint(r{0} < c) + hi.{1};", x1, vector[(i - 1) & 3]);
					else
						writer.WriteLine("c = hi.{0};", vector[(i - 1) & 3]);
				}

				if ((i & 3) == 0)
				{
					writer.WriteLine();
					writer.WriteLine("umulExtended(a[{0}], uvec4(b), hi, r[{0}]);", i / 4);
				}

				if (i > 0)
					writer.WriteLine("r{0} += c;", x);
			}

			// footer.
			writer.WriteLine("}");
		}

		private delegate void tempVarFunc(int index, out string name, out bool firstUse);

		public void GenerateSquareMethod(TextWriter writer)
		{
			// header.
			writer.WriteLine("void sqr(real a, out real r)");
			writer.WriteLine("{");
			writer.WriteLine("uvec4 m, l;");
			writer.WriteLine("uint c;");
			writer.WriteLine("uint e = uint(int(a[{0}].{1}) >= 0) + uint(-1);", (this.format.Digits - 1) / 4, vector[(this.format.Digits - 1) & 3]);
			
			List<int> vars = new List<int>();
			tempVarFunc getVar = (int i, out string name, out bool firstUse) =>
			{
				i -= this.format.RadixPoint;

				int varIndex = vars.IndexOf(i);
				if (varIndex < 0)
				{
					firstUse = true;
					varIndex = vars.Count;
					vars.Add(i);
				}
				else
				{
					firstUse = false;	
				}

				if (i >= 0 && i < this.format.Digits)
				{
					name = string.Format("r[{0}].{1}", i / 4, vector[i & 3]);
				}
				else
				{
					if (firstUse)
						writer.WriteLine("uint t{0};", varIndex);
					name = string.Format("t{0}", varIndex);
				}
			};

			for (int i = 0; i < this.format.Digits + this.format.RadixPoint; i++)
			{
				string ai;
				if (i < this.format.Digits)
					ai = string.Format("a[{0}].{1}", i / 4, vector[i & 3]);
				else
					ai = "e";
				
				bool haveCarry = false;

				for (int j = 0; i + j < this.format.Digits + this.format.RadixPoint; j++)
				{
					string aj;
					if (j < this.format.Digits)
						aj = string.Format("a[{0}].{1}", j / 4, vector[j & 3]);
					else
						aj = "e";

					writer.WriteLine("umulExtended({0}, {1}, m.x, l.x);", ai, aj);

					string varName;
					bool init;
					getVar(i + j, out varName, out init);

					if (init)
						writer.WriteLine("{0} = l.x;", varName);
					else
						writer.WriteLine("{0} += l.x;", varName);

					if (i + j + 1 < this.format.Digits + this.format.RadixPoint)
					{
						if (haveCarry)
						{
							if (init)
								writer.WriteLine("c += m.x;", varName);
							else
								writer.WriteLine("c += m.x + uint({0} < l.x);", varName);
						}
						else
						{
							if (init)
								writer.WriteLine("c = m.x;", varName);
							else
								writer.WriteLine("c = m.x + uint({0} < l.x);", varName);
						}

						getVar(i + j + 1, out varName, out init);
						if (init)
							writer.WriteLine("{0} = c;", varName);
						else
							writer.WriteLine("{0} += c;", varName);
						writer.WriteLine("c = uint({0} < c);", varName);
						haveCarry = true;
					}
				}
				writer.WriteLine();
			}

			// footer.
			writer.WriteLine("}");
		}
	}
}

