#define real float

struct complex
{
	real r;
	real i;
};

bool bailout(real x)
{
	return x >= 4.0;
}

// adds two fixed-point numbers.
void add(real a, real b, out real result)
{
	result = a + b;
}

// subtracts two fixed-point numbers.
void sub(real a, real b, out real result)
{
	result = a - b;
}

// multiplies a fixed-point number with an unsigned integer (i.e. single limb).
void mul(real a, uint b, out real result)
{
	result = a * float(b);
}

// squares a fixed-point number.
void sqr(real a, out real result)
{
	result = a * a;
}
