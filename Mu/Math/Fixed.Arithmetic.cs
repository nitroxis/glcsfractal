﻿using System;

namespace Mu
{
	public abstract partial class Fixed
	{
		#region Assign methods

		/// <summary>
		/// Negates a fixed-point number and assigns the result to this instance.
		/// </summary>
		/// <param name="value"></param>
		public virtual void AssignNegate(Fixed value)
		{
			bool carry = true;

			for (int i = 0; i < this.Format.Digits; i++)
			{
				uint d = ~value.GetDigit(i);

				if (carry)
				{
					if (d == uint.MaxValue)
					{
						d = 0;
					}
					else
					{
						d += 1;
						carry = false;
					}
				}

				this.SetDigit(i, d);
			}
		}

		/// <summary>
		/// Adds two fixed-point numbers and assigns the result to this instance.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public virtual void AssignAdd(Fixed a, Fixed b)
		{
			ulong sum = 0;
			for (int i = 0; i < this.Format.Digits; i++)
			{
				sum = (ulong)a.GetDigit(i) + (ulong)b.GetDigit(i) + (sum >> 32);
				this.SetDigit(i, (uint)sum);
			}
		}

		/// <summary>
		/// Subtracts two fixed-point numbers and assigns the result to this instance.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public virtual void AssignSubtract(Fixed a, Fixed b)
		{
			ulong diff = 0;
			for (int i = 0; i < this.Format.Digits; i++)
			{
				diff = (ulong)a.GetDigit(i) - (ulong)b.GetDigit(i) - ((diff >> 32) & 1);
				this.SetDigit(i, (uint)diff);
			}
		}

		/// <summary>
		/// Multiplies two fixed-point numbers and assigns the result to this instance.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public virtual void AssignMultiply(Fixed a, Fixed b)
		{
			int n = this.Format.Digits;
			int max = this.Format.Digits + this.Format.RadixPoint;

			uint sa = a.IsNegative ? uint.MaxValue : 0;
			uint sb = b.IsNegative ? uint.MaxValue : 0;

			uint[] temp = new uint[max];
			for (int i = 0; i < max; i++)
			{
				ulong product = 0;
				ulong da = i < n ? a.GetDigit(i) : sa;

				for (int j = 0; i + j < max; j++)
				{
					ulong db = j < n ? b.GetDigit(j) : sb;
					product = temp[i + j] + da * db + (product >> 32);
					temp[i + j] = (uint)product;
				}
			}

			for (int i = 0; i < this.Format.Digits; i++)
				this.SetDigit(i, temp[i + this.Format.RadixPoint]);
		}

		/// <summary>
		/// Squares the specified value and assigns the result to this instance.
		/// </summary>
		/// <param name="value"></param>
		public virtual void AssignSquare(Fixed value)
		{
			this.AssignMultiply(value, value);
		}

		/// <summary>
		/// Divides a by b and stores the result in this instance.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public virtual void AssignDivide(Fixed a, Fixed b)
		{
			// TODO: make an IL only version of this.
			Fixed numerator, denominator;
			bool negative = false;

			if (a.IsNegative)
			{
				negative = true;
				numerator = Fixed.Create(this.Format);
				Fixed.Negate(a, numerator);
			}
			else
			{
				numerator = a;
			}

			if (b.IsNegative)
			{
				negative ^= true;
				denominator = Fixed.Create(this.Format);
				Fixed.Negate(b, denominator);
			}
			else
			{
				denominator = b;
			}

			uint[] v = denominator.GetDigits();
			int vLen = getSignificantDigits(v);

			// divide by zero?
			if (vLen == 0)
				throw new DivideByZeroException();

			uint[] u = new uint[this.Format.Digits + this.Format.RadixPoint + 1];
			numerator.CopyDigits(u, this.Format.RadixPoint);
			int uLen = getSignificantDigits(u);

			if (uLen < vLen)
			{
				// too small, set to 0.
				for (int i = 0; i < this.Format.Digits; i++)
					this.SetDigit(i, 0u);
			}
			else if (vLen == 1)
			{
				// divide by single digit
				this.divideUnsignedSingle(u, v[0]);
			}
			else
			{
				uint[] result = new uint[this.Format.Digits];

				int shift = getNormalizeShift(v[vLen - 1]);
				normalize(u, shift, uLen);
				normalize(v, shift, vLen);

				// main division loop
				for (int j = uLen - vLen; j >= 0; j--)
				{
					int i;

					ulong rr = ((ulong)u[j + vLen] << 32) + u[j + vLen - 1];
					ulong qq = rr / v[vLen - 1];
					rr -= qq * v[vLen - 1];

					while ((qq >= (1ul << 32)) || (qq * v[vLen - 2] > ((rr << 32) + u[j + vLen - 2])))
					{
						// estimate too big?
						qq--;
						rr += v[vLen - 1];
						if (rr >= (1ul << 32))
							break;
					}

					// multiply and subtract
					long borrow = 0;
					long t;
					for (i = 0; i < vLen; i++)
					{
						ulong p = v[i] * qq;
						t = u[i + j] - (long)(p & 0xFFFFFFFFul) - borrow;
						u[i + j] = (uint)t;
						p >>= 32;
						t >>= 32;
						borrow = (long)p - t;
					}
					t = u[j + vLen] - borrow;
					u[j + vLen] = (uint)t;

					// store the calculated value
					if (j < this.Format.Digits)
						result[j] = (uint)qq;
					else if (qq != 0)
						throw new OverflowException();

					// add back vn[0..n] to un[j..j+n]
					if (t < 0)
					{
						result[j]--;
						ulong c = 0;
						for (i = 0; i < vLen; i++)
						{
							c = (ulong)v[i] + u[j + i] + c;
							u[j + i] = (uint)c;
							c >>= 32;
						}
						c += u[j + vLen];
						u[j + vLen] = (uint)c;
					}
				}

				this.SetDigits(result);
			}
			
			if (negative)
				Fixed.Negate(this, this);
		}

		private void divideUnsignedSingle(uint[] a, uint b)
		{
			int length = getSignificantDigits(a);
			ulong rem = 0;

			for (int j = length - 1; j >= 0; j--)
			{
				rem = (rem << 32) + a[j];
				ulong div = rem / b;
				rem -= div * b;

				if (j < this.Format.Digits)
					this.SetDigit(j, (uint)div);
				else if (div != 0)
					throw new OverflowException();
			}
		}

		private static int getNormalizeShift(uint value)
		{
			int shift = 0;

			if ((value & 0xFFFF0000) == 0) { shift += 16; value <<= 16; }
			if ((value & 0xFF000000) == 0) { shift += 8; value <<= 8; }
			if ((value & 0xF0000000) == 0) { shift += 4; value <<= 4; }
			if ((value & 0xC0000000) == 0) { shift += 2; value <<= 2; }
			if ((value & 0x80000000) == 0) { shift += 1; }

			return shift;
		}

		private static void normalize(uint[] digits, int shift, int length)
		{
			uint carry = 0;
			if (shift > 0)
			{
				int rshift = 32 - shift;
				for (int i = 0; i < length; i++)
				{
					uint ui = digits[i];
					digits[i] = (ui << shift) | carry;
					carry = ui >> rshift;
				}
			}

			for(int i = length; i < digits.Length; i++)
				digits[i++] = 0;

			if (carry != 0)
				digits[length] = carry;
		}

		private static int getSignificantDigits(uint[] x)
		{
			for (int i = x.Length - 1; i >= 0; i--)
			{
				if (x[i] != 0)
					return i + 1;
			}

			return 0;
		}
	
		#endregion

		#region Static

		/// <summary>
		/// Negates the specified fixed-point number.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="result"></param>
		public static void Negate(Fixed value, Fixed result)
		{
			result.AssignNegate(value);
		}

		/// <summary>
		/// Adds two fixed-point numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Add(Fixed a, Fixed b, Fixed result)
		{
			result.AssignAdd(a, b);
		}

		/// <summary>
		/// Subtracts two fixed-point numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Subtract(Fixed a, Fixed b, Fixed result)
		{
			result.AssignSubtract(a, b);
		}

		/// <summary>
		/// Multiplies two fixed-point numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Multiply(Fixed a, Fixed b, Fixed result)
		{
			result.AssignMultiply(a, b);
		}

		/// <summary>
		/// Squares a fixed-point number.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="result"></param>
		public static void Square(Fixed value, Fixed result)
		{
			result.AssignSquare(value);
		}

		/// <summary>
		/// Divides two fixed-point numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Divide(Fixed a, Fixed b, Fixed result)
		{
			result.AssignDivide(a, b);
		}

		#endregion
	}
}
