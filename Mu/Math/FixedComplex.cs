﻿using System;

namespace Mu
{
	/// <summary>
	/// Fixed-point multi-precision complex number.
	/// </summary>
	public sealed class FixedComplex
	{
		#region Fields

		/// <summary>
		/// Number of digits.
		/// </summary>
		public readonly FixedFormat Format;

		/// <summary>
		/// Real part.
		/// </summary>
		public readonly Fixed Real;

		/// <summary>
		/// Imaginary part.
		/// </summary>
		public readonly Fixed Imaginary;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		internal FixedComplex(Fixed real, Fixed imaginary)
		{
			this.Format = real.Format;
			if (!this.Format.Equals(imaginary.Format))
				throw new ArgumentException();

			this.Real = real;
			this.Imaginary = imaginary;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the complex number to the specified value.
		/// </summary>
		/// <param name="real"></param>
		/// <param name="imag"></param>
		public void Set(int real, int imag)
		{
			this.Real.Set(real);
			this.Imaginary.Set(imag);
		}

		/// <summary>
		/// Sets the complex number to the specified value.
		/// </summary>
		/// <param name="real"></param>
		/// <param name="imag"></param>
		public void Set(double real, double imag)
		{
			this.Real.Set(real);
			this.Imaginary.Set(imag);
		}

		/// <summary>
		/// Sets the complex number to the specified value.
		/// </summary>
		/// <param name="real"></param>
		/// <param name="imag"></param>
		public void Set(Fixed real, Fixed imag)
		{
			this.Real.Set(real);
			this.Imaginary.Set(imag);
		}

		/// <summary>
		/// Sets the complex number to the specified value.
		/// </summary>
		/// <param name="value"></param>
		public void Set(FixedComplex value)
		{
			this.Real.Set(value.Real);
			this.Imaginary.Set(value.Imaginary);
		}

		/// <summary>
		/// Sets the complex number to the specified value.
		/// </summary>
		/// <param name="real"></param>
		/// <param name="imag"></param>
		public void Set(string real, string imag)
		{
			this.Real.Set(real);
			this.Imaginary.Set(imag);
		}

		/// <summary>
		/// Converts the complex fixed-point number to a string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return string.Format("{0} + {1}i", this.Real, this.Imaginary);
		}

		/// <summary>
		/// Converts the complex fixed-point number to a string.
		/// </summary>
		/// <param name="maxFractDigits">The maximum number of fractional digits.</param>
		/// <returns></returns>
		public string ToString(int maxFractDigits)
		{
			return string.Format("{0} + {1}i", this.Real.ToString(maxFractDigits), this.Imaginary.ToString(maxFractDigits));
		}

		#region Static

		#region Arithmetic

		/// <summary>
		/// Adds a real value to a complex number.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Add(FixedComplex a, Fixed b, FixedComplex result)
		{
			Fixed.Add(a.Real, b, result.Real);
			result.Imaginary.Set(a.Imaginary);
		}

		/// <summary>
		/// Adds a real value to a complex number.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Add(Fixed a, FixedComplex b, FixedComplex result)
		{
			Add(b, a, result);
		}
		
		/// <summary>
		/// Adds two complex numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Add(FixedComplex a, FixedComplex b, FixedComplex result)
		{
			Fixed.Add(a.Real, b.Real, result.Real);
			Fixed.Add(a.Imaginary, b.Imaginary, result.Imaginary);
		}

		/// <summary>
		/// Subtracts a real value from a complex number.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Subtract(FixedComplex a, Fixed b, FixedComplex result)
		{
			Fixed.Subtract(a.Real, b, result.Real);
			result.Imaginary.Set(a.Imaginary);
		}

		/// <summary>
		/// Subtracts a complex number from a real value.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Subtract(Fixed a, FixedComplex b, FixedComplex result)
		{
			Fixed.Subtract(a, b.Real, result.Real);
			Fixed.Negate(b.Imaginary, result.Imaginary);
		}

		/// <summary>
		/// Subtracts two complex numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Subtract(FixedComplex a, FixedComplex b, FixedComplex result)
		{
			Fixed.Subtract(a.Real, b.Real, result.Real);
			Fixed.Subtract(a.Imaginary, b.Imaginary, result.Imaginary);
		}

		/// <summary>
		/// Multiplies a complex number and a real value.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Multiply(FixedComplex a, Fixed b, FixedComplex result)
		{
			Fixed.Multiply(a.Real, b, result.Real);
			Fixed.Multiply(a.Imaginary, b, result.Imaginary);
		}

		/// <summary>
		/// Multiplies a complex number and a real value.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Multiply(Fixed a, FixedComplex b, FixedComplex result)
		{
			Fixed.Multiply(a, b.Real, result.Real);
			Fixed.Multiply(a, b.Imaginary, result.Imaginary);
		}
		
		/// <summary>
		/// Multiplies two complex numbers.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		public static void Multiply(FixedComplex a, FixedComplex b, FixedComplex result)
		{
			Fixed temp1 = Fixed.Create(result.Format);
			Fixed temp2 = Fixed.Create(result.Format);
			Fixed temp3 = Fixed.Create(result.Format);
			Fixed temp4 = Fixed.Create(result.Format);

			Fixed.Multiply(a.Real, b.Real, temp1);
			Fixed.Multiply(a.Imaginary, b.Imaginary, temp2);

			Fixed.Multiply(a.Real, b.Imaginary, temp3);
			Fixed.Multiply(a.Imaginary, b.Real, temp4);

			Fixed.Subtract(temp1, temp2, result.Real);
			Fixed.Add(temp3, temp4, result.Imaginary);
		}

		/// <summary>
		/// Squares a complex number.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="result"></param>
		public static void Square(FixedComplex a, FixedComplex result)
		{
			Fixed temp1 = Fixed.Create(result.Format);
			Fixed temp2 = Fixed.Create(result.Format);

			Fixed.Add(a.Real, a.Imaginary, temp1);
			Fixed.Subtract(a.Real, a.Imaginary, temp2);
			Fixed.Multiply(temp1, temp2, temp1);
			Fixed.Multiply(a.Real, a.Imaginary, temp2);

			result.Real.Set(temp1);
			Fixed.Add(temp2, temp2, result.Imaginary);
		}

		#endregion

		#region Creation

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format)
		{
			return new FixedComplex(Fixed.Create(format), Fixed.Create(format));
		}

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <param name="real">The initial real value.</param>
		/// <param name="imaginary">The initial imaginary value.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format, int real, int imaginary)
		{
			FixedComplex complex = FixedComplex.Create(format);
			complex.Real.Set(real);
			complex.Imaginary.Set(imaginary);
			return complex;
		}

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <param name="real">The initial real value.</param>
		/// <param name="imaginary">The initial imaginary value.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format, double real, double imaginary)
		{
			FixedComplex complex = FixedComplex.Create(format);
			complex.Real.Set(real);
			complex.Imaginary.Set(imaginary);
			return complex;
		}

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <param name="real">The initial real value.</param>
		/// <param name="imaginary">The initial imaginary value.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format, Fixed real, Fixed imaginary)
		{
			FixedComplex complex = FixedComplex.Create(format);
			complex.Real.Set(real);
			complex.Imaginary.Set(imaginary);
			return complex;
		}

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <param name="real">The initial real value.</param>
		/// <param name="imaginary">The initial imaginary value.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format, string real, string imaginary)
		{
			FixedComplex complex = FixedComplex.Create(format);
			complex.Real.Set(real);
			complex.Imaginary.Set(imaginary);
			return complex;
		}

		/// <summary>
		/// Creates a new complex fixed-point number.
		/// </summary>
		/// <param name="format">The fixed-point format.</param>
		/// <param name="value">The initial value.</param>
		/// <returns></returns>
		public static FixedComplex Create(FixedFormat format, FixedComplex value)
		{
			FixedComplex complex = FixedComplex.Create(format);
			complex.Set(value);
			return complex;
		}

		#endregion

		#endregion

		#region Operators

		public static FixedComplex operator +(FixedComplex a, Fixed b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Add(a, b, result);
			return result;
		}

		public static FixedComplex operator +(Fixed a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Add(a, b, result);
			return result;
		}
		
		public static FixedComplex operator +(FixedComplex a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Add(a, b, result);
			return result;
		}

		public static FixedComplex operator -(FixedComplex a, Fixed b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Subtract(a, b, result);
			return result;
		}

		public static FixedComplex operator -(Fixed a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Subtract(a, b, result);
			return result;
		}

		public static FixedComplex operator -(FixedComplex a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Subtract(a, b, result);
			return result;
		}

		public static FixedComplex operator *(FixedComplex a, Fixed b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Multiply(a, b, result);
			return result;
		}

		public static FixedComplex operator *(Fixed a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Multiply(a, b, result);
			return result;
		}

		public static FixedComplex operator *(FixedComplex a, FixedComplex b)
		{
			FixedComplex result = FixedComplex.Create(a.Format);
			FixedComplex.Multiply(a, b, result);
			return result;
		}

		#endregion

		#endregion
	}
}
